package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(
        name = "FS_ROLE",
        indexes = {@Index(
                columnList = "adGroup",
                unique = true,
                name = "uk_adGroup"
        )}
)
public final class Role extends AbstractNamedEntity {
    @NotNull
    public String adGroup;
    @NotNull
    @OneToMany(mappedBy = "role")
    private List<RoleRight> rights;

    public String getAdGroup() {
        return adGroup;
    }

    public void setAdGroup(@NotNull String adGroup) {
        this.adGroup = adGroup;
    }

    public List<RoleRight> getRights() {
        return rights;
    }

    public void setRights(@NotNull List<RoleRight> rights) {
        this.rights = rights;
    }
}
