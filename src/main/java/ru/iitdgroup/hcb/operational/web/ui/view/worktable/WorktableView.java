package ru.iitdgroup.hcb.operational.web.ui.view.worktable;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import org.jetbrains.annotations.NotNull;
import ru.iitdgroup.hcb.operational.web.constants.Constants;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.AlertDef;
import ru.iitdgroup.hcb.operational.web.presenters.worktable.WorkTablePresenter;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.CommonListComponent;
import ru.iitdgroup.hcb.operational.web.ui.components.FsBaseDialog;
import ru.iitdgroup.hcb.operational.web.ui.components.cards.EntityDetails;
import ru.iitdgroup.hcb.operational.web.ui.components.dialogs.AlertOwnerDialog;
import ru.iitdgroup.hcb.operational.web.ui.components.dialogs.ResoluteDialog;
import ru.iitdgroup.hcb.operational.web.ui.components.grids.AlertWorktableGrid;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.alerts.AlertInfoView;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.iitdgroup.hcb.operational.web.constants.Constants.*;

/**
 * Common worktable view
 * <p>
 * Рабочий стол
 */
@Route(value = "worktable", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class WorktableView extends CommonView {
    private final WorkTablePresenter workTablePresenter;
    private final AlertRepository alertRepository;
    private final UserService userService;
    private final CardRepository cardRepository;
    private final CardTransactionRepository cardTransactionRepository;
    private final Breadcrumbs breadcrumbs;
    private AlertWorktableGrid myAlertGrid;
    private AlertWorktableGrid inQueueGrid;
    private Button take;
    private Button resolute;
    private Button backToQueue;
    private Button toExecutor;
    private Button escalate;
    private Button hold;

    public WorktableView(
            WorkTablePresenter workTablePresenter,
            AlertRepository alertRepository,
            UserService userService,
            CardRepository cardRepository,
            CardTransactionRepository cardTransactionRepository,
            Breadcrumbs breadcrumbs
    ) {
        super(breadcrumbs);
        this.workTablePresenter = workTablePresenter;
        this.alertRepository = alertRepository;
        this.userService = userService;
        this.cardRepository = cardRepository;
        this.cardTransactionRepository = cardTransactionRepository;
        this.breadcrumbs = breadcrumbs;
        breadcrumbs.setBreadcrumb(this.getClass(), "Рабочий стол", this.getClass(), "Оповещения");
        init();
    }

    private void init() {
        CommonListComponent commonListComponent = new CommonListComponent();
        Details alertsInQueue = initAlertsInQueue(workTablePresenter.getInQueueAlertList(Constants.inQueue));

        Details myAlerts = initMyAlerts(workTablePresenter.getMyAlerts());
        alertsInQueue.addOpenedChangeListener(e -> {
            setSingleOpenAttribute(alertsInQueue, myAlerts);
        });
        myAlerts.addOpenedChangeListener(e -> {
            setSingleOpenAttribute(alertsInQueue, myAlerts);
        });
        HorizontalLayout buttonsLayout = initButtons();
        initGridSelectionListener();
        VerticalLayout grids = new VerticalLayout(alertsInQueue, myAlerts);
        grids.setPadding(false);
        grids.setSizeFull();
        VerticalLayout gridLayout = new VerticalLayout(grids, buttonsLayout);
        gridLayout.setSizeFull();
        gridLayout.setPadding(false);
        gridLayout.getStyle().set("padding-bottom", "10px");
        commonListComponent.getContentLayout().add(gridLayout);
        center.add(commonListComponent);
    }

    private void setSingleOpenAttribute(Details alertsInQueue, Details myAlerts) {
        boolean alertsInQueueOpened = alertsInQueue.isOpened();
        boolean myAlertsOpened = myAlerts.isOpened();
        if (alertsInQueueOpened && !myAlertsOpened) {
            alertsInQueue.getElement().setAttribute("single-open", "true");
            myAlerts.getElement().removeAttribute("single-open");
            return;
        }
        if (!alertsInQueueOpened && myAlertsOpened) {
            alertsInQueue.getElement().removeAttribute("single-open");
            myAlerts.getElement().setAttribute("single-open", "true");
            return;
        }
        alertsInQueue.getElement().removeAttribute("single-open");
        myAlerts.getElement().removeAttribute("single-open");
    }

    private void initGridSelectionListener() {
        inQueueGrid.addSelectionListener(e -> {
            Set<ICAlert> selectedItems = e.getAllSelectedItems();
            if (selectedItems != null && !selectedItems.isEmpty()) {
                take.setEnabled(true);
                toExecutor.setEnabled(true);
                return;
            }
            setButtonDisabled();
        });

        myAlertGrid.addSelectionListener(e -> {
            Set<ICAlert> selectedItems = e.getAllSelectedItems();
            if (selectedItems != null && !selectedItems.isEmpty()) {
                if (selectedItems.size() == 1) {
                    ICAlert alert = selectedItems.iterator().next();
                    String status = alert.getStatus();
                    switch (status) {
                        case inProgress:
                            backToQueue.setEnabled(false);
                            take.setEnabled(false);
                            resolute.setEnabled(true);
                            hold.setEnabled(true);
                            toExecutor.setEnabled(false);
                            escalate.setEnabled(true);
                            break;
                        case escalated:
                            backToQueue.setEnabled(false);
                            take.setEnabled(true);
                            resolute.setEnabled(false);
                            hold.setEnabled(false);
                            toExecutor.setEnabled(true);
                            escalate.setEnabled(false);
                            break;
                        case holded:
                            backToQueue.setEnabled(false);
                            take.setEnabled(true);
                            resolute.setEnabled(false);
                            hold.setEnabled(false);
                            toExecutor.setEnabled(false);
                            escalate.setEnabled(false);
                            break;
                        case closed:
                            backToQueue.setEnabled(true);
                            take.setEnabled(false);
                            resolute.setEnabled(false);
                            hold.setEnabled(false);
                            toExecutor.setEnabled(false);
                            escalate.setEnabled(true);
                            break;
                    }
                } else {
                    backToQueue.setEnabled(false);
                    take.setEnabled(true);
                    resolute.setEnabled(false);
                    hold.setEnabled(false);
                    toExecutor.setEnabled(false);
                    escalate.setEnabled(false);
                }
                return;
            }
            setButtonDisabled();
        });
    }

    private void setButtonDisabled() {
        take.setEnabled(false);
        resolute.setEnabled(false);
        backToQueue.setEnabled(false);
        toExecutor.setEnabled(false);
        escalate.setEnabled(false);
        hold.setEnabled(false);
    }

    @NotNull
    private HorizontalLayout initButtons() {
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        take = new Button("Взять в работу");
        take.addClickListener(e -> {
            Set<ICAlert> selectedItems = inQueueGrid.getSelectedItems();
            if (selectedItems == null || selectedItems.isEmpty()) {
                return;
            }
            if (selectedItems.size() > 1) {
                showTakeDialog(selectedItems);
                return;
            }

            ICAlert alert = selectedItems.iterator().next();
            takeToWork(alert);
            alertRepository.save(alert);
            updateGrids();
            take.getUI().ifPresent(ui -> {
                openNewWindow(alert, ui);
            });
        });
        take.setThemeName("primary");
        buttonsLayout.add(take);
        resolute = new Button("Указать резолюцию");
        resolute.addClickListener(e -> {
            Set<ICAlert> selectedItems = myAlertGrid.getSelectedItems();
            if (selectedItems.isEmpty()) {
                return;
            }
            if (selectedItems.size() > 1) {
                return;
            }

            ICAlert alert = selectedItems.iterator().next();
            ResoluteDialog dialog = new ResoluteDialog(alertRepository, alert, this::updateGrids);
            dialog.init();
            dialog.open();
        });
        resolute.setThemeName("primary");
        buttonsLayout.add(resolute);
        backToQueue = new Button("Вернуть в очередь");
        backToQueue.addClickListener(e -> setStatus(AlertDef.IN_QUEUE));
        buttonsLayout.add(backToQueue);
        hold = new Button("Отложить");
        hold.addClickListener(e -> setStatus(AlertDef.HOLD));
        buttonsLayout.add(hold);
        toExecutor = new Button("Назначить исполнителя");
        toExecutor.addClickListener(e -> {
            openOwnerDialog(userService.findAllByWorkspace(workTablePresenter.getSelectedWorkspace()));
        });
        buttonsLayout.add(toExecutor);
        escalate = new Button("Эскалировать");
        escalate.addClickListener(e -> {
            openOwnerDialog(userService.findAllByWorkspace(workTablePresenter.getSelectedWorkspace()));
        });
        buttonsLayout.add(escalate);

        setButtonDisabled();
        return buttonsLayout;
    }

    private void openOwnerDialog(List<UserAccount> userAccounts) {
        Set<ICAlert> selectedItems = myAlertGrid.getSelectedItems();
        Set<ICAlert> inQueueGridSelectedItems = inQueueGrid.getSelectedItems();
        if (selectedItems.size() > 1 || inQueueGridSelectedItems.size() > 1) {
            return;
        }
        if (selectedItems.isEmpty() && inQueueGridSelectedItems.isEmpty()) {
            return;
        }
        if (selectedItems.size() > 0 && inQueueGridSelectedItems.size() > 0) {
            return;
        }
        ICAlert alert;
        if (selectedItems.size() > 0) {
            alert = selectedItems.iterator().next();
        } else {
            alert = inQueueGridSelectedItems.iterator().next();
        }
        AlertOwnerDialog dialog = new AlertOwnerDialog(
                "Назначить исполнителя",
                userAccounts, alertRepository,
                alert,
                this::updateGrids);
        dialog.init();
        dialog.open();
    }

    private void setStatus(AlertDef status) {
        Set<ICAlert> selectedItems = myAlertGrid.getSelectedItems();
        if (selectedItems.isEmpty()) {
            return;
        }
        if (selectedItems.size() > 1) {
            return;
        }
        ICAlert alert = selectedItems.iterator().next();
        alert.setStatus(status.getStatus());
        alertRepository.save(alert);
        updateGrids();
    }

    private void takeToWork(ICAlert alert) {
        setStatus(AlertDef.IN_PROGRESS);
    }

    private void takeToWork(Set<ICAlert> selectedItems) {
        selectedItems.forEach(this::takeToWork);
        alertRepository.saveAll(selectedItems);
    }

    private void updateGrids() {
        List<ICAlert> inQueueAlertList = workTablePresenter.getInQueueAlertList(Constants.inQueue);
        inQueueGrid.setItems(inQueueAlertList);
        inQueueGrid.getDataProvider().refreshAll();

        List<ICAlert> myAlerts = workTablePresenter.getMyAlerts();
        myAlertGrid.setItems(myAlerts);
        myAlertGrid.getDataProvider().refreshAll();
    }

    private void showTakeDialog(Set<ICAlert> selectedItems) {
        List<String> list = selectedItems.stream()
                .map(it -> "\u00B7 " + it.getId())
                .collect(Collectors.toList());
        list.add(0, "Открыть в новом окне оповещения :");

        FsBaseDialog dialog = new FsBaseDialog(event -> {
            takeToWork(selectedItems);
            take.getUI().ifPresent(ui -> {
                selectedItems.forEach(it -> openNewWindow(it, ui));
            });
            updateGrids();
        }, "Взять в работу", list);
        dialog.open();
    }


    private void openNewWindow(ICAlert selectedItem, UI ui) {
        RouteConfiguration configuration = RouteConfiguration
                .forRegistry(ui.getRouter().getRegistry());
        String url = configuration.getUrl(AlertInfoView.class, selectedItem.getId());
        VaadinServletRequest currentRequest = (VaadinServletRequest) VaadinService.getCurrentRequest();
        String serverName = currentRequest.getServerName();
        int serverPort = currentRequest.getServerPort();
        String port = serverPort == 80 ? "" : ":" + serverPort;
        UI.getCurrent().getPage().open("http://" + serverName + port + "/" + url);
    }

    private Details initDetails(String caption) {
        EntityDetails details = new EntityDetails();
        details.setClassName("fs-worktable-details");
        details.getStyle().set("border", "unset");
        details.getStyle().set("border", "1px solid var(--opr-border-color)");
        details.setOpened(true);
        details.setSummaryText(caption);
        details.addThemeVariants(DetailsVariant.REVERSE);
        return details;
    }

    private Details initMyAlerts(List<ICAlert> myAlertList) {
        Details details = initDetails("Мои оповещения");
        Div div = new Div();
        div.setClassName("fs-alert-round-sign");
        int size = myAlertList.size();
        div.setText(String.valueOf(size));
        HorizontalLayout layout = new HorizontalLayout(new Label("Мои оповещения"), div);
        layout.setAlignItems(Alignment.CENTER);
        details.setSummary(layout);
        if (size == 0) {
            div.setVisible(false);
        }
        myAlertGrid = new AlertWorktableGrid(myAlertList, cardTransactionRepository, cardRepository);
        details.addContent(myAlertGrid);
        return details;
    }


    private Details initAlertsInQueue(List<ICAlert> inQueueList) {
        Details details = initDetails("Оповещения в очереди");
        inQueueGrid = new AlertWorktableGrid(inQueueList, cardTransactionRepository, cardRepository);
        details.addContent(inQueueGrid);
        return details;
    }
}
