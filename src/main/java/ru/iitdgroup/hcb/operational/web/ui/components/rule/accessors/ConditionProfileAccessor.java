package ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.data.binder.Binder;
import ru.iitdgroup.nwng.model.metric.model.def.MetricOutputDef;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.MetricValueAccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.ProfileAccessorDef;

import java.util.Collections;
import java.util.List;

public class ConditionProfileAccessor extends ConditionAccessor<ProfileAccessorDef> {
    private Binder<ProfileAccessorDef> binder;

    public ConditionProfileAccessor(AccessorDef prevAccessorDef, AccessorDef comparableAccessorDef) {
        super(prevAccessorDef, comparableAccessorDef);
        binder = new Binder<>();
        if (prevAccessorDef instanceof ProfileAccessorDef) {
            binder.setBean((ProfileAccessorDef) prevAccessorDef);
        } else {
            binder.setBean(new MetricValueAccessorDef(new MetricOutputDef()));
        }
    }

    @Override
    public String getCaption() {
        return "Профиль";
    }

    @Override
    public List<Component> getComponents() {
        return Collections.emptyList();
    }
}
