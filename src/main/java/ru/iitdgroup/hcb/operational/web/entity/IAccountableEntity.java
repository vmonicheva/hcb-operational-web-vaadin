package ru.iitdgroup.hcb.operational.web.entity;

import java.time.ZonedDateTime;

public interface IAccountableEntity {
    ZonedDateTime getExpiresOn();

    UserAccount getLastUpdatedBy();

    ZonedDateTime getLastUpdatedOn();

    UserAccount getVerifiedBy();

    ZonedDateTime getVerifiedOn();

}
