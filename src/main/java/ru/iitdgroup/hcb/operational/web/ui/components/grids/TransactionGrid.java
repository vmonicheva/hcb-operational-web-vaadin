package ru.iitdgroup.hcb.operational.web.ui.components.grids;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.RouteConfiguration;
import ru.iitdgroup.hcb.operational.web.ui.view.transactions.TransactionInfoView;
import ru.iitdgroup.model.hcb.cards.*;

import java.util.Collection;
import java.util.Date;
import java.util.Objects;

public class TransactionGrid extends FsGrid<CardTransaction> {

    public TransactionGrid(boolean showPagination,
                           Collection<CardTransaction> items) {
        super(showPagination, items);
    }

    @Override
    protected void addMainColumns() {
        Column<CardTransaction> id = addColumn(new ComponentRenderer<>((item) -> {
            String route = RouteConfiguration.forSessionScope().getUrl(TransactionInfoView.class, item.getId());
            return new Anchor(route, String.valueOf(item.getId()));
        }), "№ транзакции")
                .setHeader("№ транзакции")
                .setKey("id")
                .setFlexGrow(0)
                .setWidth("150px");
        id.setComparator((o1, o2) -> {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            return Long.compare(o1.getId(), o2.getId());
        });
        columns.add(id);
        columnMap.put(id.getKey(), "№ транзакции");

        Column<CardTransaction> authDate = addColumn((ValueProvider<CardTransaction, String>) item -> {
            Date authDateTime = item.getLocalTransactionDateTime();
            if (authDateTime == null) {
                return "";
            }
            return authDateTime.toString();
        }, "Дата, время авторизации")
                .setHeader("Дата, время авторизации")
                .setKey("authDate");
        columns.add(authDate);
        columnMap.put(authDate.getKey(), "Дата, время авторизации");
        authDate.setVisible(false);

        Column<CardTransaction> transactionCode = addColumn((ValueProvider<CardTransaction, String>) item -> {
            Integer code = item.getTransactionCode();
            return code != null ? code.toString() : "";
        }, "Код транзакции")
                .setHeader("Код транзакции")
                .setKey("transactionCode");
        columns.add(transactionCode);
        columnMap.put(transactionCode.getKey(), "Код транзакции");
        transactionCode.setVisible(false);

        Column<CardTransaction> terminalClass = addColumn((ValueProvider<CardTransaction, String>) item -> {
            Terminal terminal = item.getTerminal();
            return terminal != null ? terminal.getTerminalClass() : "";
        }, "Класс терминала")
                .setHeader("Класс терминала")
                .setKey("terminalClass");
        columns.add(terminalClass);
        columnMap.put(terminalClass.getKey(), "Класс терминала");
        terminalClass.setVisible(false);

        Column<CardTransaction> terminalOwner = addColumn((ValueProvider<CardTransaction, String>) item -> {
            TerminalOwner owner = item.getTerminalOwner();
            return owner != null ? owner.getName() : "";
        }, "Владелец терминала")
                .setHeader("Владелец терминала")
                .setKey("terminalOwner");
        columns.add(terminalOwner);
        columnMap.put(terminalOwner.getKey(), "Владелец терминала");
        terminalOwner.setVisible(false);

        Column<CardTransaction> cardNumber = addColumn((ValueProvider<CardTransaction, String>) cardTransaction -> {
            HCBCard card = cardTransaction.getCard();
            if (card == null) {
                return "";
            }
            return Objects.requireNonNullElse(card.getPan(), "");
        }, "Номер карты")
                .setHeader("Номер карты")
                .setKey("cardNumber");
        columns.add(cardNumber);
        columnMap.put(cardNumber.getKey(), "Номер карты");

        Column<CardTransaction> recieverCardNumber = addColumn((ValueProvider<CardTransaction, String>) cardTransaction -> {
            String anotherPan = cardTransaction.getAnotherPan();
            return Objects.requireNonNullElse(anotherPan, "");
        }, "Номер карты получателя")
                .setHeader("Номер карты получателя")
                .setKey("recieverCardNumber");
        columns.add(recieverCardNumber);
        columnMap.put(recieverCardNumber.getKey(), "Номер карты получателя");

        Column<CardTransaction> amount = addColumn(CardTransaction::getTransactionAmount, "Сумма")
                .setHeader("Сумма")
                .setKey("amount");
        columns.add(amount);
        columnMap.put(amount.getKey(), "Сумма");

        Column<CardTransaction> currency = addColumn((ValueProvider<CardTransaction, String>) cardTransaction -> {
            Currency curr = cardTransaction.getSourceCurrency();
            if (curr == null) {
                return "";
            }
            return curr.getIsoName();
        }, "Валюта")
                .setHeader("Валюта")
                .setKey("currency");
        columns.add(currency);
        columnMap.put(currency.getKey(), "Валюта");

        Column<CardTransaction> dateTime = addColumn(CardTransaction::getTransmissionDateAndTimeMoscow, "Дата, время")
                .setHeader("Дата, время")
                .setKey("dateTime");
        columns.add(dateTime);
        columnMap.put(dateTime.getKey(), "Дата, время");

//        Column<CardTransaction> dateTime = addColumn(CardTransaction::getTimestamp, "Способ покупки")
//                .setHeader("Способ покупки")
//                .setKey("purchaseType");
//        columns.add(dateTime);
//        columnMap.put(dateTime.getKey(), "Способ покупки");
    }

    @Override
    protected ComponentEventListener<ClickEvent<Icon>> getDetailedInfoListener(CardTransaction item) {
        return e -> UI.getCurrent().navigate(TransactionInfoView.class, item.getId());
    }
}
