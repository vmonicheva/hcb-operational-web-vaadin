package ru.iitdgroup.hcb.operational.web.services

import org.apache.logging.log4j.LogManager
import org.springframework.stereotype.Service
import ru.iitdgroup.nwng.model.api.JacksonUtils
import ru.iitdgroup.nwng.model.metacatalog.MessageBundle

/**
 * Сервис для работы с метакаталогом в БД/Ignite
 */
@Service
open class MetacatalogService() {

    companion object {
        private val LOGGER = LogManager.getLogger(MetacatalogService::class.java)
    }

    /**
     * Загрузка актуального метакаталога из БД
     *
     * @return актуальный метакаталог
     */
    open fun getMetacatalog(): MessageBundle {
        return JacksonUtils.fromJSON(
                javaClass.classLoader.getResourceAsStream("metacatalog.json"),
                MessageBundle::class.java)
    }

}