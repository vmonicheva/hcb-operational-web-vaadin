package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.*;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.repository.RuleInstanceRepository;
import ru.iitdgroup.hcb.operational.web.services.MetacatalogService;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.rule.RuleCreatorPanel;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.nwng.model.metacatalog.MessageBundle;
import ru.iitdgroup.nwng.model.metacatalog.def.MessageDef;
import ru.iitdgroup.nwng.model.metacatalog.def.MetaAttributeDef;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.CheckDef;
import ru.iitdgroup.nwng.model.rule.hierarchydef.RuleLogicDef;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Route(value = "rules/create", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class CreateRuleView extends CommonView implements HasUrlParameter<Long> {
    private final Breadcrumbs breadcrumbs;
    private final MetacatalogService metacatalogService;
    private final RuleInstanceRepository ruleInstanceRepository;
    private CheckDef checkDef;

    public CreateRuleView(Breadcrumbs breadcrumbs, MetacatalogService metacatalogService, RuleInstanceRepository ruleInstanceRepository) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
        this.metacatalogService = metacatalogService;
        this.ruleInstanceRepository = ruleInstanceRepository;
    }

    private HorizontalLayout initTopRow() {
        HorizontalLayout topRow = new HorizontalLayout();
        topRow.setAlignItems(Alignment.CENTER);
        topRow.setWidthFull();
        Label label = new Label("Редактирование условия");
        label.setClassName("fs-rule-name-label");
        label.setWidthFull();
        topRow.add(label);
        HorizontalLayout topButtons = new HorizontalLayout();
        topButtons.setWidthFull();
        Button save = new Button("Сохранить условие");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        topButtons.add(save);
        topButtons.add(new Button("Отменить"));
        topButtons.setJustifyContentMode(JustifyContentMode.END);
        topRow.add(topButtons);
        return topRow;
    }

    @Override
    public void setParameter(BeforeEvent event, Long param) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parameters = queryParameters.getParameters();
        List<String> guid = parameters.get("guid");
        CheckDef def = null;
        if (guid != null) {
            String checkGuid = guid.get(0);
            Optional<RuleInstance> byId = ruleInstanceRepository.findById(param);
            if (byId.isPresent()) {
                RuleInstance ruleInstance = byId.get();
                RuleLogicDef logicDef = ruleInstance.getLogicDef();
                if (logicDef != null) {
                    Map<Long, CheckDef> checks = logicDef.getChecks();
                    def = checks.values().stream().filter(it -> it.getGuid() == Long.parseLong(checkGuid)).findAny().orElse(null);
                }
            }
        }

        breadcrumbs.setBreadcrumb(this.getClass(), param, "Редактирование условия");
        if (def != null) {
            checkDef = def;
        } else {
            checkDef = new CheckDef("", "");
        }

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setId("fs-rule-card-layout");
        mainLayout.setSizeFull();
        mainLayout.getStyle().set("padding-top", "0");

        HorizontalLayout topRow = initTopRow();
        mainLayout.add(topRow);

        HorizontalLayout codeNameLayout = new HorizontalLayout();
        codeNameLayout.setWidthFull();
        codeNameLayout.add(new TextField("Код"));
        TextField description = new TextField("Описание условия");
        description.setWidthFull();
        codeNameLayout.add(description);
        mainLayout.add(codeNameLayout);

        HorizontalLayout innerLayout = new HorizontalLayout();
        innerLayout.setPadding(true);
        innerLayout.setSizeFull();
        innerLayout.getStyle().set("background-color", "white");
        mainLayout.add(innerLayout);
        VerticalLayout leftParamLayout = new VerticalLayout();
        initLeftParamLayout(leftParamLayout);
        leftParamLayout.setClassName("fs-left-param-layout");
        VerticalLayout conditionLayout = new VerticalLayout();
        initConditionLayout(conditionLayout);
        conditionLayout.setClassName("fs-condition-layout");
        VerticalLayout rightParamLayout = new VerticalLayout();
        initRightParamLayout(rightParamLayout);
        rightParamLayout.setClassName("fs-right-param-layout");
        innerLayout.add(leftParamLayout, conditionLayout, rightParamLayout);
        center.add(mainLayout);
    }

    private void initConditionLayout(VerticalLayout layout) {
        Label label = new Label("Условие сравнения");
        layout.add(label);
        ComboBox<String> conditionType = new ComboBox<>("Тип");
        conditionType.setItems(Collections.singleton("Сравнение"));
        layout.add(conditionType);
        HorizontalLayout conditionLayout = new HorizontalLayout();
        Button less = new Button("<");
        less.setClassName("fs-condition-button");
        less.setSizeUndefined();
        conditionLayout.add(less);
        Button equals = new Button("=");
        equals.setClassName("fs-condition-button");
        conditionLayout.add(equals);
        Button greater = new Button(">");
        greater.setClassName("fs-condition-button");
        conditionLayout.add(greater);
        Button notEquals = new Button("\u2260");
        notEquals.setClassName("fs-condition-button");
        conditionLayout.add(notEquals);
        Button lessOrEquals = new Button("\u2264");
        lessOrEquals.setClassName("fs-condition-button");
        conditionLayout.add(lessOrEquals);
        Button greaterOrEquals = new Button("\u2265");
        greaterOrEquals.setClassName("fs-condition-button");
        conditionLayout.add(greaterOrEquals);
        Button in = new Button("IN");
        in.setClassName("fs-condition-button");
        conditionLayout.add(in);
        layout.add(conditionLayout);
    }

    private void initLeftParamLayout(VerticalLayout layout) {
        MessageBundle metacatalog = metacatalogService.getMetacatalog();
        List<MessageDef> messages = metacatalog.getMessages().stream().filter(MessageDef::isUsedInRules).collect(Collectors.toList());
        List<MetaAttributeDef> metaAttributes = metacatalog.getMetaAttributes();
        AccessorDef accessorDef = checkDef.getLeft();
        AccessorDef comparableAccessorDef = checkDef.getRight();
        RuleCreatorPanel panel = new RuleCreatorPanel("Левый параметр", messages, accessorDef, comparableAccessorDef, metaAttributes);
        layout.add(panel);
    }

    private void initRightParamLayout(VerticalLayout layout) {
        MessageBundle metacatalog = metacatalogService.getMetacatalog();
        List<MessageDef> messages = metacatalog.getMessages().stream().filter(MessageDef::isUsedInRules).collect(Collectors.toList());
        List<MetaAttributeDef> metaAttributes = metacatalog.getMetaAttributes();
        AccessorDef accessorDef = checkDef.getRight();
        AccessorDef comparableAccessorDef = checkDef.getLeft();
        RuleCreatorPanel panel = new RuleCreatorPanel("Правый параметр", messages, accessorDef, comparableAccessorDef, metaAttributes);
        layout.add(panel);
    }
}
