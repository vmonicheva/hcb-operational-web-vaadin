package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;

public class RuleActionComponent extends Div {
    private String actionName;
    private ActionProducer actionProducer;
    private DragDataProducer dataProducer;

    RuleActionComponent(String actionName, ActionProducer actionProducer, DragDataProducer dataProducer) {
        super();
        add(new Label(actionName));
        this.actionName = actionName;
        this.actionProducer = actionProducer;
        this.dataProducer = dataProducer;
    }

    public String getActionName() {
        return actionName;
    }

    public ActionProducer getActionProducer() {
        return actionProducer;
    }

    public DragDataProducer getDataProducer() {
        return dataProducer;
    }
}
