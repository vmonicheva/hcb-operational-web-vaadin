package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(
        name = "FS_USER_ROLE"
)
public final class UserRole extends AbstractAccountableEntity {
    @NotNull
    @OneToOne
    public Role role;

    @NotNull
    @ManyToOne
    public UserAccount user;

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

    @NotNull
    public UserAccount getUser() {
        return user;
    }

    public void setUser(@NotNull UserAccount user) {
        this.user = user;
    }
}
