package ru.iitdgroup.hcb.operational.web.enums;

import ru.iitdgroup.nwng.model.rule.accessdef.MetricValueAccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.ProfileAccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.ProfileListAccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.ProfileValueAccessorDef;

import java.util.Arrays;
import java.util.List;

public class WorkspaceCategory<T extends ProfileAccessorDef> {
    public static WorkspaceCategory<MetricValueAccessorDef> METRIC = new WorkspaceCategory<>("Метрики", MetricValueAccessorDef.class);
    public static WorkspaceCategory<ProfileListAccessorDef> LISTS = new WorkspaceCategory<>("Списки", ProfileListAccessorDef.class);
    public static WorkspaceCategory<ProfileValueAccessorDef> VALUES = new WorkspaceCategory<>("Значения", ProfileValueAccessorDef.class);

    private String displayName;
    private Class<T> accessorClass;

    WorkspaceCategory(String displayName, Class<T> accessorClass) {
        this.displayName = displayName;
        this.accessorClass = accessorClass;
    }

    public static List<WorkspaceCategory<? extends ProfileAccessorDef>> getCategories() {
        return Arrays.asList(METRIC, LISTS, VALUES);
    }

    public String getDisplayName() {
        return displayName;
    }

    public Class<T> getAccessorClass() {
        return accessorClass;
    }
}
