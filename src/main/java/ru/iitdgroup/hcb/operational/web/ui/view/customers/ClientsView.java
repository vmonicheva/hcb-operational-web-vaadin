package ru.iitdgroup.hcb.operational.web.ui.view.customers;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.repository.ClientRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.CommonListComponent;
import ru.iitdgroup.hcb.operational.web.ui.components.grids.ClientGrid;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.alerts.AlertView;
import ru.iitdgroup.model.hcb.clients.Client;

import java.util.List;

@Route(value = "clients", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class ClientsView extends CommonView {
    private final ClientRepository clientRepository;

    public ClientsView(Breadcrumbs breadcrumbs, ClientRepository clientRepository) {
        super(breadcrumbs);
        this.clientRepository = clientRepository;
        breadcrumbs.setBreadcrumb(AlertView.class, "Справочники", this.getClass(), "Клиенты");

        CommonListComponent commonListComponent = new CommonListComponent();
        List<Client> clients = clientRepository.findAll();
        ClientGrid grid = new ClientGrid(true, clients);
        grid.setSelectionMode(Grid.SelectionMode.NONE);
        commonListComponent.getContentLayout().add(grid);
        center.add(commonListComponent);
    }
}
