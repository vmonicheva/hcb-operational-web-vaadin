package ru.iitdgroup.hcb.operational.web.ui.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.RouteConfiguration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Breadcrumbs {
    private String text;
    private HorizontalLayout breadcrumbs;
    private List<Anchor> anchors = new ArrayList<>();

    public Breadcrumbs() {
        breadcrumbs = new HorizontalLayout();
        breadcrumbs.setClassName("fs-breadcrumbs");
    }

    public void setBreadcrumb(Class<? extends Component> target, String name) {
        String route = RouteConfiguration.forSessionScope().getUrl(target);
        Anchor anchor = new Anchor(route, name);
        anchors.clear();
        anchors.add(anchor);
        breadcrumbs.removeAll();
        breadcrumbs.add(anchor);
    }

    public void setBreadcrumb(Class<? extends Component> parent, String parentName, Class<? extends Component> target, String name) {
        String route = RouteConfiguration.forSessionScope().getUrl(target);
        Anchor anchor = new Anchor(route, name);
        String parentRoute = RouteConfiguration.forSessionScope().getUrl(parent);
        Anchor parentAnchor = new Anchor(parentRoute, parentName);
        anchors.clear();
        anchors.add(parentAnchor);
        anchors.add(anchor);
        breadcrumbs.removeAll();
        breadcrumbs.add(parentAnchor);
        breadcrumbs.add(new Label("/"));
        breadcrumbs.add(anchor);
    }


    public <T, C extends Component & HasUrlParameter<T>> String setBreadcrumb(Class<? extends C> navigationTarget, T parameter, String name) {
        String route = RouteConfiguration.forSessionScope().getUrl(navigationTarget, parameter);
        Anchor anchor = new Anchor(route, name);
        if (!anchors.isEmpty()) {
            boolean anchorIsCurrent = anchors.get(anchors.size() - 1).getHref().equals(anchor.getHref());
            if (anchorIsCurrent) {
                return route;
            }
            boolean anchorIsPrevious = anchors.size() > 1 && anchors.get(anchors.size() - 2).getHref().equals(anchor.getHref());
            if (anchorIsPrevious) {
                List<Component> children = breadcrumbs.getChildren().collect(Collectors.toList());
                breadcrumbs.remove(children.get(children.size() - 1));
                breadcrumbs.remove(children.get(children.size() - 2));
                anchors.remove(anchors.get(anchors.size() - 1));
                return route;
            }
            breadcrumbs.add(new Label("/"));
        }
        anchors.add(anchor);
        breadcrumbs.add(anchor);
        return route;
    }


    public HorizontalLayout getBreadcrumbs() {
        return breadcrumbs;
    }
}
