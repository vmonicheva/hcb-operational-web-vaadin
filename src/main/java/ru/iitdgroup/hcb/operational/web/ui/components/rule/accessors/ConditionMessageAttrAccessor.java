package ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import ru.iitdgroup.nwng.model.metacatalog.def.AttributeDef;
import ru.iitdgroup.nwng.model.metacatalog.def.MessageDef;
import ru.iitdgroup.nwng.model.metacatalog.def.MetaAttributeDef;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.IncomingMessageAccessorDef;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ConditionMessageAttrAccessor extends ConditionAccessor<IncomingMessageAccessorDef> {
    private final List<MessageDef> messages;
    private final List<MetaAttributeDef> metaAttributes;
    private Binder<IncomingMessageAccessorDef> binder;

    public ConditionMessageAttrAccessor(AccessorDef prevAccessorDef,
                                        AccessorDef comparableAccessorDef,
                                        List<MessageDef> messages, List<MetaAttributeDef> metaAttributes) {
        super(prevAccessorDef, comparableAccessorDef);
        this.messages = messages;
        this.metaAttributes = metaAttributes;
        binder = new Binder<>();
        if (prevAccessorDef instanceof IncomingMessageAccessorDef) {
            binder.setBean((IncomingMessageAccessorDef) prevAccessorDef);
        } else {
            binder.setBean(new IncomingMessageAccessorDef(messages.get(0), metaAttributes.get(0)));
        }
    }

    @Override
    public String getCaption() {
        return "Тип сообщения";
    }

    @Override
    public List<Component> getComponents() {
        ComboBox<MessageDef> messageDefBox = new ComboBox<>("Тип сообщения");
        messageDefBox.setWidthFull();
        ComboBox<MetaAttributeDef> metaAttrBox = new ComboBox<>("Бизнес-атрибут");
        metaAttrBox.setWidthFull();

        ListDataProvider<MetaAttributeDef> metaAttrDataProvider = new ListDataProvider<>(metaAttributes);
        metaAttrDataProvider.setFilter(it ->
                {
                    MessageDef value = messageDefBox.getValue();
                    if (value != null) {
                        Map<Long, AttributeDef> attributes = value.getAttributes();
                        if (attributes != null) {
                            return attributes.containsKey(it.getGuid());
                        }
                    }
                    return false;
                }
        );
        metaAttrBox.setDataProvider(metaAttrDataProvider);
        metaAttrBox.setItemLabelGenerator(MetaAttributeDef::getName);

        messageDefBox.setDataProvider(new ListDataProvider<>(messages));
        messageDefBox.setItemLabelGenerator(MessageDef::getName);
        binder.forField(messageDefBox).bind(IncomingMessageAccessorDef::getMessageDef, IncomingMessageAccessorDef::setMessageDef);
        messageDefBox.addValueChangeListener(it -> {
            if (it.isFromClient()) {
                metaAttrBox.clear();
            }
        });


        metaAttrBox.setValue(binder.getBean().getMetaAttributeDef());
        binder.forField(metaAttrBox).bind(IncomingMessageAccessorDef::getMetaAttributeDef, IncomingMessageAccessorDef::setMetaAttributeDef);

        return Arrays.asList(messageDefBox, metaAttrBox);
    }
}
