package ru.iitdgroup.hcb.operational.web.ui.view.transactions;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.CommonListComponent;
import ru.iitdgroup.hcb.operational.web.ui.components.grids.TransactionGrid;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.model.hcb.cards.CardTransaction;

import java.util.List;

@Route(value = "transactions", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class TransactionsView extends CommonView {
    private final Breadcrumbs breadcrumbs;
    private final CardTransactionRepository transactionRepository;

    public TransactionsView(Breadcrumbs breadcrumbs, CardTransactionRepository transactionRepository) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
        this.transactionRepository = transactionRepository;
        breadcrumbs.setBreadcrumb(this.getClass(), "Справочники", this.getClass(), "Транзакции");

        CommonListComponent commonListComponent = new CommonListComponent();
        List<CardTransaction> transactions = transactionRepository.findAllFetched();
        TransactionGrid grid = new TransactionGrid(true, transactions);
        grid.setSelectionMode(Grid.SelectionMode.NONE);
        commonListComponent.getContentLayout().add(grid);
        center.add(commonListComponent);
    }
}
