package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;
import ru.iitdgroup.nwng.model.rule.hierarchydef.GroupType;

import javax.persistence.*;

@Entity(
        name = "group"
)
@Table(
        name = "FS_RULE_GROUP"
)
public final class RuleGroup extends AbstractNamedEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    public GroupType groupType;

    @NotNull
    @ManyToOne(targetEntity = Workspace.class)
    public Workspace workspace;

    public RuleGroup() {
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(@NotNull GroupType groupType) {
        this.groupType = groupType;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(@NotNull Workspace workspace) {
        this.workspace = workspace;
    }
}
