package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.data.binder.Binder;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.services.MetacatalogService;

import java.io.Serializable;

@FunctionalInterface
public interface DragDataProducer extends Serializable {

    Component getComponent(Binder<RuleInstance> binder,
                           MetacatalogService metacatalogService,
                           Long key);
}
