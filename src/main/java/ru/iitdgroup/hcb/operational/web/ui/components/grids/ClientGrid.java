package ru.iitdgroup.hcb.operational.web.ui.components.grids;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.RouteConfiguration;
import ru.iitdgroup.hcb.operational.web.ui.view.customers.ClientInfoView;
import ru.iitdgroup.model.hcb.clients.Client;

import java.util.Collection;
import java.util.Comparator;

public class ClientGrid extends FsGrid<Client> {

    public ClientGrid(boolean showPagination, Collection<Client> items) {
        super(showPagination, items);
    }

    @Override
    protected void addMainColumns() {
        Comparator<Client> comparator = (o1, o2) -> {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            return o1.getFirstName().compareTo(o2.getFirstName());
        };
        Column<Client> column1 =
                addColumn(new ComponentRenderer<>((client) -> {
                    String route = RouteConfiguration.forSessionScope().getUrl(ClientInfoView.class, client.getId());
                    return new Anchor(route, String.valueOf(client.getFirstName()));
                }), "ФИО")
                        .setHeader("ФИО")
                        .setKey("fio");
        column1.setComparator(comparator);
        columns.add(column1);
        columnMap.put(column1.getKey(), "ФИО");
        Column<Client> column2 = addColumn(Client::getCuid, "CUID")
                .setHeader("CUID")
                .setKey("cuid");
        columns.add(column2);
        columnMap.put(column2.getKey(), "CUID");
        Column<Client> column3 = addColumn(Client::getBirthDate, "Дата рождения")
                .setHeader("Дата рождения")
                .setKey("birthday");
        columns.add(column3);
        columnMap.put(column3.getKey(), "Дата рождения");
        Column<Client> column4 = addColumn(Client::getPassport, "Паспорт")
                .setHeader("Паспорт")
                .setKey("passport");
        columns.add(column4);
        columnMap.put(column4.getKey(), "Паспорт");
    }

    @Override
    protected ComponentEventListener<ClickEvent<Icon>> getDetailedInfoListener(Client item) {
        return e -> UI.getCurrent().navigate(ClientInfoView.class, item.getId());
    }
}
