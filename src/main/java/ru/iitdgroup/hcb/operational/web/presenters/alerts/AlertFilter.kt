package ru.iitdgroup.hcb.operational.web.presenters.alerts


/**
 * Класс наследник от BaseFilter
 * Введен фильтрация по переключаемому окну.
 */
class AlertFilter : BaseFilter {
    var tab: String
    var name: String

    constructor(text: String?, tab: String) : super(text!!) {
        this.tab = tab
        this.name = ""
    }

    constructor() : super() {
        tab = ""
        name = ""
    }

}