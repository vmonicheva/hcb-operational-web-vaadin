package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import java.time.ZonedDateTime;

@MappedSuperclass
public abstract class AbstractAccountableEntity extends AbstractEntity implements IAccountableEntity {
    @NotNull
    @OneToOne
    public UserAccount lastUpdatedBy;

    @NotNull
    public ZonedDateTime lastUpdatedOn;

    @Nullable
    private ZonedDateTime expiresOn;

    @Nullable
    @OneToOne
    private UserAccount verifiedBy;

    @Nullable
    private ZonedDateTime verifiedOn;

    @Override
    @NotNull
    public UserAccount getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(@NotNull UserAccount lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    @Override
    @NotNull
    public ZonedDateTime getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(@NotNull ZonedDateTime lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    @Override
    @Nullable
    public ZonedDateTime getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(@Nullable ZonedDateTime expiresOn) {
        this.expiresOn = expiresOn;
    }

    @Override
    @Nullable
    public UserAccount getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(@Nullable UserAccount verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    @Override
    @Nullable
    public ZonedDateTime getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(@Nullable ZonedDateTime verifiedOn) {
        this.verifiedOn = verifiedOn;
    }
}
