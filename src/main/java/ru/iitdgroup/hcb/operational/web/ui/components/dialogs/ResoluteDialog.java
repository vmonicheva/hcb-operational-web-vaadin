package ru.iitdgroup.hcb.operational.web.ui.components.dialogs;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.listbox.ListBox;
import org.apache.commons.lang3.StringUtils;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.AlertDef;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.FsDialog;

import java.util.Arrays;
import java.util.List;

public class ResoluteDialog extends FsDialog {
    private static final List<String> resolutions = Arrays.asList("Мошенничество", "Легитимно", "Недозвон", "Звонок не требуется");
    private final AlertRepository alertRepository;
    private final ICAlert alert;
    private Runnable afterCloseUpdate;

    public ResoluteDialog(AlertRepository alertRepository, ICAlert alert, Runnable afterCloseUpdate) {
        super("Указать резолюцию");
        this.alertRepository = alertRepository;
        this.alert = alert;
        this.afterCloseUpdate = afterCloseUpdate;
    }

    @Override
    public void init() {
        super.init();
        setWidth("300px");
        getOkCancelLayout().setVisible(false);
        Button apply = new Button("Применить");
        apply.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        apply.setEnabled(false);
        ListBox<String> listBox = new ListBox<>();
        listBox.setItems(resolutions);
        listBox.addValueChangeListener(e -> apply.setEnabled(true));
        mainLayout.addComponentAtIndex(1, listBox);
        getButtonsLayout().add(apply);
        apply.addClickListener(e -> {
            String value = listBox.getValue();
            if (StringUtils.isEmpty(value)) {
                return;
            }
            alert.setResolution(value);
            alert.setStatus(AlertDef.CLOSED.getStatus());
            alertRepository.save(alert);
            close();
            afterCloseUpdate.run();
        });
    }
}
