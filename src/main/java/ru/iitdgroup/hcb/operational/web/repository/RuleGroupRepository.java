package ru.iitdgroup.hcb.operational.web.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.hcb.operational.web.entity.RuleGroup;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;

import java.util.List;

public interface RuleGroupRepository extends CrudRepository<RuleGroup, Long> {
    @Override
    List<RuleGroup> findAll();

    List<RuleGroup> findAllByWorkspace(@NotNull Workspace ws);
}
