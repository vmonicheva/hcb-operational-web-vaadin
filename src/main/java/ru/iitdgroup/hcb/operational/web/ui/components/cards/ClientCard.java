package ru.iitdgroup.hcb.operational.web.ui.components.cards;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.iitdgroup.hcb.operational.web.ui.components.SingleColumnFormLayout;

public class ClientCard extends EntityInfoLayout {

    @Override
    public void init() {
        super.init();
        createCards(card);
        initActionsButtons(innerButtonLayout);
    }

    @Override
    protected void createCards(EntityCard card) {
        createMainInfoCard(card);
        createAddressCard(card);
        createDocumentsCard(card);
        createCardsCard(card);
        createHistoryCard(card);
    }

    private void createHistoryCard(EntityCard card) {
        Grid<Object> content = new Grid<>();
        card.addInfoCard("История изменений", content);

    }

    private void createCardsCard(EntityCard card) {
        Grid<Object> content = new Grid<>();
        card.addInfoCard("Счета и карты", content);
    }

    private void createDocumentsCard(EntityCard card) {
        SingleColumnFormLayout layout = new SingleColumnFormLayout();
        layout.addFormItem(new Label(), "Паспорт");
        layout.addFormItem(new Label(), "Права");
        layout.addFormItem(new Label(), "Снилс");

        card.addInfoCard("Документы", layout);
    }

    private void createAddressCard(EntityCard card) {
        SingleColumnFormLayout layout = new SingleColumnFormLayout();
        layout.addFormItem(new Label(), "Постоянная регистрация");
        layout.addFormItem(new Label(), "Фактический");
        card.addInfoCard("Адрес", layout);
    }

    private void createMainInfoCard(EntityCard card) {
        SingleColumnFormLayout layout = new SingleColumnFormLayout();
        layout.addFormItem(new Label(), "ФИО");
        layout.addFormItem(new Label(), "CUID");
        layout.addFormItem(new Label(), "Телефон рабочий");
        layout.addFormItem(new Label(), "Телефон мобильный");
        layout.addFormItem(new Label(), "Пол");
        layout.addFormItem(new Label(), "Кодовое слово");
        card.addInfoCard("Клиент", layout);
    }

    @Override
    protected void initActionsButtons(VerticalLayout innerButtonLayout) {
        Button toWhite = new Button("Добавить в белый список");
        toWhite.setClassName("opr-action-button");
        innerButtonLayout.add(toWhite);
        Button toBlack = new Button("Добавить в черный список");
        toBlack.setClassName("opr-action-button");
        innerButtonLayout.add(toBlack);
        Button banCredit = new Button("Запретить расходные операции");
        banCredit.setClassName("opr-action-button");
        innerButtonLayout.add(banCredit);
        Button allowCredit = new Button("Разрешить расходные операции");
        allowCredit.setClassName("opr-action-button");
        innerButtonLayout.add(allowCredit);
        Button banCash = new Button("Запретить снятие наличных");
        banCash.setClassName("opr-action-button");
        innerButtonLayout.add(banCash);
        Button allowCash = new Button("Разрешить снятие наличных");
        allowCash.setClassName("opr-action-button");
        innerButtonLayout.add(allowCash);
        Button blockCard = new Button("Заблокировать карту");
        blockCard.setClassName("opr-action-button");
        innerButtonLayout.add(blockCard);
        Button blockAllCards = new Button("Заблокировать все карты");
        blockAllCards.setClassName("opr-action-button");
        innerButtonLayout.add(blockAllCards);
        Button sendReport = new Button("Отправить отчет");
        sendReport.setClassName("opr-action-button");
    }
}
