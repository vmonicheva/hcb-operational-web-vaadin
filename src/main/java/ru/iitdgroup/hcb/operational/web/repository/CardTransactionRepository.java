package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.model.hcb.cards.CardTransaction;
import ru.iitdgroup.model.hcb.cards.HCBCard;

import java.util.List;
import java.util.Optional;

public interface CardTransactionRepository extends CrudRepository<CardTransaction, Long> {

    @Query("select ct from CardTransaction ct " +
            "left join fetch ct.card " +
            "left join fetch ct.terminal " +
            "left join fetch ct.terminalPaymentSystem " +
            "left join fetch ct.dstAccountCurrency " +
            "left join fetch ct.transactionCurrencyCode " +
            "left join fetch ct.cardholderBillingCurrencyCode " +
            "left join fetch ct.terminalOwner " +
            "left join fetch ct.terminalCountry " +
            "left join fetch ct.terminalCity " +
            "left join fetch ct.terminalAddress " +
            "left join fetch ct.terminalRetailer " +
            "left join fetch ct.acquirer " +
            "left join fetch ct.acquiringInstitutionCountryCode " +
            "left join fetch ct.merchantCategory " +
            "left join fetch ct.sourceCurrency " +
            "where ct.id = :id")
    Optional<CardTransaction> findFetchedById(Long id);

    @Query("select ct from CardTransaction ct " +
            "left join fetch ct.card " +
            "left join fetch ct.terminal " +
            "left join fetch ct.terminalPaymentSystem " +
            "left join fetch ct.dstAccountCurrency " +
            "left join fetch ct.transactionCurrencyCode " +
            "left join fetch ct.cardholderBillingCurrencyCode " +
            "left join fetch ct.terminalOwner " +
            "left join fetch ct.terminalCountry " +
            "left join fetch ct.terminalCity " +
            "left join fetch ct.terminalAddress " +
            "left join fetch ct.terminalRetailer " +
            "left join fetch ct.acquirer " +
            "left join fetch ct.acquiringInstitutionCountryCode " +
            "left join fetch ct.merchantCategory " +
            "left join fetch ct.sourceCurrency")
    List<CardTransaction> findAllFetched();

    List<CardTransaction> findAllByCard(HCBCard card);

    Page<CardTransaction> findAll(Specification<CardTransaction> spec, Pageable pageable);

    @Override
    List<CardTransaction> findAll();
}
