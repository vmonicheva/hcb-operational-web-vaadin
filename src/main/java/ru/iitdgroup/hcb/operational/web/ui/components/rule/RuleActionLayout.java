package ru.iitdgroup.hcb.operational.web.ui.components.rule;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.iitdgroup.hcb.operational.web.ui.util.NoResourceLayout;

public class RuleActionLayout extends VerticalLayout {

    public RuleActionLayout() {
        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);
        NoResourceLayout layout = new NoResourceLayout();
        add(layout);
    }
}
