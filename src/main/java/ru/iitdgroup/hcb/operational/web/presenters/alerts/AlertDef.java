package ru.iitdgroup.hcb.operational.web.presenters.alerts;

import java.util.Arrays;
import java.util.Objects;

import static ru.iitdgroup.hcb.operational.web.constants.Constants.*;

public enum AlertDef {
    IN_QUEUE(inQueue, "В очереди"),
    IN_PROGRESS(inProgress, "В работе"),
    CLOSED(closed, "Закрыт"),
    ESCALATED(escalated, "Эскалирован"),
    HOLD(holded, "Отложен");

    private String status;
    private String displayName;

    AlertDef(String status, String displayName) {
        this.status = status;
        this.displayName = displayName;
    }

    public static String getDisplayName(String status) {
        AlertDef alerDef = Arrays.stream(values()).filter(it -> Objects.equals(status, it.getStatus())).findFirst().orElse(null);
        return alerDef != null ? alerDef.getDisplayName() : "";
    }

    public String getStatus() {
        return status;
    }

    public String getDisplayName() {
        return displayName;
    }
}
