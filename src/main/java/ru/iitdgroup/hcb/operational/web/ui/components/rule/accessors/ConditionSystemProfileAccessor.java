package ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.function.ValueProvider;
import ru.iitdgroup.hcb.operational.web.enums.WorkspaceCategory;
import ru.iitdgroup.nwng.model.metacatalog.def.MetaAttributeDef;
import ru.iitdgroup.nwng.model.metric.model.def.MetricOutputDef;
import ru.iitdgroup.nwng.model.profile.MockProfiles;
import ru.iitdgroup.nwng.model.profile.model.def.AbstractProfileValueDef;
import ru.iitdgroup.nwng.model.profile.model.def.ProfileListDef;
import ru.iitdgroup.nwng.model.profile.model.def.SystemProfileDef;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.MetricValueAccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.ProfileAccessorDef;

import java.util.*;

import static ru.iitdgroup.hcb.operational.web.enums.WorkspaceCategory.*;

public class ConditionSystemProfileAccessor extends ConditionAccessor<ProfileAccessorDef> {
    private final List<MetaAttributeDef> metaAttributes;
    private Binder<ProfileAccessorDef> binder;

    public ConditionSystemProfileAccessor(AccessorDef prevAccessorDef, AccessorDef comparableAccessorDef, List<MetaAttributeDef> metaAttributes) {
        super(prevAccessorDef, comparableAccessorDef);
        this.metaAttributes = metaAttributes;
        binder = new Binder<>();
        if (prevAccessorDef instanceof ProfileAccessorDef) {
            binder.setBean((ProfileAccessorDef) prevAccessorDef);
        } else {
            binder.setBean(new MetricValueAccessorDef(new MetricOutputDef()));
        }
    }

    @Override
    public String getCaption() {
        return "Системный профиль";
    }

    @Override
    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>();
        Binder<MetricValueAccessorDef> metricValueBinder = new Binder<>();
        List<Component> metricFields = getMetricFields(metricValueBinder);
        metricFields.forEach(it -> it.setVisible(true));
        List<Component> listFields = getListFields();
        List<Component> valueFields = getValueFields();
        components.addAll(metricFields);
        components.addAll(listFields);
        components.addAll(valueFields);
        components.forEach(it -> it.setVisible(false));
        ComboBox<WorkspaceCategory<? extends ProfileAccessorDef>> category = new ComboBox<>("Раздел профиля");
        category.setWidthFull();
        category.setItems(WorkspaceCategory.getCategories());
        category.setValue(WorkspaceCategory.METRIC);

        category.setItemLabelGenerator(WorkspaceCategory::getDisplayName);
        category.addValueChangeListener(event -> {
            components.forEach(it -> it.setVisible(false));
            WorkspaceCategory<? extends ProfileAccessorDef> value = event.getValue();
            if (value.equals(METRIC)) {
                metricFields.forEach(it -> it.setVisible(true));
                return;
            }
            if (value.equals(LISTS)) {
                listFields.forEach(it -> it.setVisible(true));
                return;
            }
            if (value.equals(VALUES)) {
                valueFields.forEach(it -> it.setVisible(true));
            }
        });
        List<Component> result = new ArrayList<>();
        result.add(category);
        result.addAll(components);
        return result;
    }

    private List<Component> getMetricFields(Binder<MetricValueAccessorDef> binder) {
        ComboBox<MetaAttributeDef> metaAttrBox = new ComboBox<>("Бизнес-атрибут");
        metaAttrBox.setWidthFull();
        metaAttrBox.setItems(metaAttributes);
        metaAttrBox.setItemLabelGenerator(MetaAttributeDef::getName);
        binder.forField(metaAttrBox).bind((ValueProvider<MetricValueAccessorDef, MetaAttributeDef>) metricValueAccessorDef -> {
            MetricOutputDef metricDef = metricValueAccessorDef.getMetricDef();
            return metricDef != null ? metricDef.getOutAttribute() : null;
        }, (Setter<MetricValueAccessorDef, MetaAttributeDef>) (metricValueAccessorDef, metaAttributeDef) -> {
            MetricOutputDef metricDef = metricValueAccessorDef.getMetricDef();
            if (metricDef != null) {
                metricDef.setOutAttribute(metaAttributeDef);
            }
        });
        return Collections.singletonList(metaAttrBox);
    }

    private List<Component> getListFields() {
        ComboBox<ProfileListDef> comboBox = new ComboBox<>("Наименование списка");
        comboBox.setWidthFull();
        comboBox.setItemLabelGenerator(ProfileListDef::getName);
        List<SystemProfileDef> systemProfileDefs = MockProfiles.SYSTEM_PROFILE_DEFS;
        if (!systemProfileDefs.isEmpty()) {
            SystemProfileDef systemProfileDef = systemProfileDefs.get(0);
            if (systemProfileDef != null) {
                Map<Long, ProfileListDef> profileLists = systemProfileDef.getProfileLists();
                if (!profileLists.isEmpty()) {
                    Collection<ProfileListDef> values = profileLists.values();
                    comboBox.setItems(values);
                }
            }
        }

        return Collections.singletonList(comboBox);
    }

    private List<Component> getValueFields() {
        ComboBox<AbstractProfileValueDef> comboBox = new ComboBox<>("Наименование переменной");
        comboBox.setWidthFull();
        comboBox.setItemLabelGenerator(AbstractProfileValueDef::getName);
        List<SystemProfileDef> systemProfileDefs = MockProfiles.SYSTEM_PROFILE_DEFS;
        if (!systemProfileDefs.isEmpty()) {
            SystemProfileDef systemProfileDef = systemProfileDefs.get(0);
            if (systemProfileDef != null) {
                Map<Long, AbstractProfileValueDef> profileValues = systemProfileDef.getProfileValues();
                if (!profileValues.isEmpty()) {
                    Collection<AbstractProfileValueDef> values = profileValues.values();
                    comboBox.setItems(values);
                }
            }
        }
        return Collections.singletonList(comboBox);
    }
}
