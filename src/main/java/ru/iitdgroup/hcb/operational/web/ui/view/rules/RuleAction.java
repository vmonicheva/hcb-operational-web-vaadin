package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.nwng.model.metacatalog.def.MetaAttributeDef;
import ru.iitdgroup.nwng.model.rule.actionsdef.*;
import ru.iitdgroup.nwng.model.rule.hierarchydef.RuleLogicDef;

import java.util.Map;

public enum RuleAction {
    SEND_EMAIL(new RuleActionComponent("Отправить E-mail", SendEMailActionDef::new,
            (DragDataProducer) (binder, metacatalogService, key) -> {
                VerticalLayout layout = new VerticalLayout();
                TextField to = new TextField("Адресат");
                TextField theme = new TextField("Тема письма");
                TextField text = new TextField("Текст");
                layout.add(to, theme, text);
                return layout;
            })),

    SEND_SMS(new RuleActionComponent("Отправить СМС", SendSMSAction::new,
            (DragDataProducer) (binder, metacatalogService, key) -> {
                VerticalLayout layout = new VerticalLayout();
                TextField to = new TextField("Адресат");
                TextField text = new TextField("Текст");
                layout.add(to, text);
                return layout;
            })),

    TRIGGER_RULE(new RuleActionComponent("Обработать правило",
            GrayRuleAction::new,
            (DragDataProducer) (binder, metacatalogService, key) -> {
                VerticalLayout layout = new VerticalLayout();
                TextField scoring = new TextField("Скоринг");
                GrayRuleAction grayRuleAction = new GrayRuleAction();
                if (key != null) {
                    layout.setId(key.toString());
                }
                RuleInstance bean = binder.getBean();
                if (bean != null) {
                    RuleLogicDef logicDef = bean.getLogicDef();
                    if (logicDef != null) {
                        Map<Long, AbstractActionDef> postActions = logicDef.getPostActions();
                        if (postActions != null) {
                            postActions.put(key, grayRuleAction);
                        }
                        logicDef.setPostActions(postActions);
                    }
                    scoring.setValue(bean.getScoring().toString());
                }

//                binder
//                        .forField(scoring)
//                        .asRequired()
//                        .bind({ (grayRuleAction.score?.toString()) }, { it, v ->
//                        map = it.logicDef!!.postActions
//                        grayRuleAction = map?.get(key) as GrayRuleAction
//                    grayRuleAction.score = v?.toInt()
//                    map?.set(key, grayRuleAction)
//                    if (it.logicDef != null) {
//                        it.logicDef = it.logicDef!!.setPostActions(map) as RuleLogicDef
//                    }
//                })


                ComboBox<MathAction> mathOperationBox = new ComboBox<>("Операция");
                mathOperationBox.setItems(MathAction.values());
                mathOperationBox.setItemLabelGenerator(it -> {
                    if (it == null) {
                        return "";
                    }
                    if (it.equals(MathAction.ADD)) {
                        return "+";
                    }
                    if (it.equals(MathAction.SUBTRACT)) {
                        return "-";
                    }
                    if (it.equals(MathAction.MULTIPLY)) {
                        return "*";
                    }
                    return "";
                });

//                binder
//                        .forField(mathOperationBox)
//                        .asRequired()
//                        .bind({ grayRuleAction.mathAction }, { it, v ->
//                                        map = it.logicDef!!.postActions
//                                grayRuleAction = map?.get(key) as GrayRuleAction

                ComboBox<MetaAttributeDef> attributeKey = new ComboBox<>("Ключ");
                attributeKey.setItems(metacatalogService.getMetacatalog().getMetaAttributes());
                attributeKey.setItemLabelGenerator(MetaAttributeDef::getName);
//                binder
//                        .forField(attributeKey)
//                        .asRequired()
//                        .bind({ grayRuleAction.metaAttributeDef }, { it, v ->
//                if (v != null) {
//                    map = it.logicDef!!.postActions
//                            grayRuleAction = map?.get(key) as GrayRuleAction
//                    grayRuleAction.metaAttributeDef = v
//                    map?.set(key, grayRuleAction)
//                    if (it.logicDef != null) {
//                        it.logicDef = it.logicDef!!.setPostActions(map) as RuleLogicDef
//                    }
//                }
//                        })

                ComboBox<MetaAttributeDef> attributeValue = new ComboBox<>("Значение");
                attributeValue.setItems(metacatalogService.getMetacatalog().getMetaAttributes());
                attributeValue.setItemLabelGenerator(MetaAttributeDef::getName);
//                binder
//                        .forField(attributeValue)
//                        .asRequired()
//                        .bind({ grayRuleAction.attributeValue }, { it, v ->
//                if (v != null) {
//                    map = it.logicDef!!.postActions
//                            grayRuleAction = map?.get(key) as GrayRuleAction
//                    grayRuleAction.attributeValue = v
//                    map?.set(key, grayRuleAction)
//                    if (it.logicDef != null) {
//                        it.logicDef = it.logicDef!!.setPostActions(map) as RuleLogicDef
//                    }
//                }
//                        })

                layout.add(scoring, mathOperationBox, attributeKey, attributeValue);
                return layout;
            }));

    private RuleActionComponent component;

    RuleAction(RuleActionComponent component) {
        this.component = component;
    }

    public RuleActionComponent getComponent() {
        return component;
    }
}
