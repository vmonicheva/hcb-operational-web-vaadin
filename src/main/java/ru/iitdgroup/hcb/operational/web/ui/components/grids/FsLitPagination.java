package ru.iitdgroup.hcb.operational.web.ui.components.grids;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.shared.Registration;
import org.vaadin.klaudeta.LitPagination;

public class FsLitPagination extends LitPagination {
	@Override
	public Registration addPageChangeListener(ComponentEventListener<PageChangeEvent> listener) {
		return super.addPageChangeListener(listener);
	}
}
