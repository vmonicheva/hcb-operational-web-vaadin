package ru.iitdgroup.hcb.operational.web.ui.components.cards;

import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;

@CssImport(value = "./styles/opr-entity-card.css", themeFor = "vaadin-details")
public class EntityDetails extends Details implements HasStyle {
    public EntityDetails() {
        addThemeVariants(DetailsVariant.REVERSE);
        setClassName("opr-card-entity");
        getStyle().set("width", "100%");
    }
}
