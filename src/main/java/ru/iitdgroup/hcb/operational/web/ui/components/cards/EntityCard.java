package ru.iitdgroup.hcb.operational.web.ui.components.cards;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.lang.NonNull;

public class EntityCard extends VerticalLayout {
    public EntityCard() {
        setSizeFull();
    }

    public EntityDetails addInfoCard(@NonNull String caption) {
        return addInfoCard(caption, null);
    }

    public EntityDetails addInfoCard(@NonNull String caption, Component content) {
        EntityDetails details = new EntityDetails();
        details.setSummaryText(caption);
        if (content != null) {
            details.setContent(content);
        }
        add(details);
        return details;
    }
}
