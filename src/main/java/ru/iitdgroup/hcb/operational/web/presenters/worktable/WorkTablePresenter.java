package ru.iitdgroup.hcb.operational.web.presenters.worktable;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteMessaging;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;
import ru.iitdgroup.hcb.operational.web.presenters.AbstractWorkspacePresenter;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.AlertFilter;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.DataQuery;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class WorkTablePresenter extends AbstractWorkspacePresenter<ICAlert, AlertFilter> {
    private static final Logger logger = LogManager.getLogger(WorkTablePresenter.class);
    private final List<ICAlert> alerts;
    private final WorkspaceRepository workspaceRepository;
    private final UserService userService;
    private final AlertRepository alertRepository;
    private final int worktableAlertTTL;
    private final Ignite ignite;


    public WorkTablePresenter(WorkspaceRepository workspaceRepository,
                              UserService userService,
                              AlertRepository alertRepository,
                              @Value("${app.worktableAlertTTL}") int worktableAlertTTL,
                              Ignite ignite) {
        super(workspaceRepository, userService);
        this.workspaceRepository = workspaceRepository;
        this.userService = userService;
        this.alertRepository = alertRepository;
        this.worktableAlertTTL = worktableAlertTTL;
        this.ignite = ignite;
        this.alerts = new ArrayList<>();
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Загрузка алертов в локальное хранилище");
        List<ICAlert> alertList = alertRepository.findWorktable(Date.from(Instant.now().minus(this.worktableAlertTTL, ChronoUnit.HOURS)));
        if (alertList != null) {
            alerts.addAll(alertList);
        }
        listenTopic();
        logger.debug("Загрузка алертов в локальное хранилище завершена");
    }

    public List<ICAlert> getInQueueAlertList(@NotNull String status) {
        UserAccount currentUser = userService.getCurrentUser();
        if (currentUser == null) {
            return Collections.emptyList();
        }
        Workspace selectedWorkspace = currentUser.getSelectedWorkspace();
        if (selectedWorkspace == null) {
            return Collections.emptyList();
        }
        String code = selectedWorkspace.getCode();
        return alerts.stream().filter(it -> Objects.equals(it.getStatus(), status) &&
                Objects.equals(it.getWorkspace(), code)).collect(Collectors.toList());
    }

    public int getInQueueAlertCount(@NotNull String status) {
        return alertRepository.findAllByStatus(status).size();
    }

    public List<ICAlert> getMyAlerts() {
        UserAccount currentUser = userService.getCurrentUser();
        if (currentUser == null) {
            return Collections.emptyList();
        }
        return alerts.stream()
                .filter(it ->
                        Objects.equals(it.getOwner(), currentUser.getCode()) &&
                                Objects.equals(it.getWorkspace(),
                                        currentUser.getSelectedWorkspace() != null ? currentUser.getSelectedWorkspace().getCode() : null))
                .collect(Collectors.toList());
    }


    @Scheduled(
            fixedDelay = 60000L
    )
    public void clearOutdatedAlerts() {
        logger.debug("Чистка старых алертов");
        Date ttl = Date.from(Instant.now().minus(worktableAlertTTL, ChronoUnit.HOURS));
        alerts.removeIf(it -> {
            logger.debug("Удален алерт {} из Рабочего стола", it.id);
            return it.alertDate.before(ttl);
        });
    }

    @Scheduled(
            fixedDelay = 60000L
    )
    public void updateAlerts() {
        logger.debug("Обновление алертов");
        List<ICAlert> alertList = alertRepository.findWorktable(Date.from(Instant.now().minus(worktableAlertTTL, ChronoUnit.HOURS)));
        List<Long> alertsIds = getAlertsIds();
        alertList.forEach(it -> {
            if (!alertsIds.contains(it.getId())) {
                alerts.add(it);
            }
        });
        logger.debug("Обновление алертов завершено");
    }

    private List<Long> getAlertsIds() {
        return alerts.stream().map(ICAlert::getId).collect(Collectors.toList());
    }

    private void listenTopic() {
        logger.debug("Слушаю топик для Алертов");
        IgniteMessaging message = ignite.message(ignite.cluster().forRemotes());
        message.localListen("alert", ((uuid, msg) -> {
            if (msg instanceof Long) {
                Date ttl = Date.from(Instant.now().minus(worktableAlertTTL, ChronoUnit.HOURS));
                alertRepository.findById((Long) msg).ifPresent(it -> {
                    if (it.alertDate.after(ttl)) {
                        alerts.add(it);
                    }
                });
            }
            return true;
        }));
    }

    @Override
    public Stream<ICAlert> getList(DataQuery<AlertFilter> dataQuery) {
        throw new NotImplementedException("not implemented");
    }

    @Override
    public int getCount(Optional<AlertFilter> filter) {
        throw new NotImplementedException("not implemented");
    }

    @Override
    public int getTotal() {
        throw new NotImplementedException("not implemented");
    }

}
