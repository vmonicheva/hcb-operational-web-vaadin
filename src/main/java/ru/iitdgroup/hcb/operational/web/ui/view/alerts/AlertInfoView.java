package ru.iitdgroup.hcb.operational.web.ui.view.alerts;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.presenters.worktable.WorkTablePresenter;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.cards.AlertCard;
import ru.iitdgroup.hcb.operational.web.ui.util.NoResourceLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

import java.util.Optional;

/**
 * ICAlert details view
 * Форма детализированной информации по алерту(оповещению)
 */
@Route(value = "alert/info", layout = MainLayout.class)
@CssImport(value = "./styles/opr-card-form.css", themeFor = "vaadin-form-item")
@PageTitle("Интерфейс оперативного реагирования")
public class AlertInfoView extends CommonView implements HasUrlParameter<Long> {
    private final AlertRepository alertRepository;
    private final CardRepository cardRepository;
    private final CardTransactionRepository transactionRepository;
    private final UserService userService;
    private final WorkTablePresenter workTablePresenter;
    private final Breadcrumbs breadcrumbs;
    private final AlertCard alertCard;
    private ICAlert alert;

    public AlertInfoView(AlertRepository alertRepository,
                         CardRepository cardRepository,
                         Breadcrumbs breadcrumbs,
                         CardTransactionRepository transactionRepository,
                         UserService userService,
                         WorkTablePresenter workTablePresenter) {
        super(breadcrumbs);
        this.alertRepository = alertRepository;
        this.cardRepository = cardRepository;
        this.breadcrumbs = breadcrumbs;
        this.transactionRepository = transactionRepository;
        this.userService = userService;
        this.workTablePresenter = workTablePresenter;
        this.alertCard = new AlertCard(alertRepository, cardRepository, this.userService, transactionRepository, this.workTablePresenter);
        VerticalLayout mainLayout = new VerticalLayout();
        center.add(mainLayout);
        NoResourceLayout layout = new NoResourceLayout();
        center.add(layout);
        center.setJustifyContentMode(JustifyContentMode.CENTER);
        center.setAlignItems(Alignment.CENTER);
    }


    @Override
    public void setParameter(BeforeEvent event, Long parameter) {
        breadcrumbs.setBreadcrumb(this.getClass(), parameter, "Оповещениe №" + parameter);
        Optional<ICAlert> optional = alertRepository.findById(parameter);
        optional.ifPresent(it -> alert = it);
        if (alert != null) {
            fillInfo();
        }
    }

    private void fillInfo() {
        center.removeAll();
        alertCard.init(alert);
        center.add(alertCard);
        center.getStyle().set("overflow", "hidden");
    }

}
