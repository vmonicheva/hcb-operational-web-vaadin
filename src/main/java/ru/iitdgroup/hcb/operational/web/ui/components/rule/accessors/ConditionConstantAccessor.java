package ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.SingleConstantDef;

import java.util.Collections;
import java.util.List;

public class ConditionConstantAccessor extends ConditionAccessor<SingleConstantDef> {
    public ConditionConstantAccessor(AccessorDef prevAccessorDef, AccessorDef comparableAccessorDef) {
        super(prevAccessorDef, comparableAccessorDef);
    }

    @Override
    public String getCaption() {
        return "Значение";
    }

    @Override
    public List<Component> getComponents() {
        TextField valueTextField = new TextField("Значение");
        valueTextField.setWidthFull();
//        binder
//                .forField(valueTextField)
//                .withValidator { value, _ ->
//                var r = ValidationResult.ok()
//            when (comparableAccessorDef.type) {
//                AttributeType.INTEGER, AttributeType.DOUBLE, AttributeType.LONG -> {
//                    if (!StringUtils.isNumeric(value)) {
//                        r = ValidationResult.error(i18n.getMessage("incorrectFieldValue"))
//                    }
//                }
//                AttributeType.STRING -> {
//                    if (StringUtils.isEmpty(value)) {
//                        r = ValidationResult.error(i18n.getMessage("incorrectFieldValue"))
//                    }
//                }
//                        else -> {
//                    //TODO дописать
//                    r = ValidationResult.ok()
//                }
//            }
//            r
//        }
//                .bind(
//                {
//                        it.value?.toString()
//                        },
//        { it, v ->
//                it.value = v
//        })

        return Collections.singletonList(valueTextField);
    }
}
