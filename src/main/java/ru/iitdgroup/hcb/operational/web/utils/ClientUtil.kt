package ru.iitdgroup.hcb.operational.web.utils

import ru.iitdgroup.model.hcb.clients.Client

/**
 * Утилитарный класс для работы с клиентом [Client]
 */
object ClientUtil {

    /**
     * Получение полного имени клиента (ФИО) в одну строку
     *
     * @param client клиент
     * @return ФИО клиента
     */
    fun getFullName(client: Client?): String? {
        if (client?.lastName == null
                && client?.lastName == null
                && client?.firstName == null
                && client?.patronymic == null) {
            return ""
        }

        return """
            ${client.lastName ?: ""}
            ${client.firstName ?: ""}
            ${client.patronymic ?: ""}
        """.trimIndent()
    }

    /**
     * TODO как находить?
     *
     * @param client
     * @return
     */
    fun getMainPhone(client: Client): String? {
        return null
    }

}