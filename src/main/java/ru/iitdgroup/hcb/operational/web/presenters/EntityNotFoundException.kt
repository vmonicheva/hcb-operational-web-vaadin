package ru.iitdgroup.hcb.operational.web.presenters

class EntityNotFoundException : RuntimeException()
