package ru.iitdgroup.hcb.operational.web.config

import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException

class NotLdapPrincipalException : HttpClientErrorException(HttpStatus.BAD_REQUEST)