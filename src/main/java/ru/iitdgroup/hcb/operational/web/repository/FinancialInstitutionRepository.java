package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.model.hcb.cards.FinancialInstitution;

import java.util.List;

public interface FinancialInstitutionRepository extends CrudRepository<FinancialInstitution, Long> {

    @Override
    List<FinancialInstitution> findAll();
}
