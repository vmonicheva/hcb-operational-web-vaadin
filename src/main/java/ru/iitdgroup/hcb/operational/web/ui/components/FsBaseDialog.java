package ru.iitdgroup.hcb.operational.web.ui.components;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.Arrays;
import java.util.List;

public class FsBaseDialog extends FsDialog {

    public FsBaseDialog(
            ComponentEventListener<ClickEvent<Button>> okListener,
            ComponentEventListener<ClickEvent<Button>> cancelListener,
            String title,
            String... labels
    ) {
        super(title);
        init(okListener, cancelListener, labels);
    }

    public FsBaseDialog(
            ComponentEventListener<ClickEvent<Button>> okListener,
            String title,
            String... labels
    ) {
        super(title);
        init(okListener, null, labels);
    }

    public FsBaseDialog(
            ComponentEventListener<ClickEvent<Button>> okListener,
            String title,
            List<String> labels
    ) {
        super(title);
        init(okListener, null, labels.toArray());
    }


    public FsBaseDialog(String title, String... labels) {
        super(title);
        init(null, null, labels);
    }

    public FsBaseDialog(
            ComponentEventListener<ClickEvent<Button>> okListener,
            String title,
            String okCaption,
            Button additionalButton,
            String... labels
    ) {
        super(title);
        init(okListener, null, okCaption, additionalButton, labels);
    }

    private void init(
            ComponentEventListener<ClickEvent<Button>> okListener,
            ComponentEventListener<ClickEvent<Button>> cancelListener,
            String okCaption,
            Button additionalButton,
            Object[] labels
    ) {
        VerticalLayout layout = new VerticalLayout();
        VerticalLayout textLayout = new VerticalLayout();
        textLayout.setWidthFull();
        Arrays.stream(labels).forEach(label -> textLayout.add(new Label((String) label)));

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setWidthFull();
        buttons.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        if (okCaption != null) {
            okButton.setText(okCaption);
        }
        okButton.addClickListener(e -> {
            if (okListener != null) {
                okListener.onComponentEvent(e);
            }
            close();
        });
        cancelButton.addClickListener(e -> {
            if (cancelListener != null) {
                cancelListener.onComponentEvent(e);
            }
            close();
        });
        if (additionalButton != null) {
            additionalButton.addClickListener(e -> {
                close();
            });
            okCancelLayout.addComponentAtIndex(1, additionalButton);
        }
        layout.add(textLayout, buttons);
        textLayout.setPadding(false);
        layout.setPadding(false);
        mainLayout.addComponentAtIndex(1, layout);
    }

    private void init(ComponentEventListener<ClickEvent<Button>> okListener, ComponentEventListener<ClickEvent<Button>> cancelListener, Object[] labels) {
        init(okListener, cancelListener, null, null, labels);
    }
}
