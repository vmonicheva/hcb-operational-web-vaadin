package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.model.hcb.cards.CardTransaction;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AlertRepository extends CrudRepository<ICAlert, Long> {
    @Override
    List<ICAlert> findAll();

    Page<ICAlert> findAll(Specification<ICAlert> spec, Pageable pageable);

    @Query("select ica from ICAlert ica " +
            "left join fetch ica.cardTransaction ct " +
            "left join fetch ct.card c " +
            "left join fetch ct.terminal t " +
            "left join fetch c.cardHolder cl " +
            "where ica.id = :id")
    Optional<ICAlert> findById(Long id);

    @Query("select ica from ICAlert ica " +
            "left join fetch ica.cardTransaction ct " +
            "left join fetch ct.card c " +
            "left join fetch c.cardHolder cl " +
            "where cl.id = :id " +
            "ORDER BY ica.lastModified DESC")
    List<ICAlert> findByClientId(Long id, Pageable pageable);

    @Query("select ica from ICAlert ica " +
            "left join fetch ica.cardTransaction ct " +
            "left join fetch ct.card c " +
            "left join fetch c.cardHolder cl " +
            "where ica.status = :status")
    List<ICAlert> findAllByStatus(String status);

    @Query("select ica from ICAlert ica " +
            "left join fetch ica.cardTransaction ct " +
            "left join fetch ct.card c " +
            "left join fetch c.cardHolder cl " +
            "where ica.owner = :owner")
    List<ICAlert> findAllByOwner(String owner);

    @Query("select count(ica) from ICAlert ica " +
            "left join ica.cardTransaction ct " +
            "left join ct.card c " +
            "left join c.cardHolder cl " +
            "where ica.owner = :owner")
    Long countMyAlerts(String owner);

    @Query("select ica from ICAlert ica " +
            "left join fetch ica.cardTransaction ct " +
            "left join fetch ct.card c " +
            "left join fetch c.cardHolder cl " +
            "where ica.alertDate > :date")
    List<ICAlert> findWorktable(Date date);

    List<ICAlert> findAllByCardTransactionIn(List<CardTransaction> transactionList);

    ICAlert findByCardTransaction(CardTransaction transaction);
}
