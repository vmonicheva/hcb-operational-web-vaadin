package ru.iitdgroup.hcb.operational.web.enums

import java.io.Serializable

/**
 * Конкретное право на действие вьюшки
 */
enum class WorkspaceRight : Serializable {

    RULE_CREATE,
    RULE_EDIT,
    RULE_ENABLE,

//    ALERT_STATUS,
//    ALERT_LIST,
//    ALERT_ASSIGN,
//    ALERT_RESOLUTION,
//    ALERT_CLOSED_STATUS

}