package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;

import java.util.List;

public interface WorkspaceRepository extends CrudRepository<Workspace, Long> {

    @Override
    List<Workspace> findAll();
}
