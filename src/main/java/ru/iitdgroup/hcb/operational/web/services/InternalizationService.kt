package ru.iitdgroup.hcb.operational.web.services

import com.ibm.icu.text.Transliterator
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.MessageSource
import org.springframework.stereotype.Service
import java.util.*

/**
 * Сервис для обеспечения интернализации
 */
@Service
open class InternalizationService(
        @Qualifier("I18NMessageSource") var messageSource: MessageSource
) {

    companion object {
        const val CYRILLIC_TO_LATIN = "Russian-Latin/BGN"
    }

    private val toLatinTrans = Transliterator.getInstance(CYRILLIC_TO_LATIN)!!

    open fun getMessage(code: String?): String {
        if (code.isNullOrEmpty())
            return ""
        return messageSource.getMessage(code, null, Locale.forLanguageTag("ru"))
    }

    open fun toLatin(ruText: String): String {
        return toLatinTrans.transliterate(ruText)
    }

    open fun toLatinWordFirstChars(ruText: String): String {
        return toLatin(ruText)
                .trim()
                .split(" ")
                .joinToString("") { it.first().toString() }
                .toUpperCase()
    }
}
