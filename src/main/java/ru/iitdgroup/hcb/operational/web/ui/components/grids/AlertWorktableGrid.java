package ru.iitdgroup.hcb.operational.web.ui.components.grids;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.RouteConfiguration;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.AlertDef;
import ru.iitdgroup.hcb.operational.web.repository.CardRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.ui.util.CssUtil;
import ru.iitdgroup.hcb.operational.web.ui.view.alerts.AlertInfoView;
import ru.iitdgroup.hcb.operational.web.ui.view.customers.ClientInfoView;
import ru.iitdgroup.hcb.operational.web.ui.view.transactions.TransactionInfoView;
import ru.iitdgroup.model.hcb.cards.CardTransaction;
import ru.iitdgroup.model.hcb.cards.HCBCard;
import ru.iitdgroup.model.hcb.clients.Client;

import java.util.*;

public class AlertWorktableGrid extends Grid<ICAlert> {
    protected final Map<String, String> columnMap;
    protected final List<Column<ICAlert>> columns;
    protected final Set<ICAlert> selected = new HashSet<>();
    protected Icon detailedInfo;
    private final CardTransactionRepository cardTransactionRepository;
    private final CardRepository cardRepository;

    public AlertWorktableGrid(Collection<ICAlert> items,
                              CardTransactionRepository cardTransactionRepository,
                              CardRepository cardRepository) {
        this.cardTransactionRepository = cardTransactionRepository;
        this.cardRepository = cardRepository;
        setItems(items);
        columnMap = new HashMap<>();
        columns = new ArrayList<>();
//        CallbackDataProvider<ICAlert, Void> provider = DataProvider
//                .fromCallbacks(query -> personService
//                                .fetch(query.getOffset(), query.getLimit()).stream(),
//                        query -> personService.count());
//        setDataProvider(provider);
        init();
    }


    protected void init() {
        setSelectionMode(SelectionMode.MULTI);
        GridSelectionModel<ICAlert> model = this.getSelectionModel();
        setColumnReorderingAllowed(true);
        addSelectionListener(e -> selected.addAll(e.getAllSelectedItems()));
        addInfoColumn();
        addMainColumns();
        addSettingsColumn();
        getColumns().forEach(this::setResizable);
        setClassNameGenerator(alert -> {
            if ("Черное" .equals(alert.getRuleName())) {
                return "opr-row-blackrule";
            } else if (AlertDef.ESCALATED.getDisplayName().equals(AlertDef.getDisplayName(alert.getStatus()))) {
                return "opr-row-escalated";
            } else {
                return null;
            }
        });
    }

    private void setResizable(Column<ICAlert> column) {
        column.setResizable(true);
        Element parent = column.getElement().getParent();
        while (parent != null
                && "vaadin-grid-column-group" .equals(parent.getTag())) {
            parent.setProperty("resizable", "true");
            parent = parent.getParent();
        }
    }

    protected void addMainColumns() {
        Column<ICAlert> idColumn = addColumn(new ComponentRenderer<>((item) -> {
                    String route = RouteConfiguration.forSessionScope().getUrl(AlertInfoView.class, item.getId());
                    return new Anchor(route, String.valueOf(item.getId()));
                })
                , "ID")
                .setHeader("ID")
                .setFlexGrow(0)
                .setWidth("100px")
                .setKey("id");
        Comparator<ICAlert> icAlertComparator = (o1, o2) -> {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            return Long.compare(o1.getId(), o2.getId());
        };
        idColumn.setComparator(icAlertComparator);
        columnMap.put(idColumn.getKey(), "ID");
        columns.add(idColumn);

        Grid.Column<ICAlert> cuidColumn = addColumn((ValueProvider<ICAlert, String>) alert -> {
            CardTransaction transaction = getCardTransaction(alert);
            if (transaction == null) {
                return "";
            }
            HCBCard card = transaction.getCard();
            if (card == null) {
                return "";
            }
            HCBCard fetched = cardRepository.findFetchedById(card.getId());
            Client cardHolder = fetched.getCardHolder();
            if (cardHolder == null) {
                return "";
            }
            return String.valueOf(cardHolder.getCuid());
        })
                .setHeader("CUID")
                .setFlexGrow(0)
                .setWidth("150px")
                .setKey("cuid");
        columnMap.put(cuidColumn.getKey(), "CUID");
        columns.add(cuidColumn);

        Grid.Column<ICAlert> clientColumn = addColumn(new ComponentRenderer<>((alert) -> {
            CardTransaction transaction = getCardTransaction(alert);
            if (transaction == null) {
                return new Label("");
            }
            HCBCard card = transaction.getCard();
            if (card == null) {
                return new Label("");
            }
            HCBCard fetched = cardRepository.findFetchedById(card.getId());
            Client cardHolder = fetched.getCardHolder();
            if (cardHolder == null) {
                return new Label("");
            }
            String route = RouteConfiguration.forSessionScope().getUrl(ClientInfoView.class, cardHolder.getId());
            return new Anchor(route, String.valueOf(cardHolder.getFirstName()));
        }))
                .setHeader("Клиент")
                .setKey("client");
        columnMap.put(clientColumn.getKey(), "Клиент");
        columns.add(clientColumn);

//        Grid.Column<ICAlert> transactionStatusColumn = addColumn((ValueProvider<ICAlert, String>) this::getTransaction, "Статус транзакции")
//                .setHeader("Статус транзакции")
//                .setKey("transactionStatus");
//        columnMap.put(transactionStatusColumn.getKey(), "Статус транзакции");

        Grid.Column<ICAlert> transactionColumn = addColumn(new ComponentRenderer<>((alert) -> {
            CardTransaction transaction = getCardTransaction(alert);
            if (transaction == null) {
                return new Label("");
            }
            String route = RouteConfiguration.forSessionScope().getUrl(TransactionInfoView.class, transaction.getId());
            return new Anchor(route, String.valueOf(transaction.getId()));
        }))
                .setHeader("Транзакция")
                .setKey("transaction");
        columnMap.put(transactionColumn.getKey(), "Транзакция");
        columns.add(transactionColumn);

        Column<ICAlert> ruleColumn =
                addColumn(ICAlert::getRuleName, "Правило")
                        .setHeader("Правило")
                        .setFlexGrow(0)
                        .setWidth("150px")
                        .setKey("rule");
        columnMap.put(ruleColumn.getKey(), "Правило");
        columns.add(ruleColumn);

        Column<ICAlert> scoringColumn =
                addColumn(ICAlert::getScore, "Скоринг")
                        .setHeader("Скоринг")
                        .setKey("scoring");
        columnMap.put(scoringColumn.getKey(), "Скоринг");
        columns.add(scoringColumn);

        Column<ICAlert> statusColumn =
                addColumn((ValueProvider<ICAlert, String>) icAlert -> AlertDef.getDisplayName(icAlert.getStatus()), "Статус")
                        .setHeader("Статус")
                        .setKey("status");
        columnMap.put(statusColumn.getKey(), "Статус");
        columns.add(statusColumn);
    }

    private CardTransaction getCardTransaction(ICAlert alert) {
        CardTransaction cardTransaction = alert.getCardTransaction();
        if (cardTransaction == null) {
            return null;
        }
        Optional<CardTransaction> optional = cardTransactionRepository.findFetchedById(cardTransaction.getId());
        if (optional.isEmpty()) {
            return null;
        }
        return optional.get();
    }

    protected void addSettingsColumn() {
        Icon headerComponent = new Icon(VaadinIcon.COG_O);
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setOpenOnClick(true);
        contextMenu.setTarget(headerComponent);
        columns.forEach(alertColumn -> {
            String key = alertColumn.getKey();
            Checkbox checkbox = new Checkbox(columnMap.get(key));
            checkbox.setValue(getColumnByKey(key).isVisible());
            checkbox.addClickListener(click -> {
                getColumnByKey(key).setVisible(checkbox.getValue());
            });
            contextMenu.addItem(checkbox);
        });
        headerComponent.setSize("20px");
        CssUtil.setCursorPointer(headerComponent);
        addColumn((ValueProvider<ICAlert, String>) alert -> "").setHeader(headerComponent).setFlexGrow(0).setWidth("60px");
    }

    protected void addInfoColumn() {
        addColumn(new ComponentRenderer<>((item) -> {
            detailedInfo = new Icon(VaadinIcon.SEARCH);
            detailedInfo.addClickListener(e -> UI.getCurrent().navigate(AlertInfoView.class, item.getId()));
            CssUtil.setCursorPointer(detailedInfo);
            detailedInfo.setSize("15px");
            return detailedInfo;
        }))
                .setFlexGrow(0)
                .setWidth("50px");
    }

}
