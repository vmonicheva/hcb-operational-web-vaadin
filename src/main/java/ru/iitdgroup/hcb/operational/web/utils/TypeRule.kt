package ru.iitdgroup.hcb.operational.web.utils

object TypeRule {
    const val grayRulePrefix = "gr_"
    const val whiteRulePrefix = "wr_"
    const val blackRulePrefix = "br_"
    fun getPrefix(group: String) : String{
        return when (group) {
            "Белые" -> {
                whiteRulePrefix
            }
            "Черные" -> {
                blackRulePrefix
            }
            else -> grayRulePrefix
        }
    }
}