package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.model.hcb.clients.Client;

import java.util.List;

public interface ClientRepository extends CrudRepository<Client, Long> {
    @Override
    List<Client> findAll();
}
