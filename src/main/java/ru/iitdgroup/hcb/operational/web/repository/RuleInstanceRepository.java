package ru.iitdgroup.hcb.operational.web.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;

import java.util.List;

public interface RuleInstanceRepository extends CrudRepository<RuleInstance, Long> {
    boolean existsByPosition(int var1);

//    @Query("select max(ri.position) from RuleInstance ri")
//    @Nullable
//    Integer maxIndex();
//
//    @Query("select count(ri) from RuleInstance ri where ri.group = ?1")
//    @Nullable
//    Long countByGroup(@NotNull RuleGroup var1);
//
//    @Query("select count(ri) from RuleInstance ri  left join RuleGroup rg on ri.group = rg  where rg.workspace = ?1")
//    long countByWorkspace(@NotNull Workspace var1);
//
//    @Query("select count(ri) from RuleInstance ri  left join RuleGroup rg on ri.group = rg  where rg.workspace = ?1 and ri.group.code = ?2")
//    long countByWorkspaceAndGroup(@NotNull Workspace var1, @NotNull String var2);

    @NotNull
    List<RuleInstance> findAllByGroupWorkspace(@NotNull Workspace var1);
}
