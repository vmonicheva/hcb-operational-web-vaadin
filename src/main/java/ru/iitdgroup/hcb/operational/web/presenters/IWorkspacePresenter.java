package ru.iitdgroup.hcb.operational.web.presenters;

import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.BaseFilter;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.DataQuery;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public interface IWorkspacePresenter<T, F extends BaseFilter> {

   List<Workspace> getWorkspaces();

   Workspace getSelectedWorkspace();

   void selectWorkspace(Workspace var1);

   UserAccount getCurrentUser();

   Stream<T> getList(DataQuery<F> dataQuery);

   int getCount(Optional<F> filter);

   int getTotal();
}
