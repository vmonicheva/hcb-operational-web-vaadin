package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;
import ru.iitdgroup.hcb.operational.web.repository.RuleInstanceRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.alerts.AlertView;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Route(value = "rules/white", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class WhiteRulesView extends RulesView {

    public WhiteRulesView(Breadcrumbs breadcrumbs,
                          RuleInstanceRepository ruleInstanceRepository,
                          UserService userService) {
        super(breadcrumbs, ruleInstanceRepository, userService);
        breadcrumbs.setBreadcrumb(AlertView.class, "Правила", this.getClass(), "Белые правила");
    }

    @Override
    protected List<RuleInstance> getItems() {
        UserAccount currentUser = userService.getCurrentUser();
        if (currentUser == null) {
            return Collections.emptyList();
        }
        Workspace workspace = currentUser.getSelectedWorkspace();
        if (workspace == null) {
            return Collections.emptyList();
        }
        List<RuleInstance> allByGroupWorkspace = ruleInstanceRepository.findAllByGroupWorkspace(workspace);

        return allByGroupWorkspace.stream().
                filter(it -> Objects.equals("white", it.getGroup().getCode()))
                .collect(Collectors.toList());
    }
}
