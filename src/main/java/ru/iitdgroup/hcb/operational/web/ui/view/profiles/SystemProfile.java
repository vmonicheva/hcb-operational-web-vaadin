package ru.iitdgroup.hcb.operational.web.ui.view.profiles;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

@Route(value = "profile/system", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class SystemProfile extends Profile {
    public SystemProfile(Breadcrumbs breadcrumbs) {
        super(breadcrumbs);
        breadcrumbs.setBreadcrumb(this.getClass(), "Профили", this.getClass(), "Системный");
    }

    @Override
    protected void init() {
        super.init();

    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnDeleteVirtualAttrListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnEditVirtualAttrListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnOkVirtualAttrListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnDeleteListListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnEditListListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnOkListListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnDeleteMetricListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnEditMetricListener() {
        return null;
    }

    @Override
    protected ComponentEventListener<ClickEvent<Button>> getOnOkMetricListener() {
        return null;
    }

    @Override
    protected String getAuthor() {
        return null;
    }

    @Override
    protected String getCreateDate() {
        return null;
    }

    @Override
    protected String getDescription() {
        return null;
    }

    @Override
    protected String getProfileId() {
        return null;
    }

    @Override
    protected String getProfileType() {
        return "Системный";
    }


}
