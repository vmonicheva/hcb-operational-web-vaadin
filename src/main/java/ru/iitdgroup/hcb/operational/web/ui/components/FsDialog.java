package ru.iitdgroup.hcb.operational.web.ui.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class FsDialog extends Dialog {
    protected HorizontalLayout buttonsLayout;
    protected VerticalLayout mainLayout;
    protected Button okButton;
    protected Button cancelButton;
    protected HorizontalLayout okCancelLayout;
    private String name;

    public FsDialog(String name) {
        this.name = name;
    }

    public void init() {
        mainLayout = new VerticalLayout();
        mainLayout.setPadding(false);
        Label label = new Label(name);
        label.setClassName("fs-dialog-label");

        HorizontalLayout labelLayout = new HorizontalLayout(label);
        labelLayout.setAlignItems(FlexComponent.Alignment.BASELINE);
        labelLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
        Button closeWindow = new Button(new Icon(VaadinIcon.CLOSE));
        closeWindow.setClassName("fs-dialog-close-button");
        closeWindow.addThemeVariants(ButtonVariant.LUMO_SMALL);
        closeWindow.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        closeWindow.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
        closeWindow.addClickListener(e -> {
            close();
        });
        labelLayout.add(closeWindow);
        labelLayout.setClassName("fs-dialog-label-layout");
        labelLayout.setWidthFull();
        mainLayout.add(labelLayout);
        mainLayout.setPadding(false);
//		mainLayout.getStyle().set("background-color", "var(--soup-dialog-overlay-background)");
        okButton = new Button("ОК");
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        cancelButton = new Button("Отмена");
        okCancelLayout = new HorizontalLayout(okButton, cancelButton);
        okCancelLayout.setPadding(false);
        okCancelLayout.getStyle().set("margin-left", "10px");
        buttonsLayout = new HorizontalLayout(okCancelLayout);

        buttonsLayout.setPadding(false);
        buttonsLayout.setWidthFull();
        mainLayout.add(buttonsLayout);
        buttonsLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

        mainLayout.setSizeFull();
        add(mainLayout);
        open();
    }

    public HorizontalLayout getOkCancelLayout() {
        return okCancelLayout;
    }

    public HorizontalLayout getButtonsLayout() {
        return buttonsLayout;
    }

    public VerticalLayout getMainLayout() {
        return mainLayout;
    }

    public Button getOkButton() {
        return okButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }
}
