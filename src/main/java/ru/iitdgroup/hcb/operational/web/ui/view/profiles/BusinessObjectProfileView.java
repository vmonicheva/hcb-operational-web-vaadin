package ru.iitdgroup.hcb.operational.web.ui.view.profiles;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.CommonListComponent;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

@Route(value = "profile/business-object", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class BusinessObjectProfileView extends CommonView {
    public BusinessObjectProfileView(Breadcrumbs breadcrumbs) {
        super(breadcrumbs);
        breadcrumbs.setBreadcrumb(SystemProfile.class, "Профили", this.getClass(), "Бизнес-объекта");
        CommonListComponent commonListComponent = new CommonListComponent();
        Grid<Object> businessObjectGrid = new Grid<>();
        VerticalLayout verticalLayout = new VerticalLayout(businessObjectGrid);
        verticalLayout.setPadding(false);
        verticalLayout.setSizeFull();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button create = new Button("Создать");
        create.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        buttonsLayout.add(create);
        buttonsLayout.add(new Button("Редактировать"));
        buttonsLayout.add(new Button("Удалить"));
        verticalLayout.add(buttonsLayout);
        commonListComponent.getContentLayout().add(verticalLayout);
        center.add(commonListComponent);
    }

}
