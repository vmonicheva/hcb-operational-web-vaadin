package ru.iitdgroup.hcb.operational.web.presenters.customers

import org.springframework.data.jpa.domain.Specification
import ru.iitdgroup.hcb.operational.web.QueryCount
import ru.iitdgroup.model.hcb.clients.Client
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

/**
 * Составной предикат для поиска объектов по имени таба и полю поиска
 */
class CustomerSpecification(val filter: CustomerFilter) : Specification<Client>, QueryCount {

    override fun toPredicate(root: Root<Client>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
        var predicateTextSearch: Predicate? = null
        if (!filter.text.isEmpty()) {
            predicateTextSearch = try {
                filter.text.toLong();
                val predicateCuid = criteriaBuilder.equal(root.get<Any>("cuid"), filter.text)
                val predicateIdent = criteriaBuilder.equal(root.get<Any>("ident"), filter.text)
                criteriaBuilder.or(predicateCuid, predicateIdent)
            } catch (numberFormatException: NumberFormatException) {
                val predicateFirstName = criteriaBuilder.equal(root.get<Any>("firstName"), filter.text)
                val predicateMiddleName = criteriaBuilder.equal(root.get<Any>("lastName"), filter.text)
                val predicateSecondName = criteriaBuilder.equal(root.get<Any>("middleName"), filter.text)
                val predicateIdent = criteriaBuilder.equal(root.get<Any>("ident"), filter.text)
                criteriaBuilder.or(predicateFirstName, predicateMiddleName, predicateSecondName, predicateIdent)
            }
        }
        return if (!filter.text.isEmpty())
            predicateTextSearch
        else
            null
    }

    override fun currentQueryIsCountRecords(criteriaQuery: CriteriaQuery<*>): Boolean {
        return criteriaQuery.resultType.name == "java.lang.Long" ||
                criteriaQuery.resultType == Long::class.javaPrimitiveType
    }

}