package ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import ru.iitdgroup.hcb.operational.web.enums.ResultQuantityType;
import ru.iitdgroup.hcb.operational.web.enums.WorkspaceCategory;
import ru.iitdgroup.nwng.model.metacatalog.def.MetaAttributeDef;
import ru.iitdgroup.nwng.model.metric.model.def.MetricDef;
import ru.iitdgroup.nwng.model.metric.model.def.ReturnTypeDef;
import ru.iitdgroup.nwng.model.profile.MockProfiles;
import ru.iitdgroup.nwng.model.profile.model.def.AbstractProfileValueDef;
import ru.iitdgroup.nwng.model.profile.model.def.AttributeProfileDef;
import ru.iitdgroup.nwng.model.profile.model.def.ProfileListDef;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.MetricValueAccessorDef;
import ru.iitdgroup.nwng.model.rule.accessdef.ProfileAccessorDef;

import java.util.*;
import java.util.stream.Collectors;

import static ru.iitdgroup.hcb.operational.web.enums.WorkspaceCategory.*;

public class ConditionAttributeProfileAccessor extends ConditionAccessor<ProfileAccessorDef> {
    private final List<MetaAttributeDef> metaAttributes;

    public ConditionAttributeProfileAccessor(AccessorDef prevAccessorDef,
                                             AccessorDef comparableAccessorDef,
                                             List<MetaAttributeDef> metaAttributes) {
        super(prevAccessorDef, comparableAccessorDef);
        this.metaAttributes = metaAttributes;
    }

    @Override
    public String getCaption() {
        return "Профиль бизнес-атрибута";
    }

    @Override
    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>();
        Binder<MetricValueAccessorDef> metricValueBinder = new Binder<>();
        List<Component> metricFields = getMetricFields(metricValueBinder);
        metricFields.forEach(it -> it.setVisible(false));

        List<Component> listFields = getListFields();
        List<Component> valueFields = getValueFields();
        components.addAll(metricFields);
        components.addAll(listFields);
        components.addAll(valueFields);
        components.forEach(it -> it.setVisible(false));

        ComboBox<WorkspaceCategory<? extends ProfileAccessorDef>> category = new ComboBox<>("Раздел профиля");
        category.setWidthFull();
        category.setItems(WorkspaceCategory.getCategories());
        category.setValue(METRIC);

        category.setItemLabelGenerator(WorkspaceCategory::getDisplayName);
        category.addValueChangeListener(event -> {
            components.forEach(it -> it.setVisible(false));
            WorkspaceCategory<? extends ProfileAccessorDef> value = event.getValue();
            if (value.equals(METRIC)) {
                metricFields.forEach(it -> it.setVisible(true));
                return;
            }
            if (value.equals(LISTS)) {
                listFields.forEach(it -> it.setVisible(true));
                return;
            }
            if (value.equals(VALUES)) {
                valueFields.forEach(it -> it.setVisible(true));
                return;
            }
        });
        List<Component> result = new ArrayList<>();
        result.add(category);
        result.addAll(components);
        return result;
    }

    private List<Component> getMetricFields(Binder<MetricValueAccessorDef> binder) {
        ComboBox<MetaAttributeDef> metaAttrBox = new ComboBox<>("Наименование бизнес-объекта");
        metaAttrBox.setWidthFull();
        ListDataProvider<MetaAttributeDef> metaAttrDP = new ListDataProvider<>(metaAttributes);
        metaAttrDP.setFilter(ma ->
                MockProfiles.PROFILE_DEFS.stream()
                        .anyMatch(it -> it.getForAttribute().getGuid() == ma.getGuid()));

        metaAttrBox.setDataProvider(metaAttrDP);
        metaAttrBox.setItemLabelGenerator(MetaAttributeDef::getName);
        if (!metaAttributes.isEmpty()) {
            metaAttrBox.setValue(metaAttributes.get(0));
        }
//        binder.forField(metaAttrBox).bind(
//                {
//                        it.metricDef?.outAttribute
//                },
//        { it, v ->
//                it.metricDef.outAttribute = v
//        })

        ComboBox<ResultQuantityType> resultQuantityTypeBox = new ComboBox<>("Результат");
        resultQuantityTypeBox.setWidthFull();
        resultQuantityTypeBox.setItems(ResultQuantityType.values());
        TextField resultQuantityN = new TextField("Кол-во");
        Checkbox resultQuantityDistinct = new Checkbox("Уникальность");
        ComboBox<MetaAttributeDef> resultQuantityKey = new ComboBox<>("Ключ");
        resultQuantityKey.setWidthFull();
        ComboBox<ReturnTypeDef> returnTypeBox = new ComboBox<>("Функция");
        returnTypeBox.setWidthFull();
        ComboBox<MetricDef> metricBox = new ComboBox<>("Метрика");
        metricBox.setWidthFull();

        ListDataProvider<MetricDef> metricDefDP = new ListDataProvider<>(MockProfiles.METRIC_DEFS);
        metricBox.setDataProvider(metricDefDP);
        metricBox.setItemLabelGenerator(MetricDef::getName);
//        binder.forField(metricBox).bind(
//                {
//                        it.metricDef?.metric
//                },
//        { it, v ->
//                it.metricDef?.metric = v
//        }
//        )

        setMetricFilter(metaAttrBox.getValue(), metricBox, metricDefDP);
        metaAttrBox.addValueChangeListener(event ->
                setMetricFilter(event.getValue(), metricBox, metricDefDP));

        resultQuantityTypeBox.setItemLabelGenerator(ResultQuantityType::getName);
        resultQuantityTypeBox.setValue(ResultQuantityType.STAT_GET);

        returnTypeBox.setDataProvider(new ListDataProvider<>(Arrays.stream(ReturnTypeDef.values()).filter(it -> it != ReturnTypeDef.IN).collect(Collectors.toList())));

        returnTypeBox.setItemLabelGenerator(it -> {
            switch (it) {
                case COUNT:
                    return "Кол-во";
                case IN:
                    return "IN";
                case MIN:
                    return "Минимальное значение";
                case MAX:
                    return "Максимальное значение";
                case AVERAGE:
                    return "Среднее значение";
                case STDEV:
                    return "Среднеквадратическое отклонение";
                case NSTDEVS:
                    return "Количество стандартных отклонений (NSTDEVS)";
                case SUM:
                    return "Сумма";
                default:
                    return "?";
            }
        });
//        binder.forField(returnTypeBox).bind(
//                {
//                        it.metricDef?.returnType
//                },
//        { it, v ->
//                it.metricDef.returnType = v
//        }
//        )

        return Arrays.asList(
                metaAttrBox,
                metricBox,
                resultQuantityTypeBox,
                returnTypeBox);
    }

    private List<Component> getListFields() {
        ComboBox<ProfileListDef> comboBox = new ComboBox<>("Наименование списка");
        comboBox.setWidthFull();
        comboBox.setItemLabelGenerator(ProfileListDef::getName);
        List<AttributeProfileDef> profileDefs = MockProfiles.PROFILE_DEFS;
        if (!profileDefs.isEmpty()) {
            AttributeProfileDef attributeProfileDef = profileDefs.get(0);
            if (attributeProfileDef != null) {
                Map<Long, ProfileListDef> profileLists = attributeProfileDef.getProfileLists();
                if (!profileLists.isEmpty()) {
                    Collection<ProfileListDef> values = profileLists.values();
                    comboBox.setItems(values);
                }
            }
        }
        return Collections.singletonList(comboBox);
    }

    private List<Component> getValueFields() {
        ComboBox<AbstractProfileValueDef> comboBox = new ComboBox<>("Наименование переменной");
        comboBox.setWidthFull();
        comboBox.setItemLabelGenerator(AbstractProfileValueDef::getName);
        List<AttributeProfileDef> profileDefs = MockProfiles.PROFILE_DEFS;
        if (!profileDefs.isEmpty()) {
            AttributeProfileDef attributeProfileDef = profileDefs.get(0);
            if (attributeProfileDef != null) {
                Map<Long, AbstractProfileValueDef> profileValues = attributeProfileDef.getProfileValues();
                if (!profileValues.isEmpty()) {
                    Collection<AbstractProfileValueDef> values = profileValues.values();
                    comboBox.setItems(values);
                }
            }
        }
        return Collections.singletonList(comboBox);
    }

    void setMetricFilter(MetaAttributeDef forAttributeDef, ComboBox<MetricDef> metricBox, ListDataProvider<MetricDef> metricDefDP) {
        if (forAttributeDef == null) {
            return;
        }
        List<AttributeProfileDef> filteredProfiles = MockProfiles.PROFILE_DEFS.stream().filter(it -> it.getForAttribute().getGuid() == forAttributeDef.getGuid()).collect(Collectors.toList());
        if (filteredProfiles.isEmpty()) {
            return;
        }
        AttributeProfileDef attributeProfileDef = filteredProfiles.get(0);
        if (attributeProfileDef == null) {
            return;
        }
        Map<Long, MetricDef> profileMetrics = attributeProfileDef.getProfileMetrics();
        if (profileMetrics == null || profileMetrics.isEmpty()) {
            return;
        }
        Collection<MetricDef> values = profileMetrics.values();
        if (values.isEmpty()) {
            return;
        }

        metricBox.setValue(values.iterator().next());
        metricDefDP.setFilter(m -> filteredProfiles.stream()
                .anyMatch(it -> it.getProfileMetrics().containsKey(m.getGuid()))
        );
    }
}
