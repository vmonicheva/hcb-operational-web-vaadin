package ru.iitdgroup.hcb.operational.web.utils

import ru.iitdgroup.model.hcb.clients.Address

/**
 * Утилитарный класс для работы с объектом адреса [Address]
 */
object AddressUtil {

    /**
     * Преобразует объект адреса в строчку в правильном порядке субъектов (страна, город, улица и т.д.)
     *
     * @param address объект адреса
     * @return адрес строкой
     */
    fun getFullAddressString(address: Address): String {
        return """
            ${address.countryText}
            ${address.cityText ?: address.districtText}
            ${address.streetText}
            ${address.houseText}
            ${address.flatText}
        """.trimIndent()
    }

}