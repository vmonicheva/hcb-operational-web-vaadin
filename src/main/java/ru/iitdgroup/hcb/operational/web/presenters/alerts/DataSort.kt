package ru.iitdgroup.hcb.operational.web.presenters.alerts

/**
 * Класс предназначен для внедрения сортировки в объект поиска
 */
open class DataSort {
    private var propertyName: String? = null

    private var descending = false

    constructor(propertyName: String?, descending: Boolean) {
        this.propertyName = propertyName
        this.descending = descending
    }


    open fun getPropertyName(): String? {
        return propertyName
    }

    open fun setPropertyName(propertyName: String?) {
        this.propertyName = propertyName
    }

    open fun isDescending(): Boolean {
        return descending
    }

    open fun setDescending(descending: Boolean) {
        this.descending = descending
    }


}