package ru.iitdgroup.hcb.operational.web.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(
        name = "FS_USER_ACCOUNT",
        indexes = {@Index(
                columnList = "code",
                unique = true,
                name = "uk_code"
        )}
)
public final class UserAccount extends AbstractNamedEntity {
    @Nullable
    @OneToOne
    private Workspace selectedWorkspace;
    private boolean locked;
    @NotNull
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<UserRole> roles;
    @NotNull
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<String> adGroups;

    @Nullable
    public Workspace getSelectedWorkspace() {
        return selectedWorkspace;
    }

    public void setSelectedWorkspace(@Nullable Workspace selectedWorkspace) {
        this.selectedWorkspace = selectedWorkspace;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @NotNull
    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(@NotNull List<UserRole> roles) {
        this.roles = roles;
    }

    @NotNull
    public List<String> getAdGroups() {
        return adGroups;
    }

    public void setAdGroups(@NotNull List<String> adGroups) {
        this.adGroups = adGroups;
    }
}
