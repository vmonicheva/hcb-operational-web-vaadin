package ru.iitdgroup.hcb.operational.web.presenters.alerts

import com.vaadin.flow.function.SerializablePredicate
import ru.iitdgroup.hcb.operational.web.entity.ICAlert


open class AlertGridSearchPredicate(
        text: String,
        private val status: String? = null
) : SerializablePredicate<ICAlert> {

    private val text: String = text.trim().toLowerCase()

    override fun test(rd: ICAlert): Boolean {

        val statusResolution = if (status != null) {
            rd.status?.toString() == status
        } else {
            true
        }
        val result = if (text != "")
            rd.status?.toString()?.toLowerCase()?.contains(text)!!
                    || rd.id?.toString()?.toLowerCase()?.contains(text)!!
        else
            true
        return result && statusResolution
    }



}
