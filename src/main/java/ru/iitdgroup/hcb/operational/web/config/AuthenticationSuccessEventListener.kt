package ru.iitdgroup.hcb.operational.web.config

import org.apache.logging.log4j.LogManager
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl
import ru.iitdgroup.hcb.operational.web.entity.UserAccount
import ru.iitdgroup.hcb.operational.web.repository.UserAccountRepository
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository
import ru.iitdgroup.hcb.operational.web.services.RightManager
import java.util.*

/**
 * Листенер успешнных авторизаций для сохранения пользователя в БД из AD
 */
@Configuration
open class  AuthenticationSuccessEventListener(
        private val userAccountRepository: UserAccountRepository,
        private val workspaceRepository: WorkspaceRepository,
        private val rightManager: RightManager
) : ApplicationListener<AuthenticationSuccessEvent> {

    companion object {
        private val LOGGER = LogManager.getLogger(AuthenticationSuccessEventListener::class.java)
    }

    override fun onApplicationEvent(e: AuthenticationSuccessEvent) {
        if (e.authentication.principal !is LdapUserDetailsImpl) {
            val exception = NotLdapPrincipalException()
            LOGGER.error("not ldap principals", e, exception)
            throw exception
        }
        val userDetailsImpl = e.authentication.principal as LdapUserDetailsImpl
        val username = userDetailsImpl.username
        var userAccount = userAccountRepository.findByCode(username)
        // Если пользователя не нашли, то создаем его
        if (userAccount == null) {
            userAccount = UserAccount()
            userAccount.code = username
            userAccount.name = extractCN(userDetailsImpl.dn) ?: username
            userAccount.guid = UUID.randomUUID()
        }
        if (userAccount.selectedWorkspace == null) {
            userAccount.selectedWorkspace = workspaceRepository.findAll().first()
        }
        userAccount.adGroups.clear()
        userAccount.adGroups.addAll(e.authentication.authorities.map { it.authority })
//        rightManager.resetCache(userAccount)
        userAccountRepository.save(userAccount)
    }

    private fun splitDN(dn: String): Map<String, String> {
        return dn
                .split(",")
                .associate {
                    val split = it.split("=")
                    Pair(split[0], split[1])
                }
    }

    private fun extractCN(dn: String): String? {
        return splitDN(dn)["CN"]
    }
}