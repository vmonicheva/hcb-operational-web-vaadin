package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;
import org.jetbrains.annotations.NotNull;
import ru.iitdgroup.hcb.operational.web.entity.RuleGroup;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.repository.RuleGroupRepository;
import ru.iitdgroup.hcb.operational.web.repository.RuleInstanceRepository;
import ru.iitdgroup.hcb.operational.web.services.MetacatalogService;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.rule.*;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Route(value = "rules/info", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class RuleInfoView extends CommonView implements HasUrlParameter<Long> {
    public static final String RULE = "rule";
    public static final String GROUP = "group";
    private final RuleInstanceRepository ruleInstanceRepository;
    private final RuleGroupRepository ruleGroupRepository;
    private final Breadcrumbs breadcrumbs;
    private final MetacatalogService metacatalogService;
    private RuleInstance rule;
    private RuleInfoLayout ruleInfoLayout;
    private RulePreConditionLayout rulePreConditionLayout;
    private RuleConditionLayout ruleConditionLayout;
    private RuleActionLayout ruleActionLayout;
    private RuleExpertLayout ruleExpertLayout;

    public RuleInfoView(Breadcrumbs breadcrumbs,
                        RuleInstanceRepository ruleInstanceRepository,
                        RuleGroupRepository ruleGroupRepository, MetacatalogService metacatalogService) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
        this.ruleInstanceRepository = ruleInstanceRepository;
        this.ruleGroupRepository = ruleGroupRepository;
        this.metacatalogService = metacatalogService;
    }

    private void init() {
        VerticalLayout cardLayout = new VerticalLayout();
        cardLayout.setId("fs-rule-card-layout");
        cardLayout.setSizeFull();
        cardLayout.getStyle().set("padding-top", "0");

        HorizontalLayout topRow = initTopRow();
        cardLayout.add(topRow);

        VerticalLayout innerLayout = new VerticalLayout();
        innerLayout.getStyle().set("background-color", "white");
        innerLayout.setSizeFull();
        Tabs tabs = new Tabs();
        innerLayout.add(tabs);
        Tab info = new Tab("Информация");
        tabs.setWidthFull();
        Tab preCondition = new Tab("Предусловие");
        Tab condition = new Tab("Условие");
        Tab actions = new Tab("Действия");
        Tab expert = new Tab("Экспертный режим");
        tabs.add(info, preCondition, condition, actions, expert);
        VerticalLayout content = new VerticalLayout();
        content.getStyle().set("padding-top", "0");
        content.setSizeFull();
        initContent(content);

        setContentNotVisible();
        ruleInfoLayout.setVisible(true);
        tabs.addSelectedChangeListener(e -> {
            Tab selectedTab = e.getSelectedTab();
            setContentNotVisible();
            if (selectedTab.equals(info)) {
                ruleInfoLayout.setVisible(true);
                return;
            }
            if (selectedTab.equals(preCondition)) {
                rulePreConditionLayout.setVisible(true);
                return;
            }
            if (selectedTab.equals(condition)) {
                ruleConditionLayout.setVisible(true);
                return;
            }
            if (selectedTab.equals(actions)) {
                ruleActionLayout.setVisible(true);
                return;
            }
            if (selectedTab.equals(expert)) {
                ruleExpertLayout.setVisible(true);
            }
        });
        innerLayout.add(content);
        cardLayout.add(innerLayout);
        center.add(cardLayout);
    }

    private void initContent(VerticalLayout content) {
        ruleInfoLayout = new RuleInfoLayout(rule);
        content.add(ruleInfoLayout);
        rulePreConditionLayout = new RulePreConditionLayout();
        content.add(rulePreConditionLayout);
        ruleConditionLayout = new RuleConditionLayout(rule);
        content.add(ruleConditionLayout);
        ruleActionLayout = new RuleActionLayout();
        content.add(ruleActionLayout);
        ruleExpertLayout = new RuleExpertLayout();
        content.add(ruleExpertLayout);
    }

    private void setContentNotVisible() {
        ruleInfoLayout.setVisible(false);
        rulePreConditionLayout.setVisible(false);
        ruleConditionLayout.setVisible(false);
        ruleActionLayout.setVisible(false);
        ruleExpertLayout.setVisible(false);
    }

    @NotNull
    private HorizontalLayout initTopRow() {
        HorizontalLayout topRow = new HorizontalLayout();
        topRow.setAlignItems(Alignment.CENTER);
        topRow.setWidthFull();
        Label ruleName = new Label(rule != null ? rule.getName() : "Новое правило");
        ruleName.setClassName("fs-rule-name-label");
        ruleName.setWidthFull();
        topRow.add(ruleName);
        HorizontalLayout topButtons = new HorizontalLayout();
        topButtons.setWidthFull();
        Button save = new Button("Сохранить");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        topButtons.add(save);
        topButtons.add(new Button("Скопировать"));
        topButtons.add(new Button("Удалить"));
        topButtons.setJustifyContentMode(JustifyContentMode.END);
        topRow.add(topButtons);
        return topRow;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Long param) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parameterMap = queryParameters.getParameters();
        ruleInstanceRepository.findById(param).ifPresent(it -> this.rule = it);
        if (rule == null) {
            rule = new RuleInstance();
            rule.setName("Новое правило");
            List<String> param2 = parameterMap.get(GROUP);
            RuleGroup group;
            if (param2 != null && !param2.isEmpty()) {
                long groupId = Long.parseLong(param2.get(0));
                Optional<RuleGroup> byId = ruleGroupRepository.findById(groupId);
                byId.ifPresent(ruleGroup -> rule.setGroup(ruleGroup));
            }
        }
        breadcrumbs.setBreadcrumb(this.getClass(), param, rule.getName());
        init();
    }
}
