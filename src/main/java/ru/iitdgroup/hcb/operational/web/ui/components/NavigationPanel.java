package ru.iitdgroup.hcb.operational.web.ui.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tabs;
import ru.iitdgroup.hcb.operational.web.ui.util.CssUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsModule("./src/fs-iconset.js")
public class NavigationPanel extends VerticalLayout {
    private Tabs tabs;
    private VerticalLayout menu;
    private VerticalLayout smallMenu;
    private Map<HorizontalLayout, List<HorizontalLayout>> menuItems = new HashMap<>();

    public NavigationPanel() {
        setPadding(false);
        init();
    }

    private void init() {
        setId("drawer-layout");
        setHeightFull();
        menu = new VerticalLayout();
        menu.setHeightFull();
        menu.getStyle().set("overflow", "auto");
        Icon globe = new Icon(VaadinIcon.GLOBE_WIRE);
        globe.setClassName("fs-globe-icon");
        Label label = new Label("Прод");
        label.setClassName("fs-menu-item-label");
        CssUtil.setCursorPointer(label);
        HorizontalLayout title = new HorizontalLayout(globe, label);
        CssUtil.setCursorPointer(title);
        menu.add(title);
        add(menu);
        smallMenu = new VerticalLayout();
        smallMenu.setWidth("100px");
        smallMenu.setHeightFull();
        smallMenu.setVisible(false);

        add(smallMenu);
        Icon icon = new Icon(VaadinIcon.SIGN_OUT_ALT);
        icon.getStyle().set("transform", "rotate(180deg)");
        Button collapseButton = new Button("Свернуть панель", icon);
        collapseButton.getStyle().set("background-color", "white");
        collapseButton.getStyle().set("cursor", "pointer");
        collapseButton.getStyle().set("background-image", "unset");
        collapseButton.getStyle().set("box-shadow", "unset");
        collapseButton.setWidthFull();
        collapseButton.addClickListener(e -> {
//            setWidth("100px");
//            menu.setVisible(false);
//            smallMenu.setVisible(true);
        });
        HorizontalLayout buttonLayout = new HorizontalLayout(collapseButton);
        buttonLayout.setWidthFull();
        add(buttonLayout);
    }

    public HorizontalLayout addMenuItem(String caption, VaadinIcon vaadinIcon, Class<? extends Component> target) {
        Label label = new Label(caption);
        label.setClassName("fs-menu-item-label");
        CssUtil.setCursorPointer(label);
        Icon icon = new Icon(vaadinIcon);
        icon.addClickListener(e -> icon.getUI().ifPresent(ui -> ui.navigate(target)));
        smallMenu.add(icon);
        HorizontalLayout menuItem = new HorizontalLayout(icon, label);
        menuItem.setAlignItems(Alignment.CENTER);
        menuItem.setWidthFull();
        menuItem.setClassName("fs-menu-item fs-left-menu-item");
        if (target != null) {
            menuItem.addClickListener(e -> menuItem.getUI().ifPresent(ui -> ui.navigate(target)));
        }
        menu.add(menuItem);
        return menuItem;
    }

    public HorizontalLayout addMenuSubItem(HorizontalLayout parent, String caption, Class<? extends Component> target) {
        return addMenuSubItem(parent, null, caption, target);
    }

    public HorizontalLayout addMenuSubItem(HorizontalLayout parent,
                                           HorizontalLayout lastChild,
                                           String caption, Class<? extends Component> target,
                                           boolean thirdSubItem) {
        List<HorizontalLayout> subItems = menuItems.get(parent);
        //todo раскритие 3го уровня меню
//        if (subItems != null) {
//            List<HorizontalLayout> finalItems = new ArrayList<>();
//            subItems.forEach(it -> {
//                List<HorizontalLayout> list = menuItems.get(it);
//                if (list != null) {
//                    finalItems.addAll(list);
//                }
//            });
//            subItems.addAll(finalItems);
//        }
        if (subItems == null) {
            subItems = new ArrayList<>();
            Icon chevronRight = new Icon(VaadinIcon.CHEVRON_RIGHT_SMALL);
            chevronRight.setSize("18px");
            parent.add(chevronRight);
            Icon chevronDown = new Icon(VaadinIcon.CHEVRON_DOWN_SMALL);
            chevronDown.setSize("18px");
            parent.add(chevronDown);
            chevronDown.setVisible(false);
            List<HorizontalLayout> finalSubItems = subItems;
            parent.addClickListener(click -> {
                if (chevronRight.isVisible()) {
                    showSubItems(chevronRight, chevronDown, finalSubItems);
                } else {
                    hideSubItems(chevronRight, chevronDown, finalSubItems);
                }
            });
//            chevronRight.addClickListener(click -> {
//                showSubItems(chevronRight, chevronDown, finalSubItems);
//            });
//            chevronDown.addClickListener(click -> {
//                hideSubItems(chevronRight, chevronDown, finalSubItems);
//            });
        }
        Label label = new Label(caption);
        label.setWidthFull();
        label.setClassName("fs-menu-item fs-menu-item-label");
        CssUtil.setCursorPointer(label);
        HorizontalLayout menuItem = new HorizontalLayout(label);
        menuItem.setAlignItems(Alignment.CENTER);
        menuItem.setClassName("fs-menu-item fs-left-menu-subitem");
        if (thirdSubItem) {
            menuItem.setClassName("fs-menu-item fs-left-menu-subitem fs-third");
        }
        if (target != null) {
            menuItem.addClickListener(e -> menuItem.getUI().ifPresent(ui -> ui.navigate(target)));
        }
        int index;
        if (lastChild == null) {
            index = menu.getElement().indexOfChild(parent.getElement());
        } else {
            index = menu.getElement().indexOfChild(lastChild.getElement());
        }
        menu.addComponentAtIndex(index + 1, menuItem);
        menuItem.setVisible(false);
        subItems.add(menuItem);
        menuItems.put(parent, subItems);
        return menuItem;
    }

    public HorizontalLayout addMenuSubItem(HorizontalLayout parent,
                                           HorizontalLayout lastChild,
                                           String caption, Class<? extends Component> target) {
        return addMenuSubItem(parent, lastChild, caption, target, false);
    }

    private void hideSubItems(Icon chevronRight, Icon chevronDown, List<HorizontalLayout> finalSubItems) {
        chevronRight.setVisible(true);
        chevronDown.setVisible(false);
        finalSubItems.forEach(it -> it.setVisible(false));
    }

    private void showSubItems(Icon chevronRight, Icon chevronDown, List<HorizontalLayout> finalSubItems) {
        chevronRight.setVisible(false);
        chevronDown.setVisible(true);
        finalSubItems.forEach(it -> it.setVisible(true));
    }


    public void addUserItem() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setClassName("fs-menu-item fs-left-menu-item");
        layout.setWidthFull();
        layout.setJustifyContentMode(JustifyContentMode.START);
        layout.getStyle().set("border-top", "1px solid #EBEFF2");
        Label label = new Label("User");
        CssUtil.setCursorPointer(label);
        label.setClassName("fs-menu-item fs-menu-item-label");
        layout.add(new Icon(VaadinIcon.USER), label);
        menu.add(layout);
    }

    public void expandItem(HorizontalLayout layout) {
        //todo expand
    }
}
