package ru.iitdgroup.hcb.operational.web.ui.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;

public class SingleColumnFormLayout extends FormLayout {
    public SingleColumnFormLayout() {
        setResponsiveSteps(
                new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
    }

    @Override
    public FormItem addFormItem(Component field, String label) {
        FormItem formItem = super.addFormItem(field, label);
        formItem.setClassName("fs-form-item-with-border");
        return formItem;
    }

    @Override
    public FormItem addFormItem(Component field, Component label) {
        FormItem formItem = super.addFormItem(field, label);
        formItem.setClassName("fs-form-item-with-border");
        return formItem;
    }
}
