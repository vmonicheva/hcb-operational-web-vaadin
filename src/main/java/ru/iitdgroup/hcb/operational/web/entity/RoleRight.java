package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;
import ru.iitdgroup.hcb.operational.web.enums.WorkspaceRight;

import javax.persistence.*;

@Entity
@Table(
        name = "FS_ROLE_RIGHT"
)
public final class RoleRight extends AbstractEntity {
    @NotNull
    @ManyToOne
    public Role role;
    @NotNull
    @OneToOne
    public Workspace workspace;
    @NotNull
    @Enumerated(EnumType.STRING)
    public WorkspaceRight right;

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

    @NotNull
    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(@NotNull Workspace workspace) {
        this.workspace = workspace;
    }

    @NotNull
    public WorkspaceRight getRight() {
        return right;
    }

    public void setRight(@NotNull WorkspaceRight right) {
        this.right = right;
    }
}
