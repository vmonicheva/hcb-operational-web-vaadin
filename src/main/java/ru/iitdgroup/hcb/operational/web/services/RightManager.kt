package ru.iitdgroup.hcb.operational.web.services

import org.apache.ignite.Ignite
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.iitdgroup.hcb.operational.web.entity.UserAccount
import ru.iitdgroup.hcb.operational.web.entity.Workspace
import ru.iitdgroup.hcb.operational.web.repository.RoleRepository

/**
 * Сервис для проверки прав пользователя
 * Использует кэш ignite для доступа к горячему списку доступных прав
 */
@Service
open class RightManager(
        private val userService: UserService,
        private val roleRepository: RoleRepository,
        private val ignite: Ignite,
        @Value("\${app.ldap.ad.adminRole:}") private val adAdminRole: String
) {

    companion object {
        private val LOGGER = LogManager.getLogger(RightManager::class.java)
        /**
         * Название кэша, где лежат актуальные права по суррогатному ключу клиента и workspace
         */
        const val CACHE_NAME = "USER-WORKSPACE-RIGHT"
    }

    /**
     * Проверяет права текущего пользователя. Если право доступно, то возвращается true, иначе false
     *
     * @param workspaceRight право на действие
     * @return доступно или нет право
     */
//    open fun checkRight(workspaceRight: WorkspaceRight): Boolean {
//        val currentUser = userService.getCurrentUser() ?: return false
//        val isAdminRole = currentUser.adGroups.contains(adAdminRole)
//        if (isAdminRole) {
//            LOGGER.debug("У пользователя админская роль. Доступ к {} разрешен", workspaceRight)
//            return true
//        }
//        val cache = ignite.getOrCreateCache<String, MutableSet<String>>(CACHE_NAME)
//        val cacheKey = getCacheKey(currentUser)
//        var rights = cache.get(cacheKey)
//        if (rights == null) {
//            LOGGER.debug("Не нашел прав в кэше, кэширую")
//            if (currentUser.selectedWorkspace == null) {
//                throwUserWithoutSelectedWorkspace(currentUser)
//            }
//            rights = currentUser.roles
//                    .flatMap { userRole -> userRole.role.rights.filter { it.workspace.id == currentUser.selectedWorkspace!!.id } }
//                    .map { it.right.name }
//                    .toMutableSet()
//            rights.addAll(roleRepository.findAll()
//                    .filter { currentUser.adGroups.contains(it.adGroup) }
//                    .flatMap { it.rights }
//                    .filter { it.workspace.id == currentUser.selectedWorkspace!!.id }
//                    .map { it.right.name })
//            cache.put(cacheKey, rights)
//        }
//        return rights.any { it == workspaceRight.name }
//    }
//
//    open fun resetCache(userAccount: UserAccount) {
//        val cache = ignite.getOrCreateCache<String, MutableSet<WorkspaceRight>>(CACHE_NAME)
//        cache.remove(getCacheKey(userAccount))
//    }

    /**
     * Генерация ключа для получения записи из кэша по правам
     * Учитывает текущий выбранный [Workspace]
     *
     * @param userAccount в отношении которого нужно найти права
     * @return ключ кэша
     */
    private fun getCacheKey(userAccount: UserAccount): String {
        if (userAccount.selectedWorkspace == null) {
            throwUserWithoutSelectedWorkspace(userAccount)
        }
        return String.format("%d-%d", userAccount.selectedWorkspace!!.id, userAccount.id)
    }

    /**
     * Выбросить исключение когда у пользователя не выбран [Workspace] + лог
     *
     * @param userAccount у которого нет выбранного [Workspace]
     */
    private fun throwUserWithoutSelectedWorkspace(userAccount: UserAccount) {
        val exception = UserWithoutSelectedWorkspaceException()
        LOGGER.error("У пользователя {} не выбран Workspace", userAccount.id, exception)
        throw exception
    }

}