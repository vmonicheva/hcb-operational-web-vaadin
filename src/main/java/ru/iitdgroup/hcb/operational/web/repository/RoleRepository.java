package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.hcb.operational.web.entity.Role;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Long> {

    @Query("select r from Role r join fetch r.rights")
    List<Role> findAllFetched();
}
