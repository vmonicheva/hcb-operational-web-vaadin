package ru.iitdgroup.hcb.operational.web.utils

import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort

class ChunkRequest(skip: Int, offset: Int) : Pageable {
    private var limit = 0
    private var offset = 0
    override fun getPageNumber(): Int {
        return 0
    }

    override fun getPageSize(): Int {
        return limit
    }

    override fun getOffset(): Long {
        return offset.toLong()
    }

    override fun getSort(): Sort? {
        return null
    }

    override fun next(): Pageable? {
        return null
    }

    override fun previousOrFirst(): Pageable {
        return this
    }

    override fun first(): Pageable {
        return this
    }

    override fun hasPrevious(): Boolean {
        return false
    }

    init {
        require(skip >= 0) { "Skip must not be less than zero!" }
        require(offset >= 0) { "Offset must not be less than zero!" }
        limit = offset
        this.offset = skip
    }
}