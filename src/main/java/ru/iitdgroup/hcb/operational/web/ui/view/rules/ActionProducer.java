package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import ru.iitdgroup.nwng.model.rule.actionsdef.AbstractActionDef;

@FunctionalInterface
public interface ActionProducer {
    AbstractActionDef getAction();
}
