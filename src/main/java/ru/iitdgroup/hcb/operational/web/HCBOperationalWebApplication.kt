package ru.iitdgroup.hcb.operational.web

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EntityScan(*[
    "ru.iitdgroup.hcb",
    "ru.iitdgroup.model.hcb.cards",
    "ru.iitdgroup.model.common",
    "ru.iitdgroup.model.cards",
    "com.intellinx.bom.entity",
    "ru.iitdgroup.model.hcb.clients",
    "ru.iitdgroup.model.hcb.cards"])
@EnableScheduling
open class HCBOperationalWebApplication : SpringBootServletInitializer() {
    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(HCBOperationalWebApplication::class.java, *args)
        }
    }

}
