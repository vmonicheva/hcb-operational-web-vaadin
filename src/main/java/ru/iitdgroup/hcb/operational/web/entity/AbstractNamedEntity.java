package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractNamedEntity extends AbstractEntity implements INamedEntity {

    @NotNull
    public String code;
    @NotNull
    public UUID guid;
    @NotNull
    public String name;
    @Nullable
    private String description;
    private int position;

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(@NotNull String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }


    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Override
    public UUID getGuid() {
        return guid;
    }

    public void setGuid(@NotNull UUID guid) {
        this.guid = guid;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
