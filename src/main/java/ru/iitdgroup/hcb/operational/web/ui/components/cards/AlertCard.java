package ru.iitdgroup.hcb.operational.web.ui.components.cards;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.io.IOUtils;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.AlertDef;
import ru.iitdgroup.hcb.operational.web.presenters.worktable.WorkTablePresenter;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;
import ru.iitdgroup.hcb.operational.web.ui.components.dialogs.AlertOwnerDialog;
import ru.iitdgroup.hcb.operational.web.ui.components.dialogs.ResoluteDialog;
import ru.iitdgroup.model.hcb.cards.CardTransaction;
import ru.iitdgroup.model.hcb.cards.HCBCard;
import ru.iitdgroup.model.hcb.clients.Client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import static ru.iitdgroup.hcb.operational.web.constants.Constants.*;

public class AlertCard extends EntityInfoLayout {
    private final AlertRepository alertRepository;
    private final CardRepository cardRepository;
    private final CardTransactionRepository transactionRepository;
    private final UserService userService;
    private final WorkTablePresenter workTablePresenter;
    private ICAlert alert;
    private Button take;
    private Button resolute;
    private Button backToQueue;
    private Button hold;
    private Button toExecutor;
    private Button escalate;

    public AlertCard(AlertRepository alertRepository,
                     CardRepository cardRepository,
                     UserService userService, CardTransactionRepository transactionRepository,
                     WorkTablePresenter workTablePresenter) {
        this.alertRepository = alertRepository;
        this.cardRepository = cardRepository;
        this.userService = userService;
        this.transactionRepository = transactionRepository;

        this.workTablePresenter = workTablePresenter;
    }

    public void init(ICAlert alert) {
        this.alert = alert;
        setAlignItems(Alignment.END);
        HorizontalLayout actionButtonsLayout = new HorizontalLayout();
        actionButtonsLayout.setPadding(false);
        actionButtonsLayout.getStyle().set("padding-right", "30px");
        add(actionButtonsLayout);
        actionButtonsLayout.add();
        initButtons(actionButtonsLayout);
        setButtonsVisibility();
        super.init();
        createCards(card);
        initActionsButtons(innerButtonLayout);
    }

    private void setButtonsVisibility() {
        String status = alert.getStatus();
        switch (status) {
            case inProgress:
                backToQueue.setVisible(false);
                take.setVisible(false);
                resolute.setVisible(true);
                hold.setVisible(true);
                toExecutor.setVisible(false);
                escalate.setVisible(true);
                break;
            case escalated:
                backToQueue.setVisible(false);
                take.setVisible(true);
                resolute.setVisible(false);
                hold.setVisible(false);
                toExecutor.setVisible(true);
                escalate.setVisible(false);
                break;
            case holded:
                backToQueue.setVisible(false);
                take.setVisible(true);
                resolute.setVisible(false);
                hold.setVisible(false);
                toExecutor.setVisible(false);
                escalate.setVisible(false);
                break;
            case closed:
                backToQueue.setVisible(true);
                take.setVisible(false);
                resolute.setVisible(false);
                hold.setVisible(false);
                toExecutor.setVisible(false);
                escalate.setVisible(true);
                break;
            case inQueue:
                take.setEnabled(true);
                toExecutor.setEnabled(true);
                backToQueue.setVisible(false);
                resolute.setVisible(false);
                hold.setVisible(false);
                escalate.setVisible(false);
                break;
        }
    }

    private HorizontalLayout initButtons(HorizontalLayout buttonsLayout) {
        take = new Button("Взять в работу");
        take.addClickListener(e -> {
            setStatus(AlertDef.IN_PROGRESS);
            alertRepository.save(alert);
        });
        take.setThemeName("primary");
        buttonsLayout.add(take);
        resolute = new Button("Указать резолюцию");
        resolute.addClickListener(e -> {
            ResoluteDialog dialog = new ResoluteDialog(alertRepository, alert, this::updateCard);
            dialog.init();
            dialog.open();
        });
        resolute.setThemeName("primary");
        buttonsLayout.add(resolute);
        backToQueue = new Button("Вернуть в очередь");
        backToQueue.addClickListener(e -> setStatus(AlertDef.IN_QUEUE));
        buttonsLayout.add(backToQueue);
        hold = new Button("Отложить");
        hold.addClickListener(e -> setStatus(AlertDef.HOLD));
        buttonsLayout.add(hold);
        toExecutor = new Button("Назначить исполнителя");
        toExecutor.addClickListener(e -> {
            openOwnerDialog(userService.findAllByWorkspace(workTablePresenter.getSelectedWorkspace()));
        });
        buttonsLayout.add(toExecutor);
        escalate = new Button("Эскалировать");
        escalate.addClickListener(e -> {
            openOwnerDialog(userService.findAllByWorkspace(workTablePresenter.getSelectedWorkspace()));
        });
        buttonsLayout.add(escalate);
        return buttonsLayout;
    }

    private void setStatus(AlertDef status) {
        alert.setStatus(status.getStatus());
        alertRepository.save(alert);
    }

    private void openOwnerDialog(List<UserAccount> userAccounts) {
        AlertOwnerDialog dialog = new AlertOwnerDialog(
                "Назначить исполнителя",
                userAccounts, alertRepository,
                alert,
                this::updateCard);
        dialog.init();
        dialog.open();
    }

    private void updateCard() {

    }

    @Override
    protected void createCards(EntityCard card) {
        createClientCard(card);
        createAlertCard(card);
        createTransactionCard(card);
    }

    private void createTransactionCard(EntityCard transactionCard) {
        CardTransaction transaction = getFetchedTransaction();
        if (transaction == null) {
            return;
        }
        VerticalLayout main = new VerticalLayout();
        FormLayout layout = new FormLayout();
        layout.setResponsiveSteps(new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));


        transactionCard.addInfoCard("Карточная транзакция, " + transaction.getTransactionAmount() + " руб. ", main);
        layout.addFormItem(new Label(transaction.getTransactionId() != null ? transaction.getTransactionId().toString() : ""), "Номер транзакции").setClassName("opr-form-item-with-border");
//        layout.addFormItem(new Label(transaction.get()), "Канал").setClassName("opr-form-item-with-border");
//        layout.addFormItem(new Label(transaction.getStatus()), "Статус транзакции").setClassName("opr-form-item-with-border");
        HCBCard card = transaction.getCard();
        layout.addFormItem(new Label(card != null ? card.getPan() : ""), "Номер карты").setClassName("opr-form-item-with-border");
        layout.addFormItem(new Label(transaction.getTransactionAmount().toString()), "Сумма операции").setClassName("opr-form-item-with-border");
        layout.addFormItem(new Label("Рубли"), "Валюта операции");
        main.add(layout);
        Label label = new Label("Последние транзакции");
        label.setClassName("opr-label");
        main.add(label);
        Grid<CardTransaction> grid = new Grid<>();
        grid.setHeightByRows(true);
        List<CardTransaction> transactionList = transactionRepository.findAllByCard(transaction.getCard());
        grid.setItems(transactionList);
        grid.addColumn(CardTransaction::getId, "Транзакция").setHeader("Транзакция");

        grid.addColumn((new ComponentRenderer<>((trs) -> {
            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.setAlignItems(Alignment.CENTER);
            trs = getFetchedTransaction();
            if (trs == null) {
                return horizontalLayout;
            }
            HCBCard crd = trs.getCard();
            Image cardImg = getCardImg(crd);
            if (cardImg != null) {
                cardImg.setMaxHeight("15px");
            }
            horizontalLayout.add(new Label(crd.getPan()), cardImg);
            return horizontalLayout;
        })), "Номер карты").setHeader("Номер карты");

        grid.addColumn(CardTransaction::getTransactionAmount, "Сумма").setHeader("Сумма");
        grid.addColumn((ValueProvider<CardTransaction, String>) trans -> "Рубли", "Валюта").setHeader("Валюта");
//        grid.addColumn(CardTransaction::getStatus, "Статус").setHeader("Статус");
        main.add(grid);
    }


    private CardTransaction getFetchedTransaction() {
        CardTransaction transaction = alert.getCardTransaction();
        if (transaction == null) {
            return null;
        }
        Optional<CardTransaction> optional = transactionRepository.findFetchedById(transaction.getId());
        if (optional.isEmpty()) {
            return null;
        }
        transaction = optional.get();
        return transaction;
    }

    private void createAlertCard(EntityCard alertCard) {

        VerticalLayout main = new VerticalLayout();
        FormLayout layout = new FormLayout();
        layout.setResponsiveSteps(new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
        layout.addFormItem(new Label(alert.getId().toString()), "Уникальный идентификатор").setClassName("opr-form-item-with-border");
        layout.addFormItem(new Label(alert.getStatus()), "Статус алерта").setClassName("opr-form-item-with-border");
        String ruleName = alert.getRuleName() != null ? alert.getRuleName() : "";
        layout.addFormItem(new Label(ruleName), "Группа правил").setClassName("opr-form-item-with-border");
        Long score = alert.getScore();
        layout.addFormItem(new Label(score != null ? score.toString() : ""), "Скоринг");
        main.add(layout);
        Label label = new Label("История алертов");
        label.setClassName("opr-label");
        main.add(label);
        Grid<ICAlert> alertHistory = new Grid<>();
        CardTransaction transaction = getFetchedTransaction();
        if (transaction == null) {
            return;
        }
        HCBCard card = transaction.getCard();
        if (card == null) {
            return;
        }
        card = cardRepository.findFetchedById(card.getId());
        List<CardTransaction> transactionList = transactionRepository.findAllByCard(card);
        List<ICAlert> alertList = alertRepository.findAllByCardTransactionIn(transactionList);
        alertHistory.setItems(alertList);
        alertHistory.addColumn(ICAlert::getId, "Алерт").setHeader("Алерт");
//        alertHistory.addColumn((ValueProvider<ICAlert, String>) alert -> alert.getCardTransaction().getStatus(), "Статус транзакции").setHeader("Статус транзакции");
        alertHistory.addColumn((ValueProvider<ICAlert, String>) alert -> alert.getCardTransaction().getId().toString(), "Транзакция").setHeader("Транзакция");
        alertHistory.addColumn(ICAlert::getRuleName, "Правило").setHeader("Правило");
        alertHistory.addColumn(ICAlert::getScore, "Скоринг").setHeader("Скоринг");
        alertHistory.addColumn(ICAlert::getStatus, "Статус алерта").setHeader("Статус алерта");
        main.add(alertHistory);
        alertCard.addInfoCard("Алерт: " + alert.getId() + ", " + ruleName + ", " + (score != null ? score : "") + ", " + alert.getStatus(), main);
    }

    //


    private Image getCardImg(HCBCard card) {
        String number = card.getPan();
        InputStream resource;
        if (number.startsWith("4")) {
            resource = getClass().getResourceAsStream("/META-INF/resources/img/visa.png");
        } else if (number.startsWith("9") || number.startsWith("7")) {
            resource = getClass().getResourceAsStream("/META-INF/resources/img/mastercard.png");

        } else {
            resource = getClass().getResourceAsStream("/META-INF/resources/img/mir.png");
        }

        try {
            byte[] bytes = IOUtils.toByteArray(resource);
            StreamResource streamResource = new StreamResource("image.jpg", () -> new ByteArrayInputStream(bytes));
            return new Image(streamResource, "image");
        } catch (IOException e) {
            return null;
        }
    }


    private void createClientCard(EntityCard clientCard) {
        CardTransaction transaction = getFetchedTransaction();
        if (transaction == null) {
            return;
        }

        FormLayout layout = new FormLayout();
        layout.setResponsiveSteps(new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
        HCBCard card = transaction.getCard();
        if (card == null) {
            return;
        }
        HCBCard fetchedById = cardRepository.findFetchedById(card.getId());
        if (fetchedById != null) {
            card = fetchedById;
            Client client = card.getCardHolder();
            if (client == null) {
                return;
            }
            String name = client.getFirstName();
            clientCard.addInfoCard("Клиент: " + name, layout);
            layout.addFormItem(new Label(name), "ФИО").setClassName("opr-form-item-with-border");
            layout.addFormItem(new Label(client.getCuid().toString()), "CUID").setClassName("opr-form-item-with-border");
//        String phone = client.getPhone();
            Label phoneLabel = new Label("");
            Button phoneButton = new Button(new Icon(VaadinIcon.PHONE));
            phoneButton.setClassName("opr-phone-button");
            HorizontalLayout phoneLayout = new HorizontalLayout(phoneLabel, phoneButton);
            phoneLayout.setAlignItems(FlexComponent.Alignment.CENTER);
            layout.addFormItem(phoneLayout, "Телефон мобильный");
        }
    }

    @Override
    protected void initActionsButtons(VerticalLayout innerButtonLayout) {
        Button toWhite = new Button("Добавить в белый список");
        toWhite.setClassName("opr-action-button");
        innerButtonLayout.add(toWhite);
        Button toBlack = new Button("Добавить в черный список");
        toBlack.setClassName("opr-action-button");
        innerButtonLayout.add(toBlack);
        Button banCredit = new Button("Запретить расходные операции");
        banCredit.setClassName("opr-action-button");
        innerButtonLayout.add(banCredit);
        Button allowCredit = new Button("Разрешить расходные операции");
        allowCredit.setClassName("opr-action-button");
        innerButtonLayout.add(allowCredit);
        Button banCash = new Button("Запретить снятие наличных");
        banCash.setClassName("opr-action-button");
        innerButtonLayout.add(banCash);
        Button allowCash = new Button("Разрешить снятие наличных");
        allowCash.setClassName("opr-action-button");
        innerButtonLayout.add(allowCash);
        Button blockCard = new Button("Заблокировать карту");
        blockCard.setClassName("opr-action-button");
        innerButtonLayout.add(blockCard);
        Button blockAllCards = new Button("Заблокировать все карты");
        blockAllCards.setClassName("opr-action-button");
        innerButtonLayout.add(blockAllCards);
        Button sendReport = new Button("Отправить отчет");
        sendReport.setClassName("opr-action-button");
    }
}
