package ru.iitdgroup.hcb.operational.web.presenters.alerts
/**
 * Класс описывающий общий для всех вкладок фильтр по строке поиска.
 */
open class BaseFilter {
    var text: String

    constructor() {
        text = ""
    }

    constructor(text: String) {
        this.text = text
    }

}