package ru.iitdgroup.hcb.operational.web.ui.view.reclamations;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.util.NoResourceLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.worktable.WorktableView;

@Route(value = "reclamations", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class ReclamationDicView extends CommonView {
    private final Breadcrumbs breadcrumbs;

    public ReclamationDicView(Breadcrumbs breadcrumbs) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
        NoResourceLayout layout = new NoResourceLayout();
        center.add(layout);
        center.setJustifyContentMode(JustifyContentMode.CENTER);
        center.setAlignItems(Alignment.CENTER);
        breadcrumbs.setBreadcrumb(WorktableView.class, "Рабочий стол", this.getClass(), "Рекламации");
    }
}
