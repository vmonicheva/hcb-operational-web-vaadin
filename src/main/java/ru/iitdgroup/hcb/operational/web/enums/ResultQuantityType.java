package ru.iitdgroup.hcb.operational.web.enums;

public enum ResultQuantityType {
    STAT_GET("Значение"),
    STAT_GET_LAST("Последние N значений");
    private String name;

    ResultQuantityType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
