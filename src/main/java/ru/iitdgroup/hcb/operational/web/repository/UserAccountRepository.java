package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;

import java.util.List;

public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {

    UserAccount findByCode(String code);

    @Override
    List<UserAccount> findAll();

    List<UserAccount> findAllBySelectedWorkspace(Workspace workspace);
}
