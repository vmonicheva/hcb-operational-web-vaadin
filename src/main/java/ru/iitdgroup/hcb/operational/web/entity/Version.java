package ru.iitdgroup.hcb.operational.web.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(
        name = "FS_VERSION"
)
public final class Version extends AbstractEntity {
}
