package ru.iitdgroup.hcb.operational.web.ui.components.rule;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors.*;
import ru.iitdgroup.nwng.model.metacatalog.def.MessageDef;
import ru.iitdgroup.nwng.model.metacatalog.def.MetaAttributeDef;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;

import java.util.Arrays;
import java.util.List;

public class RuleCreatorPanel extends VerticalLayout {
    private final List<MessageDef> messages;
    private final AccessorDef accessorDef;
    private final AccessorDef comparableAccessorDef;
    private final List<MetaAttributeDef> metaAttributes;

    public RuleCreatorPanel(String name,
                            List<MessageDef> messages,
                            AccessorDef accessorDef,
                            AccessorDef comparableAccessorDef,
                            List<MetaAttributeDef> metaAttributes) {
        this.messages = messages;
        this.accessorDef = accessorDef;
        this.comparableAccessorDef = comparableAccessorDef;
        this.metaAttributes = metaAttributes;
        setSizeFull();
        Label label = new Label(name);
        add(label);
        VerticalLayout paramsLayout = new VerticalLayout();
        paramsLayout.setPadding(false);
        ComboBox<ConditionAccessor<? extends AccessorDef>> accessorBox = new ComboBox<>("Тип параметра");
        accessorBox.setWidthFull();
        List<ConditionAccessor<? extends AccessorDef>> conditionAccessors = Arrays.asList(
                new ConditionConstantAccessor(accessorDef, comparableAccessorDef),
                new ConditionMessageAttrAccessor(accessorDef, comparableAccessorDef, messages, metaAttributes),
                new ConditionSystemProfileAccessor(accessorDef, comparableAccessorDef, metaAttributes),
                new ConditionWSProfileAccessor(accessorDef, comparableAccessorDef, metaAttributes),
                new ConditionAttributeProfileAccessor(accessorDef, comparableAccessorDef, metaAttributes)
        );
        accessorBox.setItems(conditionAccessors);
        accessorBox.setItemLabelGenerator(ConditionAccessor::getCaption);
        accessorBox.addValueChangeListener(e -> {
            paramsLayout.removeAll();
            ConditionAccessor<? extends AccessorDef> accessor = accessorBox.getValue();
            List<Component> components = accessor.getComponents();
            components.forEach(paramsLayout::add);
        });
        add(accessorBox);
        add(paramsLayout);
    }

    private void getDefs() {
        Arrays.asList(
                new ConditionConstantAccessor(accessorDef, comparableAccessorDef),
                new ConditionMessageAttrAccessor(accessorDef, comparableAccessorDef, messages, metaAttributes),
                new ConditionSystemProfileAccessor(accessorDef, comparableAccessorDef, metaAttributes),
                new ConditionWSProfileAccessor(accessorDef, comparableAccessorDef, metaAttributes),
                new ConditionAttributeProfileAccessor(accessorDef, comparableAccessorDef, metaAttributes)
        );
    }
}
