package ru.iitdgroup.hcb.operational.web.ui.components.rule.accessors;

import com.vaadin.flow.component.Component;
import ru.iitdgroup.nwng.model.rule.accessdef.AccessorDef;

import java.util.List;

public abstract class ConditionAccessor<T extends AccessorDef> {
    private final AccessorDef prevAccessorDef;
    private final AccessorDef comparableAccessorDef;

    public ConditionAccessor(AccessorDef prevAccessorDef,
                             AccessorDef comparableAccessorDef
    ) {
        this.prevAccessorDef = prevAccessorDef;
        this.comparableAccessorDef = comparableAccessorDef;
    }

    public abstract String getCaption();

    public abstract List<Component> getComponents();

}
