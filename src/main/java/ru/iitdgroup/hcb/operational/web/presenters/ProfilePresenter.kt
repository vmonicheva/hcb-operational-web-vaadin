package ru.iitdgroup.hcb.operational.web.presenters

import org.springframework.stereotype.Service
import ru.iitdgroup.hcb.operational.web.presenters.alerts.BaseFilter
import ru.iitdgroup.hcb.operational.web.presenters.alerts.DataQuery
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository
import ru.iitdgroup.hcb.operational.web.services.UserService
import java.util.*
import java.util.stream.Stream

@Service
open class ProfilePresenter(
        workspaceRepository: WorkspaceRepository,
        userService: UserService
) : AbstractWorkspacePresenter<Any, BaseFilter>(workspaceRepository, userService) {
    override fun getList(dataQuery: DataQuery<BaseFilter>): Stream<Any?> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCount(filter: Optional<BaseFilter>): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getTotal(): Int {
        TODO("Not yet implemented")
    }

}
