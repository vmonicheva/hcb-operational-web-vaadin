package ru.iitdgroup.hcb.operational.web.ui.components.rule;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.ui.util.CssUtil;

public class RuleInfoLayout extends VerticalLayout {
    public RuleInfoLayout(RuleInstance rule) {
        setSizeFull();
        getStyle().set("padding-top", "0");
        HorizontalLayout topRow = new HorizontalLayout();
        topRow.setWidthFull();
        VerticalLayout eyeLayout = new VerticalLayout();
        eyeLayout.setAlignItems(Alignment.CENTER);
        Label label = new Label("Статус");
        label.setClassName("fs-rule-card-label");
        eyeLayout.add(label);
        eyeLayout.setWidth("unset");
        Icon eye = new Icon(VaadinIcon.EYE);
        eye.getStyle().set("color", "var(--lumo-secondary-text-color)");
        eye.getStyle().set("margin-top", "7px");
        Icon eyeSlashed = new Icon(VaadinIcon.EYE_SLASH);
        eyeSlashed.getStyle().set("color", "var(--lumo-secondary-text-color)");
        eyeSlashed.getStyle().set("margin-top", "7px");
        boolean isVisible = true;
        if (rule != null) {
            isVisible = rule.getStatus();
        }
        eye.setVisible(isVisible);
        eyeSlashed.setVisible(!isVisible);
        eyeLayout.add(eye, eyeSlashed);
        CssUtil.setCursorPointer(eyeLayout);
        eyeLayout.addClickListener(e -> {
            boolean visible = eye.isVisible();
            eye.setVisible(!visible);
            eyeSlashed.setVisible(visible);
        });
        topRow.add(eyeLayout);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setWidthFull();
        TextField group = new TextField("Группа");
        group.setId("fs-disabled-group-textfield");
        group.setEnabled(false);
        group.setValue(rule.getName());
        horizontalLayout.add(group);
        TextField code = new TextField("Код");
        horizontalLayout.add(code);
        TextField name = new TextField("Наименование");
        name.setWidthFull();
        horizontalLayout.add(name);
        topRow.add(horizontalLayout);
        add(topRow);
        TextArea description = new TextArea("Описание");
        description.setWidthFull();
        description.setHeight("70%");
        add(description);
        HorizontalLayout horizontalLayout1 = new HorizontalLayout();
        horizontalLayout1.setWidthFull();
        TextField mode = new TextField("Режим");
        mode.setWidthFull();
        horizontalLayout1.add(mode);
        horizontalLayout1.add(new TextField("Приоритет"));
        horizontalLayout1.add(new TextField("Скоринг"));
        add(horizontalLayout1);

        HorizontalLayout horizontalLayout2 = new HorizontalLayout();
        horizontalLayout2.setWidthFull();
        TextField lastAuthor = new TextField("Автор последнего изменения");
        lastAuthor.setWidthFull();
        horizontalLayout2.add(lastAuthor);
        TextField lastDate = new TextField("Дата последнего изменения");
        horizontalLayout2.add(lastDate);
        add(horizontalLayout2);
    }
}
