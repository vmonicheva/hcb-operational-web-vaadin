package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.model.hcb.cards.HCBCard;

public interface CardRepository extends CrudRepository<HCBCard, Long> {
    @Query("select c from HCBCard c left join fetch c.cardHolder where c.id=:id")
    HCBCard findFetchedById(Long id);
}
