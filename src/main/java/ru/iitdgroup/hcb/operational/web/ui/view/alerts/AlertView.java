package ru.iitdgroup.hcb.operational.web.ui.view.alerts;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.AlertDef;
import ru.iitdgroup.hcb.operational.web.presenters.worktable.WorkTablePresenter;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardRepository;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.CommonListComponent;
import ru.iitdgroup.hcb.operational.web.ui.components.grids.AlertGrid;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Alert list view
 * Списочная форма алертов (оповещений)
 */
@Route(value = "alert", layout = MainLayout.class)
@CssImport(value = "./styles/alert-grid.css", themeFor = "vaadin-grid")
@PageTitle("Интерфейс оперативного реагирования")
public class AlertView extends CommonView {
    private final AlertRepository alertRepository;
    private final CardTransactionRepository cardTransactionRepository;
    private final CardRepository cardRepository;
    private final Breadcrumbs breadcrumbs;
    private final WorkTablePresenter workTablePresenter;
    private AlertGrid alertGrid;

    public AlertView(AlertRepository alertRepository,
                     CardTransactionRepository cardTransactionRepository,
                     CardRepository cardRepository, Breadcrumbs breadcrumbs,
                     WorkTablePresenter workTablePresenter) {
        super(breadcrumbs);
        this.alertRepository = alertRepository;
        this.cardTransactionRepository = cardTransactionRepository;
        this.cardRepository = cardRepository;
        this.breadcrumbs = breadcrumbs;
        this.workTablePresenter = workTablePresenter;
        breadcrumbs.setBreadcrumb(this.getClass(), "Справочники", this.getClass(), "Оповещения");
        init();
    }

    public void init() {
        CommonListComponent commonListComponent = new CommonListComponent();
        List<ICAlert> alertList = alertRepository.findAll();
        alertGrid = new AlertGrid(true, alertList, cardTransactionRepository, cardRepository);
        HorizontalLayout paginationLayout = alertGrid.getBottomLayout();
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setWidthFull();
        Button take = new Button("Взять в работу");
        take.addClickListener(event -> {
            Set<ICAlert> selectedItems = alertGrid.getSelectedItems();
            if (selectedItems.isEmpty()) {
                return;
            }
            if (selectedItems.size() == 1) {
                take.getUI().ifPresent(ui -> {
                    openNewWindow(selectedItems.iterator().next(), ui);
                });
            } else {
                Dialog dialog = new Dialog();
                VerticalLayout layout = new VerticalLayout();
                layout.add(new Label("Взять в работу и открыть в новых вкладках алерты №№" +
                        selectedItems.stream()
                                .map(it -> String.valueOf(it.getId()))
                                .collect(Collectors.joining(",")) + "?"));
                Button open = new Button("Открыть");
                open.addClickListener(e -> {
                    take.getUI().ifPresent(ui -> {
                        selectedItems.forEach(it ->
                                openNewWindow(it, ui));
                    });
                    dialog.close();
                });
                Button cancel = new Button("Отмена");
                cancel.addClickListener(e -> dialog.close());
                HorizontalLayout horizontalLayout1 = new HorizontalLayout(open, cancel);
                horizontalLayout1.setWidthFull();

                layout.add(horizontalLayout1);
                dialog.add(layout);
                dialog.open();
            }
        });
        take.setThemeName("primary");
        buttons.add(take);
        Button resolution = new Button("Указать резолюцию");
        resolution.setThemeName("primary");
        buttons.add(resolution);
        buttons.add(new Button("Вернуть в очередь"));
        buttons.add(new Button("Назначить исполнителя"));
        buttons.add(new Button("Эскалировать"));
        paginationLayout.addComponentAtIndex(0, buttons);


        VerticalLayout main = new VerticalLayout();
        main.getStyle().set("background-color", "white");
        main.setSizeFull();
        Tabs tabs = new Tabs();
        tabs.setWidthFull();
        Tab all = new Tab("Все");
        Tab inQueue = new Tab("В очереди");
        Tab inProgress = new Tab("В работе");
        Tab escalated = new Tab("Эскалировано");
        Tab closed = new Tab("Закрыто");
        Tab my = new Tab("Мои оповещения");
        tabs.add(all, inQueue, inProgress, escalated, closed, my);
        tabs.addSelectedChangeListener(event -> {
            Tab selectedTab = event.getSelectedTab();
            if (selectedTab.equals(all)) {
                alertGrid.setItems(alertList);
                return;
            }
            if (selectedTab.equals(inQueue)) {
                alertGrid.setItems(alertList.stream()
                        .filter(alert -> alert.getStatus().equals(AlertDef.IN_QUEUE.getStatus())));
                return;
            }
            if (selectedTab.equals(inProgress)) {
                alertGrid.setItems(alertList.stream()
                        .filter(alert -> alert.getStatus().equals(AlertDef.IN_PROGRESS.getStatus())));
                return;
            }
            if (selectedTab.equals(escalated)) {
                alertGrid.setItems(alertList.stream()
                        .filter(alert -> alert.getStatus().equals(AlertDef.ESCALATED.getStatus())));
                return;
            }
            if (selectedTab.equals(closed)) {
                alertGrid.setItems(alertList.stream()
                        .filter(alert -> alert.getStatus().equals(AlertDef.CLOSED.getStatus())));
                return;
            }
            if (selectedTab.equals(my)) {
                alertGrid.setItems(workTablePresenter.getMyAlerts());
            }
        });
        main.add(tabs);
        main.add(commonListComponent);
        commonListComponent.setHeightFull();
        commonListComponent.setPadding(false);
        commonListComponent.getContentLayout().add(alertGrid);
        center.add(main);
    }

    private void openNewWindow(ICAlert selectedItem, UI ui) {
        RouteConfiguration configuration = RouteConfiguration
                .forRegistry(ui.getRouter().getRegistry());
        String url = configuration.getUrl(AlertInfoView.class, selectedItem.getId());
        VaadinServletRequest currentRequest = (VaadinServletRequest) VaadinService.getCurrentRequest();
        String serverName = currentRequest.getServerName();
        int serverPort = currentRequest.getServerPort();
        String port = serverPort == 80 ? "" : ":" + serverPort;
        UI.getCurrent().getPage().open("http://" + serverName + port + "/" + url);
    }
}
