package ru.iitdgroup.hcb.operational.web.ui.view.security;

import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.util.NoResourceLayout;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

@Route(value = "security", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class SecurityView extends CommonView {
    private final Breadcrumbs breadcrumbs;
    public SecurityView(Breadcrumbs breadcrumbs) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
        NoResourceLayout layout = new NoResourceLayout();
        center.add(layout);
        center.setJustifyContentMode(JustifyContentMode.CENTER);
        center.setAlignItems(Alignment.CENTER);
        breadcrumbs.setBreadcrumb(this.getClass(), "Безопасность");
    }
}
