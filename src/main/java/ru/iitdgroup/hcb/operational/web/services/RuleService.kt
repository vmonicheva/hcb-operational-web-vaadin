package ru.iitdgroup.hcb.operational.web.services

import org.apache.ignite.Ignite
import org.springframework.stereotype.Service
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository
import ru.iitdgroup.nwng.model.rule.hierarchydef.SystemRuleSpaceDef
import ru.iitdgroup.nwng.model.rule.hierarchydef.WorkspaceDef

/**
 * Сервис для загрузки правил в Ignite для ядра
 * Возможно это переделается
 */
@Service
open class RuleService(
        private val ignite: Ignite,
        private val workspaceRepository: WorkspaceRepository
) {

    open fun refresh() {
        val systemRuleSpaceDef = initSystemRuleSpace()
        val cache = ignite.getOrCreateCache<Long, SystemRuleSpaceDef>("SystemRuleSpaceDef")
        cache.put(1L, systemRuleSpaceDef)
        val message = ignite.message(ignite.cluster().forRemotes())
        message.send("rulesDef", systemRuleSpaceDef)
    }

    private fun initSystemRuleSpace(): SystemRuleSpaceDef {
        val systemRuleSpaceDef = SystemRuleSpaceDef()
        workspaceRepository.findAll().forEach { workspace ->
            val workspaceDef = WorkspaceDef(workspace.name, workspace.code)
            workspaceDef.guid = workspace.id
            workspaceDef.description = workspace.description
            workspaceDef.position = workspace.position

//            workspace.ruleGroups.forEach { ruleGroup ->
//                val ruleGroupDef = RuleGroupDef(ruleGroup.name, ruleGroup.code)
//                ruleGroupDef.groupType = ruleGroup.groupType
//                ruleGroupDef.workspaceDef = workspaceDef
//                ruleGroup.ruleInstance().forEach { ruleInstance ->
//                    if (!ruleInstance.isStatus) {
//                        return@forEach
//                    }
//                    val ruleInstanceDef = RuleInstanceDef(ruleInstance.name, ruleInstance.code)
//                    ruleInstanceDef.group = ruleGroupDef
//                    ruleInstanceDef.logic = ruleInstance.logicDef
//                    ruleInstanceDef.description = ruleInstance.description
//                    ruleInstanceDef.position = ruleInstance.position
//                    ruleGroupDef.ruleInstances[ruleInstance.id] = ruleInstanceDef
//                }
//                workspaceDef.ruleGroups[ruleGroup.id] = ruleGroupDef
//            }
            systemRuleSpaceDef.workspaces[workspaceDef.guid] = workspaceDef
        }
        return systemRuleSpaceDef
    }

}