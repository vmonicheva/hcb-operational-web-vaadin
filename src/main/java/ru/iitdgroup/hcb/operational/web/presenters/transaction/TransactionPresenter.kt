package ru.iitdgroup.hcb.operational.web.presenters.transaction

import org.apache.logging.log4j.LogManager
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import ru.iitdgroup.hcb.operational.web.presenters.AbstractWorkspacePresenter
import ru.iitdgroup.hcb.operational.web.presenters.alerts.DataQuery
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository
import ru.iitdgroup.hcb.operational.web.services.UserService
import ru.iitdgroup.hcb.operational.web.utils.OffsetBasedPageRequest
import ru.iitdgroup.model.hcb.cards.CardTransaction
import java.util.*
import java.util.stream.Stream

@Service
open class TransactionPresenter(
        workspaceRepository: WorkspaceRepository,
        userService: UserService,
        private val cardTransactionRepository: CardTransactionRepository
) : AbstractWorkspacePresenter<CardTransaction, TransactionFilter>(workspaceRepository, userService) {

    companion object {
        private const val SORT_COLUMN_ATTR = "transaction-grid-sort-column"
        private const val SETTING_COLUMN_ATTR = "transaction-grid-settings"
        private val LOGGER = LogManager.getLogger(TransactionPresenter::class.java)
    }

    open fun find(): List<CardTransaction> {
        return cardTransactionRepository.findAll().toList()
    }

    open fun find(id: Long): CardTransaction? {
        return cardTransactionRepository.findById(id).orElse(null)
    }

    open fun find(offset: Int, limit: Int, sort: Sort, filter: TransactionFilter): List<CardTransaction?> {
        if (limit == 0)
            return Collections.emptyList()
        val pageRequest = OffsetBasedPageRequest(offset, limit, sort)
        val spec: Specification<CardTransaction>? = resolveSpecificationFromInfixExpr(filter)
        return cardTransactionRepository.findAll(spec, pageRequest).content
    }

    protected open fun resolveSpecificationFromInfixExpr(transactionFilter: TransactionFilter): Specification<CardTransaction>? {

        return TransactionSpecification(transactionFilter)
    }

//    open fun getGridSettings(grid: WideGrid<CardTransaction, TransactionFilter>): List<WideGridColumnSetting<CardTransaction>> {
//        val r: List<WideGridColumnSetting<CardTransaction>>
//        val attribute = VaadinSession.getCurrent().getAttribute(SETTING_COLUMN_ATTR)
//        r = if (attribute != null) {
//            attribute as List<WideGridColumnSetting<CardTransaction>>
//        } else {
//            grid.columns
//                    .map { WideGridColumnSetting(true, it) }
//                    .toList()
//        }
//        return r
//    }
//
//    open fun updateGridSettings(settings: Collection<WideGridColumnSetting<CardTransaction>>) {
//        VaadinSession.getCurrent().setAttribute(SETTING_COLUMN_ATTR, settings)
//        LOGGER.info("updated transaction grid column settings")
//    }

    override fun getList(dataQuery: DataQuery<TransactionFilter>): Stream<CardTransaction?> {
        return find(dataQuery.offset, dataQuery.limit, dataQuery.dataSort, dataQuery.filter).stream()
    }

    override fun getCount(filter: Optional<TransactionFilter>): Int {
        return cardTransactionRepository.count().toInt()
    }

    override fun getTotal(): Int {
        return cardTransactionRepository.count().toInt()
    }
}