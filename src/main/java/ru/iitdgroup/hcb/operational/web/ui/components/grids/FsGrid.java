package ru.iitdgroup.hcb.operational.web.ui.components.grids;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.ValueProvider;
import ru.iitdgroup.hcb.operational.web.ui.util.CssUtil;

import java.util.*;

public abstract class FsGrid<T> extends FsPaginatedGrid<T> {
    protected final Map<String, String> columnMap;
    protected final List<Column<T>> columns;
    protected final Set<T> selected = new HashSet<>();
    private final Collection<T> items;
    protected Icon detailedInfo;
    private boolean showPagination;

    public FsGrid(boolean showPagination, Collection<T> items) {
        super(showPagination);
        this.showPagination = showPagination;
        columnMap = new HashMap<>();
        columns = new ArrayList<>();
        this.items = items;
        init();
    }


    protected void init() {
        setHeightByRows(false);
        setPaginatorTexts("Страница", "из");
        setPaginatorSize(2);
        setPageSize(15);
        setItems(items);
        setSelectionMode(SelectionMode.MULTI);
        GridSelectionModel<T> model = this.getSelectionModel();
        if (model instanceof GridMultiSelectionModel && showPagination) {
            asMultiSelect().addSelectionListener(e -> {
                Set<T> selectedItems = e.getAllSelectedItems();
                if (selectedItems.size() > 0) {
                    selectedItemsCount.setText("Выбрано " + selectedItems.size() + " записей");
                    selectedItemsCount.setVisible(true);
                } else {
                    selectedItemsCount.setVisible(false);
                }
            });
        }
        setColumnReorderingAllowed(true);
        addSelectionListener(e -> selected.addAll(e.getAllSelectedItems()));
        addPageChangeListener(event -> selected.forEach(this::select));

        addInfoColumn();
        addMainColumns();
        addSettingsColumn();
        getColumns().forEach(this::setResizable);
    }

    private void setResizable(Column<T> column) {
        column.setResizable(true);
        Element parent = column.getElement().getParent();
        while (parent != null
                && "vaadin-grid-column-group".equals(parent.getTag())) {
            parent.setProperty("resizable", "true");
            parent = parent.getParent();
        }
    }

    protected abstract void addMainColumns();

    protected void addSettingsColumn() {
        Icon headerComponent = new Icon(VaadinIcon.COG_O);
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setOpenOnClick(true);
        contextMenu.setTarget(headerComponent);
        columns.forEach(alertColumn -> {
            String key = alertColumn.getKey();
            Checkbox checkbox = new Checkbox(columnMap.get(key));
            checkbox.setValue(getColumnByKey(key).isVisible());
            checkbox.addClickListener(click -> {
                getColumnByKey(key).setVisible(checkbox.getValue());
            });
            contextMenu.addItem(checkbox);
        });
        headerComponent.setSize("20px");
        CssUtil.setCursorPointer(headerComponent);
        addColumn((ValueProvider<T, String>) alert -> "").setHeader(headerComponent).setFlexGrow(0).setWidth("60px");
    }

    protected void addInfoColumn() {
        addColumn(new ComponentRenderer<>((item) -> {
            detailedInfo = new Icon(VaadinIcon.SEARCH);
            detailedInfo.addClickListener(getDetailedInfoListener(item));
            CssUtil.setCursorPointer(detailedInfo);
            detailedInfo.setSize("15px");
            return detailedInfo;
        }))
                .setFlexGrow(0)
                .setWidth("50px");
    }

    protected abstract ComponentEventListener<ClickEvent<Icon>> getDetailedInfoListener(T item);
}
