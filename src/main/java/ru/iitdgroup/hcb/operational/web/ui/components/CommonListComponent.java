package ru.iitdgroup.hcb.operational.web.ui.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

/**
 * List form element
 * <p>
 * Элемент списочной формы (включает в себя компоненты поиска, основного контента и кнопок управления)
 */
public class CommonListComponent extends VerticalLayout {
    private final VerticalLayout contentLayout;
    private final TextField searchField;
    private final Button searchButton;

    public CommonListComponent() {
        setId("fs-common-list-layout");
        getStyle().set("background-color", "var(--opr-grey)");
        VerticalLayout innerLayout = new VerticalLayout();
        innerLayout.getStyle().set("background-color", "white");
        innerLayout.setSizeFull();
        HorizontalLayout searchLayout = new HorizontalLayout();
        searchLayout.setWidthFull();
        searchField = new TextField();
        searchField.setId("fs-search-textfield");
        searchField.setPlaceholder("Поиск");
        searchField.setWidthFull();
        searchButton = new Button("Найти");
        searchButton.setId("fs-search-button");
        searchButton.setThemeName("primary");
        searchLayout.add(searchField, searchButton);
        contentLayout = new VerticalLayout();
        contentLayout.setId("fs-common-list-content");
        contentLayout.setPadding(false);
        contentLayout.setSizeFull();
        innerLayout.add(searchLayout);
        innerLayout.add(contentLayout);
        add(innerLayout);
    }


    public VerticalLayout getContentLayout() {
        return contentLayout;
    }

    public TextField getSearchField() {
        return searchField;
    }

    public Button getSearchButton() {
        return searchButton;
    }
}
