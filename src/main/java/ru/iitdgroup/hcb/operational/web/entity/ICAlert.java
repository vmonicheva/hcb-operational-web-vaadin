package ru.iitdgroup.hcb.operational.web.entity;

import com.intellinx.bom.annotations.Description;
import com.intellinx.bom.annotations.DisplayName;
import com.intellinx.bom.annotations.EntityType;
import com.intellinx.bom.annotations.EntityTypeDescriptor;
import ru.iitdgroup.model.hcb.cards.CardTransaction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@EntityType(EntityTypeDescriptor.APPLICATIVE)
@Table(name = "IC_ALERTS")
public class ICAlert implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @Column(name = "OBJECT_TYPE")
    @NotNull
    public String objectType;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ALERT_DATE")
    public Date alertDate;

    @Column(name = "ALERT_TARGET_NAME")
    public String alertTargetName;

    @Column(name = "CHANGED")
    public Boolean changed;

    @Column(name = "DESCRIPTION")
    public String description;

    @Column(name = "LAST_MODIFIED")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    public Date lastModified;

    @Column(name = "ORIGIN")
    public String origin;

    @Column(name = "RESOLUTION")
    public String resolution;

    @Column(name = "RULE_NAME")
    public String ruleName;

    @Column(name = "SCORE")
    public Long score;

    @Column(name = "SEVERITY")
    @NotNull
    public String severity;

    @Column(name = "STATUS")
    @NotNull
    public String status;

    @Column(name = "STATUS_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    public Date statusModified;

    @Column(name = "TITLE")
    @NotNull
    public String title;

    @Column(name = "URGENT")
    public Boolean urgent;

    @Column(name = "WORKSPACE")
    @NotNull
    public String workspace;

    @Column(name = "ALERT_TARGET_ID")
    public Long alertTargetId;

    @Column(name = "LAST_MODIFIED_BY")
    public String lastModifiedBy;

    @Column(name = "OWNER")
    public String owner;

    @Column(name = "STATUS_MODIFIED_BY")
    public String statusModifiedBy;

    @DisplayName(name = "Карточная транзакция")
    @JoinColumn(name = "CARD_TRANSACTION_FK")
    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @Description("Карточная транзакция, по которой было сформировано данное оповещение")
    public CardTransaction cardTransaction;

    public ICAlert() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Date getAlertDate() {
        return alertDate;
    }

    public void setAlertDate(Date alertDate) {
        this.alertDate = alertDate;
    }

    public String getAlertTargetName() {
        return alertTargetName;
    }

    public void setAlertTargetName(String alertTargetName) {
        this.alertTargetName = alertTargetName;
    }

    public Boolean getChanged() {
        return changed;
    }

    public void setChanged(Boolean changed) {
        this.changed = changed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatusModified() {
        return statusModified;
    }

    public void setStatusModified(Date statusModified) {
        this.statusModified = statusModified;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public Long getAlertTargetId() {
        return alertTargetId;
    }

    public void setAlertTargetId(Long alertTargetId) {
        this.alertTargetId = alertTargetId;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getStatusModifiedBy() {
        return statusModifiedBy;
    }

    public void setStatusModifiedBy(String statusModifiedBy) {
        this.statusModifiedBy = statusModifiedBy;
    }

    public CardTransaction getCardTransaction() {
        return cardTransaction;
    }

    public void setCardTransaction(CardTransaction cardTransaction) {
        this.cardTransaction = cardTransaction;
    }
}
