package ru.iitdgroup.hcb.operational.web.ui.components.dialogs;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.FsDialog;

import java.util.List;

public class AlertOwnerDialog extends FsDialog {
    private final List<UserAccount> accountList;
    private final AlertRepository alertRepository;
    private final Runnable afterCloseUpdate;
    private final ICAlert alert;

    public AlertOwnerDialog(String name,
                            List<UserAccount> accountList,
                            AlertRepository alertRepository,
                            ICAlert alert,
                            Runnable afterCloseUpdate
    ) {
        super(name);
        this.accountList = accountList;
        this.alertRepository = alertRepository;
        this.afterCloseUpdate = afterCloseUpdate;
        this.alert = alert;
    }

    @Override
    public void init() {
        super.init();
        okCancelLayout.setVisible(false);
        Button apply = new Button("Применить");
        apply.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        apply.setEnabled(false);
        getButtonsLayout().add(apply);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setWidthFull();
        TextField search = new TextField();
        search.setPlaceholder("Поиск");
        search.setWidthFull();
        Button button = new Button("Найти");
        horizontalLayout.add(search, button);
        mainLayout.addComponentAtIndex(1, horizontalLayout);

        ListBox<UserAccount> listBox = new ListBox<>();
        listBox.addValueChangeListener(e -> apply.setEnabled(true));
        listBox.setItems(accountList);
        listBox.setRenderer(new ComponentRenderer<>((item) -> new Label(item.getName())));
        mainLayout.addComponentAtIndex(2, listBox);
        apply.addClickListener(e -> {
            UserAccount value = listBox.getValue();
            if (value == null) {
                return;
            }
            alert.setOwner(value.getCode());
            alertRepository.save(alert);
            close();
            afterCloseUpdate.run();
        });
    }
}
