package ru.iitdgroup.hcb.operational.web.presenters.user

import org.springframework.data.jpa.domain.Specification
import ru.iitdgroup.hcb.operational.web.QueryCount
import ru.iitdgroup.hcb.operational.web.entity.UserAccount
import ru.iitdgroup.model.hcb.cards.CardTransaction
import javax.persistence.criteria.*

/**
 * Составной предикат для поиска объектов по имени таба и полю поиска
 */
class UserAccountSpecification(val filter: UserAccountFilter) : Specification<UserAccount>, QueryCount {

    override fun toPredicate(root: Root<UserAccount>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {

        if (currentQueryIsCountRecords(query)) {

        } else {

        }
        return null
    }

    override fun currentQueryIsCountRecords(criteriaQuery: CriteriaQuery<*>): Boolean {
        return criteriaQuery.resultType.name == "java.lang.Long" ||
                criteriaQuery.resultType == Long::class.javaPrimitiveType
    }

}