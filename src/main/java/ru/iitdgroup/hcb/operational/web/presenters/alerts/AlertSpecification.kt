package ru.iitdgroup.hcb.operational.web.presenters.alerts

import org.springframework.data.jpa.domain.Specification
import ru.iitdgroup.hcb.operational.web.QueryCount
import ru.iitdgroup.hcb.operational.web.constants.Constants
import ru.iitdgroup.hcb.operational.web.entity.ICAlert
import javax.persistence.criteria.*

/**
 * Составной предикат для поиска объектов по имени таба и полю поиска
 */
class AlertSpecification(val filter: AlertFilter) : Specification<ICAlert>, QueryCount {

    override fun toPredicate(root: Root<ICAlert>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
        val predicateForStatus = generateForStatusPredicate(criteriaBuilder, root)
        val predicateTextSearch = generateForTextSearchPredicate(criteriaBuilder, root)

        if (!currentQueryIsCountRecords(query)) {
            root.fetch<Any, Any>("cardTransaction", JoinType.LEFT)
                    .fetch<Any, Any>("card", JoinType.LEFT)
                    .fetch<Any, Any>("cardHolder", JoinType.LEFT)
        }

        return generatePredicate(predicateForStatus, predicateTextSearch, criteriaBuilder)
    }

    override fun currentQueryIsCountRecords(criteriaQuery: CriteriaQuery<*>): Boolean {
        return criteriaQuery.resultType.name == "java.lang.Long" ||
                criteriaQuery.resultType == Long::class.javaPrimitiveType
    }

    private fun generateForStatusPredicate(criteriaBuilder: CriteriaBuilder, root: Root<ICAlert>): Predicate? {
        return when {
            filter.tab.isNotEmpty() && filter.tab == Constants.myAlerts -> criteriaBuilder.equal(root.get<Any>("owner"), filter.name)
            filter.tab.isNotEmpty() -> criteriaBuilder.equal(root.get<Any>("status"), filter.tab)
            else -> null
        }
    }

    private fun generateForTextSearchPredicate(criteriaBuilder: CriteriaBuilder, root: Root<ICAlert>): Predicate? {
        val cardTransaction: Join<Any, Any> = root.join("cardTransaction", JoinType.LEFT)
        when {
            filter.text.isNotBlank() -> {
                return try {
                    filter.text.toLong()
                    val predicateId = criteriaBuilder.equal(root.get<Any>("id"), filter.text)
                    val predicateTransactionId = criteriaBuilder.equal(cardTransaction.get<Any>("id"), filter.text)
                    criteriaBuilder.or(predicateId, predicateTransactionId)
                } catch (numberFormatException: NumberFormatException) {
                    val predicateRule = criteriaBuilder.equal(root.get<Any>("ruleName"), filter.text)
                    when {
                        filter.tab.isEmpty() || filter.text == filter.tab -> {
                            val predicateTextStatus = criteriaBuilder.equal(root.get<Any>("status"), filter.text)
                            criteriaBuilder.or(predicateRule, predicateTextStatus)
                        }
                        else -> criteriaBuilder.or(predicateRule)
                    }
                }
            }
            else -> return null
        }
    }

    private fun generatePredicate(statusSearchPredicate: Predicate?, textSearchPredicate: Predicate?, criteriaBuilder: CriteriaBuilder): Predicate? {
        return when {
            statusSearchPredicate != null && textSearchPredicate != null -> {
                criteriaBuilder.and(statusSearchPredicate, textSearchPredicate)
            }
            statusSearchPredicate != null -> statusSearchPredicate
            textSearchPredicate != null -> textSearchPredicate
            else -> null
        }
    }
}