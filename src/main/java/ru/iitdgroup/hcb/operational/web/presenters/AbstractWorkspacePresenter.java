package ru.iitdgroup.hcb.operational.web.presenters;

import org.springframework.beans.factory.annotation.Value;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;
import ru.iitdgroup.hcb.operational.web.presenters.alerts.BaseFilter;
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractWorkspacePresenter<T, Y extends BaseFilter> implements IWorkspacePresenter<T, Y> {
    private final WorkspaceRepository workspaceRepository;
    private final UserService userService;
    @Value("${app.dateFormat}")
    private String dateFormat;

    protected AbstractWorkspacePresenter(WorkspaceRepository workspaceRepository,
                                         UserService userService) {
        this.workspaceRepository = workspaceRepository;
        this.userService = userService;
    }

    @Override
    public List<Workspace> getWorkspaces() {
        return workspaceRepository.findAll();
    }

    @Override
    public Workspace getSelectedWorkspace() {
        UserAccount currentUser = userService.getCurrentUser();
        List<Workspace> workspaces = new ArrayList<>(getWorkspaces());
        if (currentUser == null) {
            return workspaces.isEmpty() ? null : workspaces.get(0);
        }
        Workspace selectedWorkspace = currentUser.getSelectedWorkspace();
        if (selectedWorkspace == null) {
            return workspaces.isEmpty() ? null : workspaces.get(0);
        }
        return selectedWorkspace;
    }

    @Override
    public void selectWorkspace(Workspace workspace) {
        userService.selectWorkspace(workspace);
    }

    @Override
    public UserAccount getCurrentUser() {
        return userService.getCurrentUser();
    }

    public DateTimeFormatter dateFormatter() {
        return DateTimeFormatter.ofPattern(dateFormat);
    }
}
