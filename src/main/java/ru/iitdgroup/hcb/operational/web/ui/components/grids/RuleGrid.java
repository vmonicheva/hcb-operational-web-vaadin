package ru.iitdgroup.hcb.operational.web.ui.components.grids;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;
import ru.iitdgroup.hcb.operational.web.entity.RuleGroup;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.ui.util.CssUtil;
import ru.iitdgroup.hcb.operational.web.ui.view.rules.RuleInfoView;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static ru.iitdgroup.hcb.operational.web.ui.view.rules.RuleInfoView.GROUP;

public class RuleGrid extends FsGrid<RuleInstance> {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy-HH:mm:ss z");

    public RuleGrid(boolean showPagination, Collection<RuleInstance> items) {
        super(showPagination, items);
    }

    @Override
    protected void addMainColumns() {
        addColumn(RuleInstance::getName, "Название").setHeader("Название");
        addColumn(RuleInstance::getCode, "Код").setHeader("Код");
        addColumn(RuleInstance::getScoring, "Скоринг").setHeader("Скоринг");
        addColumn((ValueProvider<RuleInstance, String>) ruleInstance -> {
            ZonedDateTime lastUpdatedOn = ruleInstance.getLastUpdatedOn();
            return lastUpdatedOn.format(formatter);
        }, "Дата изменения").setHeader("Дата изменения");
        addColumn(new ComponentRenderer<>((item) -> {
            Icon eye = new Icon(VaadinIcon.EYE);
            Icon eyeSlashed = new Icon(VaadinIcon.EYE_SLASH);
            Boolean status = item.getStatus();
            eye.setVisible(status);
            eyeSlashed.setVisible(!status);
            HorizontalLayout horizontalLayout = new HorizontalLayout(eye, eyeSlashed);
            CssUtil.setCursorPointer(horizontalLayout);
            horizontalLayout.addClickListener(e -> {
                item.setStatus(!status);
                if (!e.isFromClient()) {
                    return;
                }
                getDataProvider().refreshAll();
            });
            return horizontalLayout;
        })).setHeader("Статус");
    }

    @Override
    protected ComponentEventListener<ClickEvent<Icon>> getDetailedInfoListener(RuleInstance item) {
        Map<String, List<String>> params = new HashMap<>();
        RuleGroup group = item.getGroup();
        if (group != null) {
            params.put(GROUP, Collections.singletonList(String.valueOf(group.getId())));
        }
        QueryParameters queryParameters = new QueryParameters(params);
        String url = RouteConfiguration.forSessionScope().getUrl(RuleInfoView.class, item.getId());
        return e -> getUI().ifPresent(ui -> ui.navigate(url));
    }
}
