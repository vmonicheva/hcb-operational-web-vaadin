package ru.iitdgroup.hcb.operational.web.ui.view.customers;

import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.cards.ClientCard;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;

@Route(value = "client", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class ClientInfoView extends CommonView implements HasUrlParameter<Long> {
    private final Breadcrumbs breadcrumbs;

    public ClientInfoView(Breadcrumbs breadcrumbs) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Long parameter) {
        breadcrumbs.setBreadcrumb(this.getClass(), parameter, "Клиент №" + parameter);
        ClientCard clientCard = new ClientCard();
        center.add(clientCard);
        clientCard.init();
    }
}
