package ru.iitdgroup.hcb.operational.web.ui.components.rule;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.ui.view.rules.CreateRuleView;
import ru.iitdgroup.nwng.model.rule.accessdef.CheckDef;
import ru.iitdgroup.nwng.model.rule.hierarchydef.RuleLogicDef;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class RuleConditionLayout extends VerticalLayout {
    public RuleConditionLayout(RuleInstance rule) {
        setSizeFull();
        setPadding(false);

        TextField formula = new TextField("Формула");
        formula.setWidthFull();
        add(formula);
        Label label = new Label("Условия");
        label.setClassName("fs-rule-card-label");
        add(label);
        Grid<CheckDef> conditionGrid = new Grid<>();
        conditionGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        conditionGrid.addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        conditionGrid.setHeightFull();
        RuleLogicDef logicDef = rule.getLogicDef();
        if (logicDef != null) {
            Map<Long, CheckDef> checks = logicDef.getChecks();
            if (checks != null) {
                Collection<CheckDef> values = checks.values();
                conditionGrid.setItems(values);
            }
        }
        conditionGrid.addColumn(CheckDef::getCode).setHeader("Код условия");
        conditionGrid.addColumn(CheckDef::getDescription).setHeader("Описание условия");
        add(conditionGrid);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button add = new Button("Добавить условие");
        add.addClickListener(e -> add.getUI().ifPresent(ui -> ui.navigate(CreateRuleView.class, 0L)));
        horizontalLayout.add(add);
        Button edit = new Button("Редактировать");
        edit.addClickListener(e -> add.getUI().ifPresent(ui -> ui.navigate(CreateRuleView.class, rule.getId())));
        edit.setEnabled(false);
        horizontalLayout.add(edit);
        Button delete = new Button("Удалить");
        delete.setEnabled(false);
        conditionGrid.addSelectionListener(e -> {
            Optional<CheckDef> firstSelectedItem = e.getFirstSelectedItem();
            if (firstSelectedItem.isEmpty()) {
                return;
            }
            edit.setEnabled(true);
            delete.setEnabled(true);
        });
        horizontalLayout.add(delete);
        add(horizontalLayout);

    }
}