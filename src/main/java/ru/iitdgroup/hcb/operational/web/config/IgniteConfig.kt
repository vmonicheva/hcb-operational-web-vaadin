package ru.iitdgroup.hcb.operational.web.config

import org.apache.ignite.Ignite
import org.apache.ignite.Ignition
import org.apache.ignite.configuration.IgniteConfiguration
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder
import org.apache.ignite.springdata.repository.config.EnableIgniteRepositories
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Бин для конфигурации подключения к Ignite
 */
@Configuration
@EnableIgniteRepositories
@ConfigurationProperties(prefix = "app.ignite")
open class IgniteConfig {

    open var metricsLogFrequency: Long = 60000
    open var addresses: Array<String> = emptyArray()

    @Bean
    open fun igniteInstance(): Ignite {
        val config = IgniteConfiguration()
        config.igniteInstanceName = "hcb-operational-web"
        config.isClientMode = true
        config.metricsLogFrequency = metricsLogFrequency
        config.isPeerClassLoadingEnabled = true
        val discoverySpi = TcpDiscoverySpi()
        val vmIpFinder = TcpDiscoveryVmIpFinder()
        vmIpFinder.setAddresses(addresses.toList())
        discoverySpi.ipFinder = vmIpFinder
        config.discoverySpi = discoverySpi
        return Ignition.start(config)
    }
}