package ru.iitdgroup.hcb.operational.web.ui.util;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class NoResourceLayout extends VerticalLayout {
    public NoResourceLayout() {
        try {
            InputStream resource = getClass().getResourceAsStream("/META-INF/resources/img/no-resource-view.png");
            byte[] bytes = IOUtils.toByteArray(resource);
            StreamResource streamResource = new StreamResource("image.jpg", () -> new ByteArrayInputStream(bytes));
            Image image = new Image(streamResource, "image");
            image.setMaxWidth("300px");
            Label label = new Label("Экран находится в разработке");
            label.setClassName("fs-no-resource-label");
            add(image, label);
            setWidth("unset");
            setAlignItems(Alignment.CENTER);
        } catch (IOException e) {
            //todo logger
        }
    }
}
