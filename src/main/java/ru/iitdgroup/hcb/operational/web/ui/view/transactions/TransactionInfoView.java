package ru.iitdgroup.hcb.operational.web.ui.view.transactions;

import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.repository.CardTransactionRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.cards.TransactionCard;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.model.hcb.cards.CardTransaction;

@Route(value = "transaction", layout = MainLayout.class)
@PageTitle("Интерфейс оперативного реагирования")
public class TransactionInfoView extends CommonView implements HasUrlParameter<Long> {
    private final Breadcrumbs breadcrumbs;
    private final CardTransactionRepository transactionRepository;
    private final TransactionCard transactionCard;
    private CardTransaction transaction;

    public TransactionInfoView(Breadcrumbs breadcrumbs, CardTransactionRepository transactionRepository, TransactionCard transactionCard) {
        super(breadcrumbs);
        this.breadcrumbs = breadcrumbs;
        this.transactionRepository = transactionRepository;
        this.transactionCard = transactionCard;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Long parameter) {
        transactionRepository.findFetchedById(parameter).ifPresent(it -> transaction = it);
        breadcrumbs.setBreadcrumb(this.getClass(), parameter, "Транзакция №" + parameter);
        if (transaction != null) {
            init();
        }
    }

    private void init() {
        center.removeAll();
        transactionCard.init(transaction);
        center.add(transactionCard);
        center.getStyle().set("overflow", "hidden");
    }
}
