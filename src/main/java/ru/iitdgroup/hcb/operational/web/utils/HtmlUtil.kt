package ru.iitdgroup.hcb.operational.web.utils

object HtmlUtil {
    fun getBadge(name: String, count: Long): String {
        return name + "&nbsp;<span style=\"position: absolute; width: 18px; " +
                "height: 18px; background: #E61C41; border-radius: 50%; " +
                "text-align: center;     margin-top: 9.5px;\n" +
                "line-height: normal;color: white;font-size:initial;\">"+count+"</span>&nbsp&nbsp&nbsp&nbsp&nbsp"
    }
}