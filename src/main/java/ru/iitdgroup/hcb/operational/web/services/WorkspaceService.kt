package ru.iitdgroup.hcb.operational.web.services

import org.springframework.stereotype.Service
import ru.iitdgroup.hcb.operational.web.entity.Workspace
import ru.iitdgroup.hcb.operational.web.repository.WorkspaceRepository

@Service
open class WorkspaceService(private val workspaceRepository: WorkspaceRepository) {

    open fun getAll(): List<Workspace> {
        return workspaceRepository.findAll().toList()
    }

}