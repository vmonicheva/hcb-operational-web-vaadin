package ru.iitdgroup.hcb.operational.web.ui.util;

import com.vaadin.flow.component.HasStyle;

public class CssUtil {
    public static void setCursorPointer(HasStyle component) {
        component.getStyle().set("cursor", "pointer");
    }
}
