package ru.iitdgroup.hcb.operational.web.ui.view.profiles;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.SingleColumnFormLayout;
import ru.iitdgroup.hcb.operational.web.ui.components.cards.EntityCard;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;

public abstract class Profile extends CommonView {

    public Profile(Breadcrumbs breadcrumbs) {
        super(breadcrumbs);
        init();
    }

    protected void init() {
        VerticalLayout main = new VerticalLayout();
        main.getStyle().set("overflow", "auto");
        main.setSizeFull();
        VerticalLayout innerLayout = new VerticalLayout();
        innerLayout.getStyle().set("background-color", "white");
        main.add(innerLayout);

        SingleColumnFormLayout commonInfo = new SingleColumnFormLayout();
        commonInfo.getStyle().set("border", "1px solid #E0E0E0");
        commonInfo.getStyle().set("padding-right", "5px");
        commonInfo.getStyle().set("padding-left", "5px");
        commonInfo.addFormItem(new Label(getProfileType()), "Тип профиля");
        commonInfo.addFormItem(new Label(getProfileId()), "Уникальный идентификатор");
        commonInfo.addFormItem(new Label(getDescription()), "Описание");
        commonInfo.addFormItem(new Label(getCreateDate()), "Дата создания");
        commonInfo.addFormItem(new Label(getAuthor()), "Кем создан");
        innerLayout.add(commonInfo);

        EntityCard card = new EntityCard();
        card.setPadding(false);
        Grid<Object> historyGrid = new Grid<>();
        card.addInfoCard("История сообщений", historyGrid);
        VerticalLayout metricLayout = getAttributeCard(historyGrid, getOnOkMetricListener(), getOnEditMetricListener(), getOnDeleteMetricListener());
        card.addInfoCard("Метрики", metricLayout);
        Grid<Object> listGrid = new Grid<>();
        VerticalLayout listLayout = getAttributeCard(listGrid, getOnOkListListener(), getOnEditListListener(), getOnDeleteListListener());
        card.addInfoCard("Списки", listLayout);
        Grid<Object> virtualAttributeGrid = new Grid<>();
        VerticalLayout virtualAttributeLayout = getAttributeCard(virtualAttributeGrid, getOnOkVirtualAttrListener(), getOnEditVirtualAttrListener(), getOnDeleteVirtualAttrListener());
        card.addInfoCard("Виртуальные атрибуты", virtualAttributeLayout);

        innerLayout.add(card);
        center.add(main);
    }

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnDeleteVirtualAttrListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnEditVirtualAttrListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnOkVirtualAttrListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnDeleteListListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnEditListListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnOkListListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnDeleteMetricListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnEditMetricListener();

    protected abstract ComponentEventListener<ClickEvent<Button>> getOnOkMetricListener();

    protected abstract String getAuthor();

    protected abstract String getCreateDate();

    protected abstract String getDescription();

    protected abstract String getProfileId();

    protected abstract String getProfileType();

    protected VerticalLayout getAttributeCard(Grid<Object> grid,
                                              ComponentEventListener<ClickEvent<Button>> onOkListener,
                                              ComponentEventListener<ClickEvent<Button>> onEditListener,
                                              ComponentEventListener<ClickEvent<Button>> onDeleteListener
    ) {
        VerticalLayout metricLayout = new VerticalLayout();
        metricLayout.add(grid);
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button add = new Button("Создать");
        add.addClickListener(onOkListener);
        buttonsLayout.add(add);
        Button edit = new Button("Редактировать");
        edit.addClickListener(onEditListener);
        buttonsLayout.add(edit);
        Button delete = new Button("Удалить");
        delete.addClickListener(onDeleteListener);
        buttonsLayout.add(delete);
        metricLayout.add(buttonsLayout);
        return metricLayout;
    }
}
