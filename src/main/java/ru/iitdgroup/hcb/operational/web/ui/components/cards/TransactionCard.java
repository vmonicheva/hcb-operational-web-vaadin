package ru.iitdgroup.hcb.operational.web.ui.components.cards;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.iitdgroup.hcb.operational.web.repository.AlertRepository;
import ru.iitdgroup.hcb.operational.web.ui.components.SingleColumnFormLayout;
import ru.iitdgroup.model.hcb.cards.*;

import java.math.BigDecimal;
import java.util.Objects;

@Component
public class TransactionCard extends EntityInfoLayout {
    public static final String DEFAULT_OBJ = "?";
    private final AlertRepository alertRepository;
    private CardTransaction cardTransaction;

    public TransactionCard(AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    public void init(CardTransaction cardTransaction) {
        super.init();
        this.cardTransaction = cardTransaction;
        createCards(card);
        initActionsButtons(innerButtonLayout);
    }

    @Override
    protected void createCards(EntityCard card) {
        createMainAttributes(card);
        createAdditionalAttributes(card);
        createAcquireAttributes(card);
    }

    private void createAcquireAttributes(EntityCard card) {
        VerticalLayout main = new VerticalLayout();
        SingleColumnFormLayout formLayout = new SingleColumnFormLayout();
        TerminalRetailer terminalRetailer = cardTransaction.getTerminalRetailer();
        formLayout.addFormItem(new Label(getParameter(terminalRetailer != null ? terminalRetailer.getName() : DEFAULT_OBJ)), "Имя торговца");

        Country terminalCountry = cardTransaction.getTerminalCountry();
        formLayout.addFormItem(new Label(getParameter(terminalCountry != null ? terminalCountry.getName() : DEFAULT_OBJ)), "Страна торговца");

        TerminalCity terminalCity = cardTransaction.getTerminalCity();
        formLayout.addFormItem(new Label(getParameter(terminalCity != null ? terminalCity.getName() : DEFAULT_OBJ)),
                "Город торговца");

        TerminalAddress terminalAddress = cardTransaction.getTerminalAddress();
        formLayout.addFormItem(new Label(getParameter(terminalAddress != null ? terminalAddress.getName() : DEFAULT_OBJ)), "Местоположение торговца");

        Terminal terminal = cardTransaction.getTerminal();
        formLayout.addFormItem(new Label(getParameter(terminal != null ? terminal.getTerminalID() : DEFAULT_OBJ)), "Имя терминала");

        TerminalOwner terminalOwner = cardTransaction.getTerminalOwner();
        formLayout.addFormItem(new Label(getParameter(terminalOwner != null ? terminalOwner.getName() : DEFAULT_OBJ)), "Владелец терминала");

        FinancialInstitution acquirer = cardTransaction.getAcquirer();
        formLayout.addFormItem(new Label(getParameter(acquirer != null ? acquirer.getName() : DEFAULT_OBJ)), "BIN эквайера");

//        formLayout.addFormItem(new Label(), "Страна BIN'а эквайера");
        Country acquiringInstitutionCountryCode = cardTransaction.getAcquiringInstitutionCountryCode();
        formLayout.addFormItem(new Label(getParameter(acquiringInstitutionCountryCode != null ?
                acquiringInstitutionCountryCode.getName() : DEFAULT_OBJ)), "Страна эквайера");

//        formLayout.addFormItem(new Label(), "Название банка эквайера");
        MerchantCategory merchantCategory = cardTransaction.getMerchantCategory();
        formLayout.addFormItem(new Label(getParameter(merchantCategory != null ? merchantCategory.getName() : DEFAULT_OBJ)), "MCC");
//        formLayout.addFormItem(new Label(), "Страна IP");
//        formLayout.addFormItem(new Label(), "IP-адрес клиента");

        main.add(formLayout);
        card.addInfoCard("Атрибуты эквайера", main);
    }

    private void createAdditionalAttributes(EntityCard card) {
        VerticalLayout main = new VerticalLayout();
        SingleColumnFormLayout formLayout = new SingleColumnFormLayout();
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getRrnSentToFinalInterchange())), "RRN");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getPosConditionCode())), "POS condition code");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getPosEntryMode())), "POS entry mode");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getPinVerificationResult())), "Проверка PIN-а");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getTerminalCanEnterPin())), "Доступность PIN");
//        formLayout.addFormItem(new Label(getParameter(cardTransaction.getTerminalCanEnterPin())), "PIN введен");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getCvvVerificationResult())), "Проверка карты");
        HCBCard hcbCard = cardTransaction.getCard();
        String cardNumber = hcbCard != null ? getParameter(hcbCard.getPan()) : DEFAULT_OBJ;
        formLayout.addFormItem(new Label(cardNumber), "Анализируемая карта");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getSourceCurrencyAmount())), "Оригинальная сумма");
        Currency currency = cardTransaction.getSourceCurrency();
        formLayout.addFormItem(new Label(getParameter(currency != null ? currency.getName() : DEFAULT_OBJ)), "Оригинальная валюта");
        Currency billingCurrencyCode = cardTransaction.getCardholderBillingCurrencyCode();
        formLayout.addFormItem(new Label(getParameter(billingCurrencyCode != null ? billingCurrencyCode.getName() : DEFAULT_OBJ)), "Валюта авторизации (billing)");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getDestinationAccountCurrencyAmount())), "Сумма в валюте счета");
        Currency dstAccountCurrency = cardTransaction.getDstAccountCurrency();
        formLayout.addFormItem(new Label(getParameter(dstAccountCurrency != null ? dstAccountCurrency.getName() : DEFAULT_OBJ)), "Валюта счета");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getAvailableBalance())), "Доступный баланс");
//        formLayout.addFormItem(new Label(), "Код подтверждения");
//        formLayout.addFormItem(new Label(), "Возможность ввода данных карты в терминал");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getCvvVerificationResult())), "Детали CVV2");
//        formLayout.addFormItem(new Label(), "Electronic commerce indicator");
//        formLayout.addFormItem(new Label(), "Данные 3-D Secure");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getTokenData())), "Данные по токенам");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getAuthorizerResponceText())), "Тип авторизации");
//        formLayout.addFormItem(new Label(), "Список счетов источников");

        main.add(formLayout);
        card.addInfoCard("Дополнительные атрибуты", main);
    }

    @NotNull
    private String getParameter(String parameter) {
        return Objects.requireNonNullElse(parameter, DEFAULT_OBJ);
    }

    @NotNull
    private String getParameter(Long parameter) {
        return String.valueOf(Objects.requireNonNullElse(parameter, DEFAULT_OBJ));
    }

    @NotNull
    private String getParameter(BigDecimal parameter) {
        return String.valueOf(Objects.requireNonNullElse(parameter, DEFAULT_OBJ));
    }

    @NotNull
    private String getParameter(Integer parameter) {
        return String.valueOf(Objects.requireNonNullElse(parameter, DEFAULT_OBJ));
    }

    private void createMainAttributes(EntityCard card) {
        VerticalLayout main = new VerticalLayout();
        SingleColumnFormLayout formLayout = new SingleColumnFormLayout();
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getTransactionRRN())), "Номер");
        formLayout.addFormItem(new Label(), "Время");
        PaymentSystem paymentSystem = cardTransaction.getTerminalPaymentSystem();
        String paymentSystemName = paymentSystem != null ? getParameter(paymentSystem.getName()) : DEFAULT_OBJ;
        formLayout.addFormItem(new Label(paymentSystemName), "Платежная система");
        formLayout.addFormItem(new Label(), "Рамки");
        Terminal terminal = cardTransaction.getTerminal();

        String terminalClass = terminal != null ? getParameter(terminal.getTerminalClass()) : DEFAULT_OBJ;
        formLayout.addFormItem(new Label(terminalClass), "Класс терминала");

        formLayout.addFormItem(new Label(getParameter(cardTransaction.getTransactionCode())), "Код транзакции");
        formLayout.addFormItem(new Label(), "Анализируемая сумма");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getDeclineReason())), "Причина отклонения транзакции");
        formLayout.addFormItem(new Label(getParameter(cardTransaction.getRetainCardRaw())), "Код ответа авторизации");

//        ICAlert alert = alertRepository.findByCardTransaction(cardTransaction);
//        String alertId = alert != null ? String.valueOf(getParameter(alert.getId())) : DEFAULT_OBJ;
//        formLayout.addFormItem(new Label(alertId), "Алерт");
//        String score = alert != null ? String.valueOf(getParameter(alert.getScore())) : DEFAULT_OBJ;
//        formLayout.addFormItem(new Label(score), "Скорринг");
        main.add(formLayout);
        card.addInfoCard("Основные атрибуты", main);
    }

    @Override
    protected void initActionsButtons(VerticalLayout innerButtonLayout) {
        Button toWhite = new Button("Добавить в белый список");
        toWhite.setClassName("opr-action-button");
        innerButtonLayout.add(toWhite);
        Button toBlack = new Button("Добавить в черный список");
        toBlack.setClassName("opr-action-button");
        innerButtonLayout.add(toBlack);
        Button banCredit = new Button("Запретить расходные операции");
        banCredit.setClassName("opr-action-button");
        innerButtonLayout.add(banCredit);
        Button allowCredit = new Button("Разрешить расходные операции");
        allowCredit.setClassName("opr-action-button");
        innerButtonLayout.add(allowCredit);
        Button banCash = new Button("Запретить снятие наличных");
        banCash.setClassName("opr-action-button");
        innerButtonLayout.add(banCash);
        Button allowCash = new Button("Разрешить снятие наличных");
        allowCash.setClassName("opr-action-button");
        innerButtonLayout.add(allowCash);
        Button blockCard = new Button("Заблокировать карту");
        blockCard.setClassName("opr-action-button");
        innerButtonLayout.add(blockCard);
        Button blockAllCards = new Button("Заблокировать все карты");
        blockAllCards.setClassName("opr-action-button");
        innerButtonLayout.add(blockAllCards);
        Button sendReport = new Button("Отправить уведомление в ФинЦЕРТ");
        sendReport.setClassName("opr-action-button");
        innerButtonLayout.add(sendReport);
    }
}
