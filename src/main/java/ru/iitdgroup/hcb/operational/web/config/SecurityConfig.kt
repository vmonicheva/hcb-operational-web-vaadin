package ru.iitdgroup.hcb.operational.web.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider
import org.springframework.security.web.util.matcher.RequestMatcher

/**
 * Конфигурация Spring security
 * Требуются конфиги в application.yml:
 * <ol>
 *     <li>app.ldap.ad.domain</li>
 *     <li>app.ldap.ad.url</li>
 *     <li>app.ldap.ad.rootDn</li>
 * </ol>
 */
@Configuration
@EnableWebSecurity
open class SecurityConfig(
        @field:Value("\${app.ldap.ad.domain}") private val LDAPDomain: String? = null,
        @field:Value("\${app.ldap.ad.url}") private val LDAPUrl: String? = null,
        @field:Value("\${app.ldap.ad.rootDn}") private val LDAPRootDn: String? = null
) : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers(
                "/resources/**",
                "/VAADIN/**", // Vaadin Flow static resources
                // the standard favicon URI
                "/favicon.ico",

                // the robots exclusion standard
                "/robots.txt",

                // web application manifest // Needed only when developing a Progressive Web Application
                "/manifest.webmanifest",
                "/sw.js",
                "/offline-page.html",

                // icons and images
                "/icons/**",
                "/images/**",

                // (development mode) static resources //
                "/frontend/**",

                // (development mode) webjars //
                "/webjars/**",

                // (development mode) H2 debugging console
                "/h2-console/**",

                // (production mode) static resources //
                "/frontend-es5/**",
                "/frontend-es6/**"
        )
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        // Not using Spring CSRF here to be able to use plain HTML for the login page
        http
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                    .csrf().disable()
                // Register our CustomRequestCache, that saves unauthorized access attempts, so
                // the user is redirected after login.
//                .requestCache().requestCache(requestCache())
                // Restrict access to our application.
                .authorizeRequests()
                // Allow all flow internal requests.
                .requestMatchers(RequestMatcher { SecurityUtils.isFrameworkInternalRequest(it) }).permitAll()
                // Allow all requests by logged in users.
                .anyRequest().fullyAuthenticated()
        // Configure the login page.
        //                .and().formLogin().loginPage("/" + LoginView.ROUTE).permitAll()
        // Configure logout
        //                .and().logout().logoutSuccessUrl(LOGOUT_SUCCESS_URL);
    }

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(ActiveDirectoryLdapAuthenticationProvider(LDAPDomain, LDAPUrl, LDAPRootDn))
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }
}
