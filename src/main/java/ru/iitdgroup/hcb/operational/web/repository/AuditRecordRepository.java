package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.nwng.model.rights.def.AuditRecord;

public interface AuditRecordRepository extends CrudRepository<AuditRecord, Long> {
}
