package ru.iitdgroup.hcb.operational.web.repository;

import org.springframework.data.repository.CrudRepository;
import ru.iitdgroup.model.hcb.clients.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {
}
