package ru.iitdgroup.hcb.operational.web.ui.login;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.iitdgroup.hcb.operational.web.ui.view.worktable.WorktableView;

@Route("login")
@RouteAlias("")
@PageTitle("Логин")
@Theme(value = Lumo.class)
public class LoginView extends FlexLayout {
    private final AuthenticationManager authenticationManager;
    private final Logger LOGGER = LogManager.getLogger(LoginView.class);

    public LoginView(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        init();
    }

    private void init() {
        LoginI18n i18n = LoginI18n.createDefault();
        i18n.setHeader(new LoginI18n.Header());
        i18n.getForm().setUsername("Имя пользователя");
        i18n.getForm().setSubmit("Войти");
        i18n.getForm().setPassword("Пароль");
        i18n.getForm().setForgotPassword("Не помню пароль");
        i18n.getErrorMessage()
                .setMessage("Неверное имя пользователя или пароль<br>Проверьте корректность вводимых данных и повторите вход");
        LoginForm loginForm = new LoginForm();
        loginForm.setI18n(i18n);
        loginForm.addLoginListener(loginEvent -> {
            login(loginEvent.getUsername(), loginEvent.getPassword());
        });
        add(loginForm);
    }

    private void login(String username, String password) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            if (authentication != null) {
                SecurityContextHolder.getContext().setAuthentication(authentication);
                logAuth(authentication);
                UI.getCurrent().navigate(WorktableView.class);
            }

        } catch (Exception e) {
            Notification.show(e.getMessage());
            LOGGER.error(e);
        }

    }

    private void logAuth(Authentication authentication) {
        LOGGER.info("Authentication success for login " + authentication.getName());
    }

}