package ru.iitdgroup.hcb.operational.web.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iitdgroup.nwng.model.rule.hierarchydef.RuleLogicDef;

import javax.persistence.*;
import java.io.*;
import java.time.ZonedDateTime;


/*
 * Сущность для хранения правила {@link RuleInstanceDef}
 */
@Entity
@Table(
        name = "FS_RULE_INSTANCE",
        indexes = {
                @Index(name = "uk_code", columnList = "code", unique = true)
        }
)
public final class RuleInstance extends AbstractNamedEntity {

    @ManyToOne(targetEntity = RuleGroup.class, fetch = FetchType.EAGER)
    @NotNull
    @JoinColumn(name = "GROUP_ID")
    public RuleGroup group;

    private Integer scoring;
    private Boolean status;
    private ZonedDateTime lastUpdatedOn;

    @OneToOne
    private UserAccount lastUpdatedBy;

    private byte[] logicDefJson;

    @Transient
    private RuleLogicDef logicDef;

    public RuleInstance() {
    }

    public RuleLogicDef getLogicDef() {
        if (logicDefJson != null) {
            try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(logicDefJson))) {
                return (RuleLogicDef) objectInputStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public void setLogicDef(RuleLogicDef logicDef) {
        try (ByteArrayOutputStream ba = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(ba)) {
            objectOutputStream.writeObject(logicDef);
            this.logicDefJson = ba.toByteArray();
        } catch (IOException e) {
            //todo
        }
    }


    public RuleGroup getGroup() {
        return group;
    }

    public void setGroup(@NotNull RuleGroup group) {
        this.group = group;
    }

    public Integer getScoring() {
        return scoring;
    }

    public void setScoring(Integer scoring) {
        this.scoring = scoring;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ZonedDateTime getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(@Nullable ZonedDateTime lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public UserAccount getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(@Nullable UserAccount lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public byte[] getLogicDefJson() {
        return logicDefJson;
    }

    public void setLogicDefJson(@Nullable byte[] logicDefJson) {
        this.logicDefJson = logicDefJson;
    }
}
