package ru.iitdgroup.hcb.operational.web.ui.view.settings;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import ru.iitdgroup.hcb.operational.web.entity.ICAlert;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.presenters.worktable.WorkTablePresenter;
import ru.iitdgroup.hcb.operational.web.repository.*;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;
import ru.iitdgroup.hcb.operational.web.ui.view.MainLayout;
import ru.iitdgroup.model.hcb.cards.CardTransaction;
import ru.iitdgroup.model.hcb.cards.Currency;
import ru.iitdgroup.model.hcb.cards.FinancialInstitution;
import ru.iitdgroup.model.hcb.cards.HCBCard;
import ru.iitdgroup.model.hcb.clients.Client;
import ru.iitdgroup.model.hcb.clients.Gender;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Route(value = "db", layout = MainLayout.class)
@PageTitle("Служебное")
public class DBInitView extends CommonView {

    private final WorkTablePresenter workTablePresenter;
    private final ClientRepository clientRepository;
    private final CardRepository cardRepository;
    private final CurrencyRepository currencyRepository;
    private final CardTransactionRepository cardTransactionRepository;
    private final FinancialInstitutionRepository financialInstitutionRepository;
    private final UserAccountRepository userAccountRepository;
    private final AlertRepository alertRepository;
    private final List<String> operators = Arrays.asList("Одинцов В.В.", "Субботин Х.М.", "Гамула М.М.", "Волкова Б.А.");
    private final List<String> transactionStatuses = Arrays.asList("В очереди", "Операция отклонена", "Операция приостановлена");
    private final List<String> statuses = Arrays.asList("inQueue", "inProgress", "escalated");
    private final List<String> rules = Arrays.asList("Серое", "Черное");
    private final List<String> severity = Arrays.asList("minor", "major");
    private List<HCBCard> cards;
    private ArrayList<Currency> currencies;
    private List<Client> clients;
    private List<ICAlert> alerts;
    private List<CardTransaction> transactions;
    private List<FinancialInstitution> aquires;

    public DBInitView(Breadcrumbs breadcrumbs,
                      WorkTablePresenter workTablePresenter,
                      ClientRepository clientRepository,
                      CardRepository cardRepository,
                      CurrencyRepository currencyRepository,
                      CardTransactionRepository cardTransactionRepository,
                      FinancialInstitutionRepository financialInstitutionRepository,
                      UserAccountRepository userAccountRepository,
                      AlertRepository alertRepository) {
        super(breadcrumbs);
        this.workTablePresenter = workTablePresenter;
        this.clientRepository = clientRepository;
        this.cardRepository = cardRepository;
        this.currencyRepository = currencyRepository;
        this.cardTransactionRepository = cardTransactionRepository;
        this.financialInstitutionRepository = financialInstitutionRepository;
        this.userAccountRepository = userAccountRepository;
        this.alertRepository = alertRepository;
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(new Label("Служебная страница для генерации данных в БД"));
        center.add(verticalLayout);
        Button button = new Button("Сгенерировать");
        button.addClickListener(e -> {
            load();
        });
        verticalLayout.add(button);
        Button button2 = new Button("Сгенерировать алерты");
        button2.addClickListener(e -> {
            loadAlerts();
        });
        verticalLayout.add(button2);

        Button delete = new Button("Очистить");
        delete.addClickListener(e -> {
            alertRepository.deleteAll();
            cardTransactionRepository.deleteAll();
            financialInstitutionRepository.deleteAll();
            cardRepository.deleteAll();
            currencyRepository.deleteAll();
            clientRepository.findAll().forEach(it -> {
                try {
                    clientRepository.delete(it);
                } catch (Exception ex) {

                }
            });
        });
        verticalLayout.add(delete);
    }

    private void loadAlerts() {
        if (transactions == null) {
            transactions = cardTransactionRepository.findAll();
        }
        List<UserAccount> accounts = userAccountRepository.findAll();
        alerts = new ArrayList<>();
        for (int i = 0; i < 10_000; i++) {
            ICAlert alert = new ICAlert();
            alert.setStatus(statuses.get((int) (Math.random() * 3)));
            alert.setCardTransaction(transactions.get((int) (Math.random() * 3)));
            alert.setRuleName(rules.get((int) (Math.random() * 2)));
            alert.setScore((10 + (long) (Math.random() * 1000)));
            alert.setOwner(accounts.get((int) (Math.random() * accounts.size())).getCode());
            alert.setWorkspace("prod");
            alert.setObjectType("ICAlert");
            Timestamp timestamp = Timestamp.valueOf(Timestamp.valueOf(LocalDateTime.now()).toLocalDateTime().minusHours(5 + (int) (Math.random() * 24)));
            alert.setAlertDate(Date.from(timestamp.toInstant()));
            alert.setLastModified(Date.from(Timestamp.valueOf(timestamp.toLocalDateTime().plusMinutes((int) (Math.random() * 5))).toInstant()));
            alert.setSeverity(severity.get((int) (Math.random() * 2)));
            alert.setTitle("Алерт");
            alerts.add(alert);
        }
        alertRepository.saveAll(alerts);
    }

    public void load() {
        createClients();
        createCurrencies();
        transactions = createTransactions();
    }

//    private void createHistory(List<Alert> alerts) {
//        List<History> histories = new ArrayList<>();
//        alerts.forEach(alert -> {
//            History create = new History();
//            create.setAction("Алерт создан");
//            create.setAlert(alert);
//            create.setAuthor("Система");
//            Timestamp createTime = Timestamp.valueOf(LocalDateTime.now().minusHours(((int) (Math.random() * 5))));
//            create.setDateTime(createTime);
//            histories.add(create);
//            if (statuses.get(1).equals(alert.getStatus())) {
//                histories.add(createInWork(alert, createTime));
//            } else if (statuses.get(2).equals(alert.getStatus())) {
//                histories.add(createInWork(alert, createTime));
//                histories.add(createEscalated(alert));
//            }
//        });
//        historyRepository.saveAll(histories);
//    }

//    private History createEscalated(Alert alert) {
//        History history = new History();
//        history.setAction("Эскалирован");
//        history.setAlert(alert);
//        LocalDateTime now = LocalDateTime.now();
//        history.setDateTime(Timestamp.valueOf(now));
//        history.setAuthor(operators.get((int) (Math.random() * 4)));
//        return history;
//    }

    //    private History createInWork(Alert alert, Timestamp createTime) {
//        History history = new History();
//        history.setAction("Взят в работу");
//        history.setAlert(alert);
//        history.setDateTime(Timestamp.valueOf(createTime.toLocalDateTime().plusHours((int) (Math.random() * 5))));
//        history.setAuthor(operators.get((int) (Math.random() * 4)));
//        return history;
//    }
//
    private List<CardTransaction> createTransactions() {
        List<CardTransaction> result = new ArrayList<>();
        aquires = new ArrayList<>();
        FinancialInstitution institution = new FinancialInstitution();
        institution.setName("Сбербанк");
        FinancialInstitution institution2 = new FinancialInstitution();
        institution2.setName("Уралсиб");
        aquires.add(institution);
        aquires.add(institution2);
        financialInstitutionRepository.saveAll(aquires);
        for (int i = 0; i < 50; i++) {
            CardTransaction transaction = new CardTransaction();
            transaction.setSourceCurrency(currencies.get((int) (Math.random() * currencies.size())));
            transaction.setTransactionAmount(BigDecimal.valueOf((500 + (int) (Math.random() * 11111))));
            HCBCard card = cards.get((int) (Math.random() * cards.size()));
            transaction.setCard(card);
            Timestamp timestamp = Timestamp.valueOf(Timestamp.valueOf(LocalDateTime.now()).toLocalDateTime().plusHours((int) (Math.random() * 5)));
            transaction.setTransmissionDateAndTimeMoscow(Date.from(timestamp.toInstant()));
            transaction.setAcquirer(aquires.get((int) (Math.random() * 2)));
            result.add(transaction);
        }
        cardTransactionRepository.saveAll(result);
        return result;
    }

    private void createCurrencies() {
        currencies = new ArrayList<>();
        Currency rub = new Currency();
        rub.setCode(810);
        rub.setIsoName("RUR");
        rub.setName("Рубль");
        currencies.add(rub);

        Currency usd = new Currency();
        usd.setCode(840);
        usd.setIsoName("USD");
        usd.setName("Доллар США");
        currencies.add(usd);

        Currency eur = new Currency();
        eur.setCode(978);
        eur.setIsoName("EUR");
        eur.setName("Евро");
        currencies.add(eur);

        currencyRepository.saveAll(currencies);

    }

    private List<Client> createClients() {
        clients = new ArrayList<>();
        Client client1 = new Client();
        client1.setPassport("1234 567890");
        client1.setCuid((long) (1111 + (int) (Math.random() * 11111)));
        client1.setFirstName("Сорокин Ярослав Андреевич");
        client1.setLastName("Сорокин");
//        client1.setPhone("8(983)696-56-21");
        client1.setGender(Gender.M);
        clients.add(client1);

        Client client2 = new Client();
        client2.setPassport("0987 654321");
        client2.setCuid((long) (1111 + (int) (Math.random() * 11111)));
        client2.setFirstName("Тимошенко Игнатий Дмитриевич");
        client2.setLastName("Тимошенко");
//        client2.setPhone("7(599)902-93-92");
        client2.setGender(Gender.M);
        clients.add(client2);

        Client client3 = new Client();
        client3.setPassport("4567 098123");
        client3.setCuid((long) (1111 + (int) (Math.random() * 11111)));
        client3.setFirstName("Жданов Юрий Богданович");
        client3.setLastName("Жданов");
//        client3.setPhone("7(997)955-41-33");
        client3.setGender(Gender.M);
        clients.add(client3);

        Client client4 = new Client();
        client4.setPassport("8752 125696");
        client4.setCuid((long) (1111 + (int) (Math.random() * 11111)));
        client4.setFirstName("Стрелков Виталий Васильевич");
        client4.setLastName("Стрелков");
//        client4.setPhone("8(957)434-51-98");
        client4.setGender(Gender.M);
        clients.add(client4);
        clientRepository.saveAll(clients);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        try {
            HCBCard card1 = new HCBCard();
            card1.setExpireDate(format.parse("20-01-2022"));
            card1.setCardHolder(client1);
            card1.setPan("4231 9276 6201 6405");
            card1.setExpireDate(format.parse("24-06-2024"));

            HCBCard card2 = new HCBCard();
            card2.setCardHolder(client2);
            card2.setPan("5455 5273 5959 4362");
            card2.setExpireDate(format.parse("21-11-2023"));

            HCBCard card3 = new HCBCard();
            card3.setCardHolder(client3);
            card3.setPan("9581 1712 4876 8967");
            card3.setExpireDate(format.parse("12-12-2022"));

            HCBCard card4 = new HCBCard();
            card4.setCardHolder(client4);
            card4.setPan("4373 4419 4121 1126");
            card4.setExpireDate(format.parse("05-03-2024"));

            HCBCard card5 = new HCBCard();
            card5.setCardHolder(client1);
            card5.setPan("7780 0960 0740 1801");
            card5.setExpireDate(format.parse("10-07-2022"));

            HCBCard card6 = new HCBCard();
            card6.setCardHolder(client2);
            card6.setPan("9638 5497 6806 4481");
            card6.setExpireDate(format.parse("08-08-2021"));

            HCBCard card7 = new HCBCard();
            card7.setCardHolder(client3);
            card7.setPan("7994 7645 6361 1743");
            card7.setExpireDate(format.parse("14-08-2023"));

            HCBCard card8 = new HCBCard();
            card8.setCardHolder(client4);
            card8.setPan("4335 9715 6328 3480");
            card8.setExpireDate(format.parse("23-12-2021"));

            cards = Arrays.asList(card1, card2, card3, card4, card5, card6, card7, card8);
            cardRepository.saveAll(cards);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return clients;
    }
}
