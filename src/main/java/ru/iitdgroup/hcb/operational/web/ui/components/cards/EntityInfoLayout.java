package ru.iitdgroup.hcb.operational.web.ui.components.cards;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.jetbrains.annotations.NotNull;

public abstract class EntityInfoLayout extends VerticalLayout {
    protected VerticalLayout innerButtonLayout;
    protected EntityCard card;

    public EntityInfoLayout() {

    }

    public void init() {
        setWidthFull();
        setHeightFull();
        getStyle().set("background-color", "var(--opr-grey)");
        setPadding(false);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.getStyle().set("padding-left", "30px");
        horizontalLayout.getStyle().set("padding-right", "30px");
        horizontalLayout.setWidthFull();
        horizontalLayout.setHeight("fit-content");
        horizontalLayout.getStyle().set("background-color", "var(--opr-grey)");
        horizontalLayout.getStyle().set("overflow", "hidden");
        VerticalLayout innerCardLayout = new VerticalLayout();
        innerCardLayout.getStyle().set("background-color", "#FFFFFF");
        innerCardLayout.getStyle().set("flex-grow", "2");
        innerCardLayout.setHeightFull();
        innerCardLayout.setWidthFull();

        horizontalLayout.add(innerCardLayout);

        innerButtonLayout = initButtonLayout();
        horizontalLayout.add(innerButtonLayout);
        card = new EntityCard();
        innerCardLayout.getStyle().set("overflow", "auto");
        innerCardLayout.add(card);

        add(horizontalLayout);
    }

    @NotNull
    private VerticalLayout initButtonLayout() {
        VerticalLayout innerButtonLayout = new VerticalLayout();
        innerButtonLayout.setWidth("fit-content");
        innerButtonLayout.setHeight("fit-content");
        innerButtonLayout.getStyle().set("background-color", "#FFFFFF");
        Label buttonsLabel = new Label("Применить ручные действия");
        buttonsLabel.setClassName("opr-label");
        buttonsLabel.getStyle().set("font-size", "16px");
        innerButtonLayout.add(buttonsLabel);
        return innerButtonLayout;
    }

    protected abstract void createCards(EntityCard card);

    protected abstract void initActionsButtons(VerticalLayout innerButtonLayout);
}
