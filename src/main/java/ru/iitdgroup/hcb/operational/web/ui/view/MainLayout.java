package ru.iitdgroup.hcb.operational.web.ui.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import ru.iitdgroup.hcb.operational.web.ui.components.NavigationPanel;
import ru.iitdgroup.hcb.operational.web.ui.view.alerts.AlertView;
import ru.iitdgroup.hcb.operational.web.ui.view.analytics.AnalyticsView;
import ru.iitdgroup.hcb.operational.web.ui.view.customers.ClientsView;
import ru.iitdgroup.hcb.operational.web.ui.view.profiles.BusinessObjectProfileView;
import ru.iitdgroup.hcb.operational.web.ui.view.profiles.SystemProfile;
import ru.iitdgroup.hcb.operational.web.ui.view.profiles.WsProfileView;
import ru.iitdgroup.hcb.operational.web.ui.view.reclamations.ReclamationDicView;
import ru.iitdgroup.hcb.operational.web.ui.view.reclamations.ReclamationView;
import ru.iitdgroup.hcb.operational.web.ui.view.rules.BlackRulesView;
import ru.iitdgroup.hcb.operational.web.ui.view.rules.GreyRulesView;
import ru.iitdgroup.hcb.operational.web.ui.view.rules.WhiteRulesView;
import ru.iitdgroup.hcb.operational.web.ui.view.security.SecurityView;
import ru.iitdgroup.hcb.operational.web.ui.view.transactions.TransactionsView;
import ru.iitdgroup.hcb.operational.web.ui.view.worktable.WorktableView;

import java.util.HashMap;
import java.util.Map;

/**
 * Main layout for all views
 * Основной лейаут для всех форм
 * Описывает левое меню и навигацию
 */
public class MainLayout extends AppLayout implements BeforeEnterObserver {
    private final Tabs tabs = new Tabs();
    private final Map<Class<? extends Component>, HorizontalLayout> navigationTargets = new HashMap<>();

    public MainLayout() {
        setPrimarySection(Section.DRAWER);
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        NavigationPanel navigationPanel = new NavigationPanel();
        HorizontalLayout worktable = navigationPanel.addMenuItem("Рабочий стол", VaadinIcon.HOME_O, null);
        navigationPanel.expandItem(worktable);
        HorizontalLayout alertWorktable = navigationPanel.addMenuSubItem(worktable, "Оповещения", WorktableView.class);
        navigationTargets.put(WorktableView.class, alertWorktable);
        HorizontalLayout reclamations = navigationPanel.addMenuSubItem(worktable, alertWorktable, "Рекламации", ReclamationView.class);
        navigationTargets.put(ReclamationView.class, reclamations);

        HorizontalLayout catalogs = navigationPanel.addMenuItem("Справочники", VaadinIcon.BOOK, null);
        HorizontalLayout alerts = navigationPanel.addMenuSubItem(catalogs, "Оповещения", AlertView.class);
        navigationTargets.put(AlertView.class, alerts);
        HorizontalLayout clients = navigationPanel.addMenuSubItem(catalogs, alerts, "Клиенты", ClientsView.class);
        navigationTargets.put(ClientsView.class, clients);
        HorizontalLayout transactions = navigationPanel.addMenuSubItem(catalogs, clients, "Транзакции", TransactionsView.class);
        navigationTargets.put(TransactionsView.class, transactions);
        HorizontalLayout reclamationsDic = navigationPanel.addMenuSubItem(catalogs, transactions, "Рекламации", ReclamationDicView.class);
        navigationTargets.put(ReclamationDicView.class, reclamationsDic);

        HorizontalLayout analytics = navigationPanel.addMenuItem("Отчетность", VaadinIcon.LINE_CHART, AnalyticsView.class);
        navigationTargets.put(AnalyticsView.class, analytics);
        HorizontalLayout settings = navigationPanel.addMenuItem("Настройки", VaadinIcon.COG_O, null);

        HorizontalLayout rules = navigationPanel.addMenuSubItem(settings, settings, "Правила", null);
        HorizontalLayout blackRules = navigationPanel.addMenuSubItem(rules, rules, "Черные", BlackRulesView.class, true);
        navigationTargets.put(BlackRulesView.class, blackRules);
        HorizontalLayout greyRules = navigationPanel.addMenuSubItem(rules, blackRules, "Серые", GreyRulesView.class, true);
        navigationTargets.put(GreyRulesView.class, greyRules);
        HorizontalLayout whiteRules = navigationPanel.addMenuSubItem(rules, greyRules, "Белые", WhiteRulesView.class, true);
        navigationTargets.put(WhiteRulesView.class, whiteRules);

        HorizontalLayout profiles = navigationPanel.addMenuSubItem(settings, whiteRules, "Профили", null);
        HorizontalLayout systemProfile = navigationPanel.addMenuSubItem(profiles, profiles, "Системный", SystemProfile.class, true);
        navigationTargets.put(SystemProfile.class, systemProfile);
        HorizontalLayout wsProfile = navigationPanel.addMenuSubItem(profiles, systemProfile, "WS", WsProfileView.class, true);
        navigationTargets.put(WsProfileView.class, wsProfile);
        HorizontalLayout businessObjectProfile = navigationPanel.addMenuSubItem(profiles, wsProfile, "Бизнес-объекта", BusinessObjectProfileView.class, true);
        navigationTargets.put(BusinessObjectProfileView.class, businessObjectProfile);


        HorizontalLayout security = navigationPanel.addMenuSubItem(settings, businessObjectProfile, "Безопасность", SecurityView.class);
        HorizontalLayout rolesAndRights = navigationPanel.addMenuSubItem(security, security, "Роли и права", SecurityView.class, true);
//        navigationTargets.put(SecurityView.class, rolesAndRights);
        HorizontalLayout users = navigationPanel.addMenuSubItem(security, rolesAndRights, "Пользователи", SecurityView.class, true);
//        navigationTargets.put(SecurityView.class, users);
        HorizontalLayout sessions = navigationPanel.addMenuSubItem(security, users, "Активные сессии", SecurityView.class, true);
//        navigationTargets.put(SecurityView.class, sessions);
        HorizontalLayout logs = navigationPanel.addMenuSubItem(security, sessions, "Логи", SecurityView.class, true);
//        navigationTargets.put(SecurityView.class, logs);

//        navigationPanel.addMenuSubItem(settings, logs, "Служебное", DBInitView.class);
        navigationPanel.addUserItem();
        addToDrawer(navigationPanel);
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Class<?> navigationTarget = event.getNavigationTarget();
        HorizontalLayout layout = navigationTargets.get(navigationTarget);
        if (layout != null) {
            navigationTargets.values().forEach(it -> it.getElement().removeAttribute("selected"));
            layout.getElement().setAttribute("selected", "true");
        }
    }

    protected void logout() {
        //https://stackoverflow.com/a/5727444/1572286
//		SecurityContextHolder.clearContext();
//		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//		if (requestAttributes instanceof ServletRequestAttributes) {
//			((ServletRequestAttributes) requestAttributes).getRequest().getSession().invalidate();


//		getUI().ifPresent(ui -> ui.getPage().setLocation(MainView.ROUTE));
    }
}