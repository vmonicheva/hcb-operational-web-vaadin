package ru.iitdgroup.hcb.operational.web.presenters.transaction

import org.springframework.data.jpa.domain.Specification
import ru.iitdgroup.hcb.operational.web.QueryCount
import ru.iitdgroup.model.hcb.cards.CardTransaction
import javax.persistence.criteria.*

/**
 * Составной предикат для поиска объектов по имени таба и полю поиска
 */
class TransactionSpecification(val filter: TransactionFilter) : Specification<CardTransaction>, QueryCount {

    override fun toPredicate(root: Root<CardTransaction>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {

        if (currentQueryIsCountRecords(query)) {
//            root.join<ICAlert, CardTransaction>("cardTransaction", JoinType.LEFT)
//                    .join<Any, Any>("card", JoinType.LEFT)
//                    .join<Any, Any>("client", JoinType.LEFT)
        } else {
            root.fetch<Any, Any>("card", JoinType.LEFT)
                    .fetch<Any, Any>("cardHolder", JoinType.LEFT)
            root.fetch<Any, Any>("cardholderBillingCurrencyCode", JoinType.LEFT)
            root.fetch<Any, Any>("terminalPaymentSystem", JoinType.LEFT)
        }
        return null
    }

    override fun currentQueryIsCountRecords(criteriaQuery: CriteriaQuery<*>): Boolean {
        return criteriaQuery.resultType.name == "java.lang.Long" ||
                criteriaQuery.resultType == Long::class.javaPrimitiveType
    }

}