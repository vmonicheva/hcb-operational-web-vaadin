package ru.iitdgroup.hcb.operational.web.entity;

import java.util.UUID;


public interface INamedEntity {

    String getCode();

    String getDescription();

    UUID getGuid();

    String getName();
}
