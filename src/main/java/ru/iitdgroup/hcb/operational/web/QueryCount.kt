package ru.iitdgroup.hcb.operational.web

import javax.persistence.criteria.CriteriaQuery
/**
 * Интерфейс небходим для внедрения метода отвечающего за различие между запросами count и лист
 * */
interface QueryCount {
    fun currentQueryIsCountRecords(criteriaQuery: CriteriaQuery<*>): Boolean
}