package ru.iitdgroup.hcb.operational.web.services;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Service;
import ru.iitdgroup.hcb.operational.web.config.SecurityUtils;
import ru.iitdgroup.hcb.operational.web.entity.UserAccount;
import ru.iitdgroup.hcb.operational.web.entity.Workspace;
import ru.iitdgroup.hcb.operational.web.repository.UserAccountRepository;

import java.util.List;

@Service
public class UserService {
    private final UserAccountRepository repository;

    public UserService(UserAccountRepository repository) {
        this.repository = repository;
    }

    public UserAccount getCurrentUser() {
        Authentication authentication = SecurityUtils.INSTANCE.getAuthentication();
        if (authentication != null) {
            String username = ((LdapUserDetailsImpl) authentication.getPrincipal()).getUsername();
            return repository.findByCode(username);
        } else {
            return null;
        }
    }

    public List<UserAccount> findAllByWorkspace(Workspace workspace) {
        return repository.findAllBySelectedWorkspace(workspace);
    }


    public List<UserAccount> getAllUsers() {
        return repository.findAll();
    }

    public void selectWorkspace(@NotNull Workspace workspace) {
        UserAccount user = getCurrentUser();
        if (user != null) {
            user.setSelectedWorkspace(workspace);
            repository.save(user);
        }
    }
}
