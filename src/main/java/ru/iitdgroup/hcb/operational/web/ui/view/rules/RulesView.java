package ru.iitdgroup.hcb.operational.web.ui.view.rules;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.iitdgroup.hcb.operational.web.entity.RuleInstance;
import ru.iitdgroup.hcb.operational.web.repository.RuleInstanceRepository;
import ru.iitdgroup.hcb.operational.web.services.UserService;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;
import ru.iitdgroup.hcb.operational.web.ui.components.CommonListComponent;
import ru.iitdgroup.hcb.operational.web.ui.components.grids.RuleGrid;
import ru.iitdgroup.hcb.operational.web.ui.view.CommonView;

import java.util.List;

public abstract class RulesView extends CommonView {
    protected final RuleInstanceRepository ruleInstanceRepository;
    protected final UserService userService;
    protected final Button createRule;

    public RulesView(Breadcrumbs breadcrumbs, RuleInstanceRepository ruleInstanceRepository, UserService userService) {
        super(breadcrumbs);
        this.ruleInstanceRepository = ruleInstanceRepository;
        this.userService = userService;

        CommonListComponent commonListComponent = new CommonListComponent();
        commonListComponent.setHeightFull();
        commonListComponent.getStyle().set("margin-top", "0");

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setPadding(false);
        horizontalLayout.getStyle().set("margin-top", "0");
        horizontalLayout.getStyle().set("padding-right", "15px");
        horizontalLayout.setJustifyContentMode(JustifyContentMode.END);
        horizontalLayout.setWidthFull();
        createRule = new Button("Создать правило");
        createRule.addClickListener(e -> {
            UI.getCurrent().navigate(RuleInfoView.class, null);
        });
        createRule.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        horizontalLayout.add(createRule);

        RuleGrid grid = new RuleGrid(true, getItems());
        commonListComponent.getContentLayout().add(grid);
        VerticalLayout main = new VerticalLayout();
        main.setPadding(false);
        main.add(horizontalLayout);
        HorizontalLayout buttonsLayout = new HorizontalLayout(new Button("Удалить"));
        buttonsLayout.setWidthFull();
        buttonsLayout.setJustifyContentMode(JustifyContentMode.START);
        grid.getBottomLayout().addComponentAtIndex(0, buttonsLayout);
        main.add(commonListComponent);
        center.add(main);
    }

    protected abstract List<RuleInstance> getItems();
}
