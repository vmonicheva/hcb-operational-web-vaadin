package ru.iitdgroup.hcb.operational.web.constants

object Constants {
    const val client: String = "client"
    const val alert: String = "alert"
    const val alerts: String = "alerts"
    const val myAlerts: String = "my_alerts"
    const val closed = "closed"
    const val escalated = "escalated"
    const val holded = "holded"
    const val inQueue = "inQueue"
    const val inProgress = "inProgress"
    const val all = "all"
    const val blackRule = "Черное"
    const val grayRule = "Серое"
    const val countAlerts = "showed_n_alerts_from_y"
    const val countTransactions = "showed_n_transactions_from_y"
    const val countRules = "showed_n_rules_from_y"
    const val countCustomers = "showed_n_customers_from_y"
    const val countUsers = "showed_n_users_from_y"
    const val countRoles = "showed_n_roles_from_y"
}