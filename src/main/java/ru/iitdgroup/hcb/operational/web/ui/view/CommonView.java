package ru.iitdgroup.hcb.operational.web.ui.view;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.iitdgroup.hcb.operational.web.ui.components.Breadcrumbs;

import javax.annotation.PostConstruct;

/**
 * Center part layout
 * <p>
 * Центральная часть для основного лейаута
 */
@CssImport(value = "./styles/fs-style.css")
@CssImport(value = "./styles/fs-button-style.css", themeFor = "vaadin-button")
@CssImport(value = "./styles/fs-textfield.css", themeFor = "vaadin-text-field")
@CssImport(value = "./styles/fs-text-area.css", themeFor = "vaadin-text-area")
public class CommonView extends FlexLayout {
    protected final FlexLayout center = new FlexLayout();
    private final VerticalLayout breadcrumbsLayout;
    private final Breadcrumbs breadcrumbs;

    public CommonView(Breadcrumbs breadcrumbs) {
        this.breadcrumbs = breadcrumbs;
        setClassName("opr-gray-layout");
        breadcrumbsLayout = new VerticalLayout();
        breadcrumbsLayout.setClassName("fs-breadcrumbs");
        breadcrumbsLayout.setSizeFull();
        breadcrumbsLayout.removeAll();
        HorizontalLayout crumbs = breadcrumbs.getBreadcrumbs();
        //workaround https://vaadin.com/forum/thread/17411679/can-t-move-a-node-from-one-state-tree-to-another-error-in-vaadin-10
        try {
            crumbs.getElement().removeFromTree();
            breadcrumbsLayout.add(crumbs);
        } catch (Exception e) {
            //todo log
        }

        center.setSizeFull();
        center.setClassName("opr-gray-layout");
        breadcrumbsLayout.add(center);
        add(breadcrumbsLayout);
        setSizeFull();
    }

    @PostConstruct
    public void postcontract() {
//
    }
}