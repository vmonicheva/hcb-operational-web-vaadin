package ru.iitdgroup.hcb.operational.web.config

import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.i18n.CookieLocaleResolver
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import java.nio.charset.StandardCharsets

/**
 * Вспомагательные бины для работы MVC
 */
@Configuration
open class MvcConfigurer : WebMvcConfigurer {

    @Bean
    open fun localeResolver(): LocaleResolver {
        return CookieLocaleResolver()
    }

    @Bean
    open fun localeInterceptor(): LocaleChangeInterceptor {
        val localeInterceptor = LocaleChangeInterceptor()
        localeInterceptor.paramName = "lang"
        return localeInterceptor
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(localeInterceptor())
    }

    @Bean(name = ["I18NMessageSource"])
    open fun messageSource(): MessageSource {
        val messageSource = ReloadableResourceBundleMessageSource()
        messageSource.setBasename("classpath:i18n/messages")
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.displayName())
        return messageSource
    }
}