package ru.iitdgroup.hcb.operational.web.presenters.alerts


import org.springframework.data.domain.Sort
import java.time.ZoneOffset

/**
 * Класс представляет собой набор опций необходимых для успешной работы запроса
 */
open class DataQuery<F: BaseFilter>(var offset: Int, var limit: Int, var filter: F) {
    var dataSort: Sort = Sort.unsorted()
}